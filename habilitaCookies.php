<?php
include("fun/funciones.php");
$config = parseaconf("conf/conf.ini","1");
?>
<!doctype HTML>
<html>

<head>
  <meta charset="utf-8" />
  <meta lang="es" />
  <meta name="description" content="Operacion y mantenimiento OX de Telecable" />
  <meta name="author" content="Sergio Bastian" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <?php if($config['cabecera']['faviconmostrar'] == true){
	if($config['cabecera']['faviconpersonal'] == true)
		echo "<link rel='shortcut icon' type='image/png' href='img/favicon.png' />";
	else
                echo "<link rel='shortcut icon' type='image/png' href='img/default.png' />";
	}
  ?>
  <title>WikiB</title>
  <link rel="stylesheet" type="text/css" href="css/reset2.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>-->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- Encriptar pwd -->
  <script type="text/javascript" src="js/jquery.crypt.js"></script>
  <script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
  <div id="habilitacookies">
<h1>Habilitar cookies</h1>
<br />
<p>Para hacer uso de <strong>WikiB</strong> es necesario habilitar las cookies del navegador.</p>
<p>En <stonr>WikiB</stong> no hacemos uso de cookies de terceros, pero si usamos cookis propias para que la experiencia de uso sea lo más confortable posible.</p>
<p>A continuaci&oacute;n puedes consultar como habilitar esta funcionalidad seg&uacute;n el navegador que est&eacute;s utilizando:</p>
<br />
<div class="enlacescookies">
  <a href="https://support.google.com/accounts/answer/61416?hl=es" target="_blank">Chrome</a><br />
  <a href="https://support.microsoft.com/es-es/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank">Explorer</a><br />
  <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias" target="_blank">Firefox</a><br />
  <a href="https://support.apple.com/kb/ph21411?locale=es_ES" target="_blank">Safari</a><br />
  <a href="http://help.opera.com/Linux/10.60/es-ES/cookies.html" target="_blank">Opera</a>
</div>
<br />
<br />
<p>Una vez hayas habilitado esta funcionalidad solo tienes que recargar la p&aacute;gina o dirigirte al siguiente enlace pinchando <span class="link" onclick="inicio()">aqu&iacute;</span></p>
</div>
</body>
</html>
