<?php

  header("Content-Type: text/html;charset=utf-8");

  // Inicializamos las variables.
  session_start();
  // Mostramos cualquier error de PHP.
  error_reporting(E_ALL);
  ini_set('display_errores', 1);
  // Incluimos el fichero de funciones de PHP.
  include("fun/funciones.php");

  // Comprobamos si las cookies estan o no habilitadas.
  if(isset($_GET['cookiesnotenabled'])){
    error_log("ERROR - No estan habilitadas las cookies.", 0);
    redireccion("http://wikioxpre.telecable.es/habilitaCookies.php");
  } else {

  // Comprobamos datos de configuracion llamando a la funcion.
  $config = parseaconf("conf/conf.ini","1");

  // Confirmamos si se usa o no Chrome para mostrar una alerta.
  alertaNavegador();

  // Cargamos las librerias de PHPMailer.
  $phpmailerinstall = compruebaPHPMailerConf();
  if($phpmailerinstall == 'ko'){
    echo "<b>PHPMailer no está configurado en el sistema, y por tanto fallarán las acciones que requieran el envío de correos electrónicos</b>";
  }

?>
<!DOCTYPE html>
<head>
  <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
  <meta lang="es" />
  <meta name="description" content="Wiki personal y colaborativa" />
  <meta name="author" content="WikiB editor" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <?php if($config['cabecera']['faviconmostrar'] == true){
    if($config['cabecera']['faviconpersonal'] == true){
      echo "<link rel='shortcut icon' type='image/png' href='img/".$config['cabecera']['favicon']."' />";
    } else {
      echo "<link rel='shortcut icon' type='image/png' href='img/default.png'  />";
    }
  }
  ?>
  <title>WikiB</title>
  <link rel="stylesheet" type="text/css" href="css/reset2.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- Editor TinyCME -->
  <script language="javascript" type="text/javascript" src="js/editor/tinymce/js/tinymce/tinymce.min.js"></script>
  <!-- JQUERY css -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- JQUERY -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- Encriptar pwd JQUERY -->
  <script type="text/javascript" src="js/jquery.crypt.js"></script>
  <!-- JS personal -->
  <script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
<?php
  // LOGIN
  // ------
  // Si se ejecuta el formulario de login.
  if(isset($_POST['usuario'])){
    include("inc/loginComprueba.php");
  }

  // RECORDAR PWD
  // -------------
  // Si se ejecuta el formulario de recordar pwd.
  elseif(isset($_POST['usuario2'])){
    include("inc/pwdAleatoriaRecuerda.php");
  }

  // EXIT
  // -------
  // Si el usuario le da a cerrar sesion destruimos la variables de sesiones creadas.
  elseif(isset($_GET['quit'])){
    Logger::info("Sesion cerrada por el usuario " . $_SESSION['user'] . ".");
    session_destroy();
    inicio();
  }

  // CONTENIDO
  // ----------
  // Si no ne cumple los casos anteriores mostramos el contenido.
  else {

?>
  <!--Contenido principal-->
  <div id="contenido" class="flex-container">
    <!--Fondo para inactivar pagina cuando se muestra una capa superior-->
    <div id="fondo-negro" class="fondo-negro"></div>
    <!--Menu izquierda con indice de contenidos-->
    <div id="izq" >
      <div id="container">
        <div id="cabecera">

          <!-- Boton para mostrar el menu en responsive design. -->
          <img id='muestraMenuIzqOculta' src='img/hamburger.png' title='Oculta el menú lateral izquierdo.' />

          <!--Imagen y texto de cabecera-->
          <h2><a href="http://<?php echo $_SERVER['SERVER_NAME'];?>">WikiB <?php
            if($config['cabecera']['logomostrar'] == true){
              if($config['cabecera']['logopersonal'] == true){
                echo "<img id='cabeceralogo' src='img/".$config['cabecera']['logo']."' />";
              } else {
                echo "<img id='cabeceralogo' src='img/default.png'  />";
              }
            }
            ?></a></h2>
          <p><?php echo $config['cabecera']['texto'];?></p>
          <?php if(isset($_SESSION['id']))
                echo "<p id='bienvenido'>Bienvenido ".$_SESSION['name']."</p>"; ?>
        </div>
        <!--Botones de menu indice si se inicia sesion-->
        <div id="indice-iconos">
          <?php if (isset($_SESSION['id'])) { ?>

            <a title="Cerrar sesi&oacute;n" href="?quit=1"><img src="img/logoff.png" /></a>
            <a id="editaIcon" title="Edita &iacute;ndice" href='?indiceEdita=1'><img src="img/doc.png" /></a>
            <img id="buscarIcon" title="Buscar, pulsa Enter" src="img/searchicon.png" onclick="muestraBuscar()" />
            <a id="logsIcon" title="Logs de actividad." href="?logs=1" ><img src="img/logs.png" /></a>
            <a id="confUserIcon" title='Configuraci&oacute; de usuario' href='?confusuario=1'><img src='img/usericon.png' /></a>

          <?php
            if ($_SESSION['group'] == 'administrador') {
              echo "<a id='usersIcon' title='Administraci&oacute;n de usuarios' href='?confusuarios=1'><img src='img/users.png' /></a>";
              echo "<a id='confIcon' title='Datos de configuraci&oacute;n de la plataforma' href='?confplataforma=1'><img src='img/configicon.png' /></a>";
            }
          } else {
              // Boton de login si no se inicia sesion.
              // Si los datos de administracion de la db y el usuario admin no estan agregados mostramos un boton deshabilitado.
              if ( $config['mysql']['dbconfig'] != 0 && $config['confinicial']['conf'] != 0 ) {
                echo '<button name="login" id="botonLogin" type="button">Login</button>';
                echo '<img id="buscarIcon" title="Buscar, pulsa Enter" src="img/searchicon.png" onclick="muestraBuscar()" />';
              } else
                echo '<button name="login" id="botonLoginDisabled" type="button" disabled>Login</button>';
           } ?>

        </div>
        <div id='buscaPatron'>
          <input id="buscaPatronInput" name="buscaPatronInput" type="text" placeholder="Pulsa Enter ..." />
        </div>
        <?php
        // Indice.
        // Si los datos de administracion de la db no estan agregados no mostramos nada.
        // ############## PENDIENTE ##########################
        if ( $config['mysql']['dbconfig'] != 0 ) {
          echo '<nav id="indice">';
            include("inc/indiceLateralIzquierdo.php");
          echo '</nav>';
        }
        ?>
      </div>
    </div>

    <!--Menu derecha con el contenido-->
    <div id="dch" class="flex-item">
      <div id="container">

        <!-- Boton para mostrar el menu en responsive design. -->
        <img id='muestraMenuIzq' src='img/hamburger.png' title='Muestra el menú lateral izquierdo.' />

        <?php
        // Info uso de cookies.
        if(!isset($_COOKIE['politicacookies'])){
          include('inc/mensajePoliticaCookies.php');
        }

        // Si se accede a la URL generada para la recuperacion de la pwd cargamos el archivo que comprueba si el codigo aun existe y envia la nueva pwd aleatoria.
        if(isset($_GET['recuperapwd'])) {
          include("inc/pwdAleatoriaEnvia.php");
        }
        // Si se genera a mostrar la informacion de la politica de cookies.
        elseif(isset($_GET['politicacookies'])){
          include("inc/politicaCookies.php");
        }
        // Administracion de usuarios.
        elseif(isset($_GET['confusuarios'])){
          include("inc/usuariosMostrar.php");
        }
        // Confirmamos si los datos de administracion de la db y usuario admin estan creados o no.
        // Para ello consultamos el fichero de configuracion.
        elseif ( $config['mysql']['dbconfig'] == 0 OR $config['admin']['adminconfig'] == 0 OR $config['redis']['redisconfig'] == 0 OR $config['email']['emailconfig'] == 0 ){
          // Si no estan configurados los datos de conf de la plataforma.
          include ("inc/config.php");
        }
        // Si el admin accede a la configuración de la plataforma.
        elseif ( isset($_GET['confplataforma']) && $_SESSION['group'] == 'administrador') {
            include ("inc/config.php");
        }
        // Configuracion de usuario.
        elseif (isset($_GET['confusuario'])){
            include ("inc/configUserForm.php");
        }
        // Editar indice.
        elseif (isset($_GET['indiceEdita'])){
            include ("inc/indiceEdita.php");
        }

        // Logs de actividad.
        elseif (isset($_GET['logs'])) {
          include("inc/logs.php");
        }

        // Resultado de busqueda.
        elseif (isset($_GET['busqueda'])){
            include("inc/busqueda.php");
        }

        // Indice general de indices y contenidos activos o contenido especifico.
        else {
          include ("inc/contenido.php");
        }

 ?>
      </div>
    </div>
  </div>

  <!--Capa para el login de usuario, oculta-->
  <?php
    if(!isset($_SESSION['id']))
      include("inc/login.php");

  // Fin contenido.
  }
// FIN comprueba cookies.
}
  ?>
  <script>
    // Confirmamos si las cookies estan habilitadas.
    var cookiescheck = navigator.cookieEnabled;
    // Si no estan habilitadas redirigimos a pagina informativa para habilitarlas.
    if ( cookiescheck == false ){
      alert('Cookies deshabilitadas, redirigiendo a página de información.');
      window.location="/?cookiesnotenabled=1";
    }

    // Confirmamos navegador.
    var navegador = navigator.userAgent;
    if(!navegador.match(/Chrome/)){
        // Si no es Chrome creamos/actualizamos la cookie con TTL 1h.
        var now = new Date();
        now.setTime(now.getTime()+1*3600*1000);
        document.cookie = "navegador=1; expires=" + now.toUTCString() + "path=/";
    }
  </script>
</body>
</html>
