<?php
// Para obtener las variables de sesión tras el login.
if(!isset($_SESSION)){
    session_start();
}

// Importacion de PHPMailer.
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

///////////////////}//////////////////////////////////////////////////
// Funcion para comprobar si los ficheros de PHPMailer están correctamente instalado en el servidor.
function compruebaPHPMailerConf(){
    if (file_exists("/usr/share/php/PHPMailer/PHPMailer.php")){

        // Para el envio de correos con PHPMailer.
        //require("/usr/share/php/PHPMailer/class.phpmailer.php");
        require '/usr/share/php/PHPMailer/Exception.php';
        require '/usr/share/php/PHPMailer/PHPMailer.php';
        require '/usr/share/php/PHPMailer/SMTP.php';

        return "ok";

    } else {
        return "ko";
    }
}
///////////////////}//////////////////////////////////////////////////

///////////////////}//////////////////////////////////////////////////
// Funcion para comprobar si PHPMailer esta correctamente instalado o no.
function compruebaPHPMailer(){
    if(new PHPMailer(true))
        return 'ok';
    else
        return 'ko';
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para escribir logs
class Logger {
  private static function addEntry($str) {
    $remoteip = $_SERVER['REMOTE_ADDR'];

    if(isset($_SESSION['user']))
        $usuarioActivo = $_SESSION['user'];
    else
        $usuarioActivo = "Anonimo";

    //$handle = fopen('./requests.log', 'a');
    $log_file = $_SERVER['DOCUMENT_ROOT']."/logs/requests.log";

    if (file_exists($log_file))
        $handle = fopen($log_file, 'a');
    else
        $handle = fopen($log_file, 'wb');

    fwrite($handle, sprintf("%s - - %s [user: %s] %s\n", $remoteip, date('[d/M/Y:H:i:s O]'),$usuarioActivo, $str));

    fclose($handle);
  }
  public static function warn($str) {
    self::addEntry("WARNING - $str");
  }
  public static function info($str) {
    self::addEntry("INFO - $str");
  }
  public static function debug($str) {
    self::addEntry("DEBUG - $str");
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Parsear archivo de configuracion
function parseaconf($file,$aso){
  //$conffile="conf/$file";
  $conffile = $file;
  if(file_exists($conffile)){
    // Para parseo a array asociativo.
    if($aso == "1"){
      return parse_ini_file($conffile,TRUE);
      Logger::info('Archivo de configuracion comprobado.');
    } else {
    //Para parseo a array simple.
      return parse_ini_file($conffile);
      Logger::info('Archivo de configuracion comprobado.');
    }
  } else {
    error_log("ERROR - El fichero de configuracion ".$file." no existe.", 0);
    echo "<script>alert('El fichero de configuracion ".$file." no existe.');</script>";
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para conectar a MySQL
function conectar_db() {
  // Obtenemos la informacion del archivo de configuracion en donde estan las credenciales
  $conf = parseaconf("conf/access.ini",1);
  $host = $conf['mysql']['servername'];
  $user = $conf['mysql']['username'];
  $pwd = $conf['mysql']['password'];
  $db = $conf['mysql']['dbname'];
  $port = $conf['mysql']['portmysql'];
  $mysql = mysqli_connect($host,$user,$pwd,$db,$port);
  if (mysqli_connect_errno()){
    error_log("Conexion a la base de datos MySQL fallida. Host: $host - User: $user - Pwd: $pwd - DB: $db - Port: $port.");
    //echo "Fallo la conexion a la base de datos: ". mysqli_connect_error();
    $mensaje = "ERROR - Fallo la conexion a la base de datos: ".mysqli_connect_error();
    return $mensaje;
  } else {
    // Indicamos que vamos a usar el tipo de codificacion UTF8 para mantener caracteres latinos, como acentos.
    mysqli_set_charset($mysql,"utf8");
    Logger::info("MySQL conexion OK.");
    return $mysql;
  }
  // Borramos las variables obtenidas.
  unset($conf);
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para conectar a MySQL para carga inicial.
function conectar_db_inicial($host,$user,$pwd,$db,$port){

  $host = $host;
  $user = $user;
  $pwd = $pwd;
  $db = $db;
  $port = $port;

  $mysql = mysqli_connect($host,$user,$pwd,$db,$port);

  if (mysqli_connect_errno()){

    error_log("Conexion a la base de datos MySQL fallida para la carga inicial de tablas. Host: $host - Pwd: $pwd - DB: $db - Port: $port. " . mysqli_connect_error());
    $mensaje = "ERROR - Fallo la conexion a la base de datos: " . mysqli_connect_error();
    return $mensaje;

  } else {

    mysqli_set_charset($mysql,"utf8");
    Logger::info("MySQL conexion OK.");
    return $mysql;

  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Carga las tablas en MySQL durante la configuracion inicial.
function cargaTablasInicial($mysql){

  // Archivo con el dump inicial de la base de datos.
  $filename = "sql/orig.sql";

  // Variable temporal, usada para almacenar la query actual.
  $templine = '';

  // Variable para devolver resultado correcto.
  $res = 1;

  // Leer el fichero entero.
  $lines = file($filename);

  // Bucle sobre cada linea.
  foreach ( $lines as $line ){

    // Descarta comentarios.
    if ( substr($line, 0, 2) == '--' || $line == '' )
      continue;

    // Agrega la linea en el segmento actual.
    $templine .= $line;

    // Si tiene un punto y coma al final, quiere decir que es el final de la query.
    if ( substr(trim($line), -1, 1) == ';' ) {

      // Crea la query.
      if(!mysqli_query($mysql,$templine)) {

        // Si fallara la carga de la linea paramos el bucle retornando el error.
        return "ERROR - No se pudieron cargar las tablas durante la configuración inicial. " . mysqli_error($mysql);
        $res = 0;
        break;

      }

      // Reiniciamos la variable temporal.
      $templine = '';

    }

  }

  // Confirmamos si el resultado de la ejecucion de cada linea fue correcto.
  if ( $res == 1 )
    return 'ok';

}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para actualizar datos MySQL
function actualiza_db($servername,$username,$password,$dbname,$portmysql){
  $mysql = mysqli_connect($servername,$username,$password,$dbname,$portmysql);
  if (mysqli_connect_errno()){
    error_log("Conexion a la base de datos MySQL fallida. Host: $servername - User: $username - Pwd: $password - DB: $dbname - Port: $portmysql.");
    //echo "Fallo la conexion a la base de datos: ". mysqli_connect_error();
    $mensaje = "ERROR - Fallo la conexion a la base de datos: ".mysqli_connect_error();
    return $mensaje;
  } else {
    Logger::info("MySQL conexion OK.");
    return $mysql;
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para comprobar si existe un usuario admin.
function compruebaadmin($mysql){
  $sql = "SELECT id, usuario, grupo FROM usuarios WHERE grupo = 'administrador'";
  $query = mysqli_query($mysql,$sql);
  $array = mysqli_fetch_array($query);
  return $array;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para comprobar las credenciales de usuario.
function compruebalogin($mysql,$usuario,$pwd){
  // Evitamos SQL INJECTION. Confirmamos login.
  $user = mysqli_real_escape_string($mysql,$usuario);
  $user = strtolower($user);
  $pass = mysqli_real_escape_string($mysql,$pwd);
  $sql = "SELECT id, usuario, nombre, pwd, email, grupo FROM usuarios WHERE usuario = '".$user."' AND  pwd = '".$pass."' AND  activo = 1";
  $query = mysqli_query($mysql,$sql);
  $array=mysqli_fetch_array($query);
  return $array;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para actualizar fecha e IP del ultimo login.
function ultimologin($mysql,$usuario,$fecha,$ip){
  $usuario = mysqli_real_escape_string($mysql,$usuario);
  $ususario = strtolower($usuario);
  $fecha = mysqli_real_escape_string($mysql,$fecha);
  $ip = mysqli_real_escape_string($mysql,$ip);
  $sql = "UPDATE usuarios SET ultimoLogin = '".$fecha."', ultimologinIp = '".$ip."' WHERE usuario = '".$usuario."'";
  $query = mysqli_query($mysql,$sql);
  if($query)
    return "ok";
  else
    return $query;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para comprobar datos usuario
function compruebauser($mysql,$usuario){
  // Evitamos SQL IJECTION. Confirmamos datos de usuario.
  $user = mysqli_real_escape_string($mysql,$usuario);
  $user = strtolower($user);
  $sql = "SELECT id FROM usuarios WHERE usuario = '".$user."'";
  $query = mysqli_query($mysql,$sql);
  $array = mysqli_fetch_array($query);
  return $array;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Funcion para comprobar datos usuario-email para el recordatorio de pwd
function compruebauseremail($mysql,$usuario,$email){
  // Evitamos SQL IJECTION. Confirmamos datos de usuario.
  $user = mysqli_real_escape_string($mysql,$usuario);
  $user = strtolower($user);
  $mail = mysqli_real_escape_string($mysql,$email);
  $sql = "SELECT id, usuario, email, grupo FROM usuarios WHERE usuario = '".$user."' AND email = '".$mail."' AND activo = 1";
  $query = mysqli_query($mysql,$sql);
  $array = mysqli_fetch_array($query);
  return $array;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Function obter email
function obtenemail($mysql,$usuario){
  $user = mysqli_real_escape_string($mysql,$usuario);
  $user = strtolower($user);
  $sql = "SELECT email FROM usuarios WHERE usuario = '".$user."'";
  $query = mysqli_query($mysql,$sql);
  $res = mysqli_fetch_array($query);
  return $res;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Cambio de pwd desde recuerda contraseña
function cambiapwdrecuerda($mysql,$usuario,$con,$md5){
  $user = mysqli_real_escape_string($mysql,$usuario);
  $user = strtolower($user);
  $pwd = mysqli_real_escape_string($mysql,$con);
  $md5 = mysqli_real_escape_string($mysql,$md5);
  // Si la pwd ya va en md5 indicamos al llamar a la funcion que la variable $md5 = si, de lo contrario no.
  if($md5 == 'no')
    $sql = "UPDATE usuarios SET pwd = MD5('".$pwd."') WHERE usuario = '".$user."' AND activo = '1'";
   else
    $sql = "UPDATE usuarios SET pwd = '".$pwd."' WHERE usuario = '".$user."' AND activo = '1'";
  $query = mysqli_query($mysql,$sql);
  if($query){

    $user = obtenUsuarioUser($mysql,$usuario);
    $userArray = mysqli_fetch_array($user);

    if ( !isset($_SESSION['user']))
      $_SESSION['user'] = '-';

    // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
    guardarLog($mysql,'cambia','usuario',$userArray['id'],$usuario,'Cambiada contraseña al usuario con ID ' . $userArray['id'] . " - Usuario: $usuario - Nombre: " . $userArray['nombre'],$_SESSION['user'],'','');

    Logger::info("PWD cambiada para el usuario $usuario correctamente.");
    return "ok";
  } else {
    error_log("PWD no cambiada para el usuario $usuario.",0);
    return $query;
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Actualizar datos de acceso para un usuario.
function actualizausuario($mysql,$nombre,$usuario,$con,$correo,$grupo,$estado){
  $nombre = mysqli_real_escape_string($mysql,$nombre);
  $user = mysqli_real_escape_string($mysql,$usuario);
  $user = strtolower($user);
  $pwd = mysqli_real_escape_string($mysql,$con);
  $email = mysqli_real_escape_string($mysql,$correo);
  $group = mysqli_real_escape_string($mysql,$grupo);
  $state = mysqli_real_escape_string($mysql,$estado);
  $sql = "UPDATE usuarios SET nombre = '".$nombre."', usuario = '".$user."', pwd = MD5('".$pwd."'), email = '".$email."', grupo = '".$group."', activo = '".$state."' where usuario = '".$user."' and activo = '1'";
  $query = mysqli_query($mysql,$sql);
  if($query) {
    Logger::info("Usuario administrador $usuario actualizado correctamente.");
    return "ok";
  } else {
    error_log("Error al actualizar el usuario administrador $usuario.",0);
    return $query;
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Obtener estado de un acuenta.
function estadoCuenta($mysql,$id){
  $id = mysqli_real_escape_string($mysql,$id);
  $sql = "SELECT activo FROM usuarios WHERE id = '".$id."'";
  $query = mysqli_query($mysql,$sql);
  $array = mysqli_fetch_array($query);
  return $array;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Actualizar datos de acceso para un usuario.
function cambiaEstadoUsuario($mysql,$accion,$id){
  $accion = mysqli_real_escape_string($mysql,$accion);
  $id = mysqli_real_escape_string($mysql,$id);

  if($accion == 'activa'){
    $tipoAccion = 'Activar';
    $sql = "UPDATE usuarios SET activo = '1' where id = '".$id."'";
  } else {
    $tipoAccion = 'Desactivar';
    $sql = "UPDATE usuarios SET activo = '0' where id = '".$id."'";
  }
  if(mysqli_query($mysql,$sql)){

    $usuario = obtenusuario($mysql,$id);
    $usuarioArray = mysqli_fetch_array($usuario);

    // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
    guardarLog($mysql,'cambia','usuario',$id,$usuarioArray['usuario'],"$tipoAccion usuario con id " . $usuarioArray['id'] . " - Nombre: " .$usuarioArray['nombre'],$_SESSION['user'],'','');

    Logger::warn("Accion: $accion - Usuario ID: $id");
    return "ok";
  } else {
    error_log("ERROR - Accion: $accion - Usuario ID: $id. No se ha podido ejecutar la accion solicitada.", 0);
    return "ko";
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Crea usuario MySQL.
function creausuario($mysql,$nombre,$usuario,$con,$correo,$grupo,$estado,$md5,$archivoConf){

  $user = mysqli_real_escape_string($mysql,$usuario);
  $name = mysqli_real_escape_string($mysql,$nombre);
  $user = strtolower($user);
  $pwd = mysqli_real_escape_string($mysql,$con);
  $email = mysqli_real_escape_string($mysql,$correo);
  $group = mysqli_real_escape_string($mysql,$grupo);
  $state = mysqli_real_escape_string($mysql,$estado);
  $md5 = mysqli_real_escape_string($mysql,$md5);
  if($md5 == 0)
    $sql = "INSERT INTO usuarios (nombre,usuario,pwd,email,grupo,activo) VALUES ('".$name."', '".$user."', MD5('".$pwd."'), '".$email."', '".$group."', '1')";
  else
    $sql = "INSERT INTO usuarios (nombre,usuario,pwd,email,grupo,activo) VALUES ('".$name."', '".$user."', '".$pwd."', '".$email."', '".$group."', '".$estado."')";
  $query = mysqli_query($mysql,$sql);
  if($query) {

    if ( $estado == 1 )
      $estadoUsuario = 'activo';
    else
      $estadoUsuario = 'inactivo';

    // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
    $conf3 = parseaconf($archivoConf,1);
    if ( $conf3['confinicial']['conf'] != 0 )
      guardarLog($mysql,'crea','usuario',mysqli_insert_id($mysql),$usuario,"Creado usuario: $usuario - Nombre: $nombre - Email: $email - Grupo: $grupo - Estado: $estadoUsuario",$_SESSION['user'],'','');

    Logger::info("Usuario $usuario creado correctamente en MySQL.");
    return "ok";
  } else {
    error_log("Error al crear el usuario $usuario en MySQL. ".mysqli_error($mysql),0);
    return "Error al crear el usuario $usuario en MySQL. ".mysqli_error($mysql);
  }

}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Borrar usuario MySQL.
function borrarUsuario($mysql,$id,$user){
    $id = mysqli_real_escape_string($mysql,$id);
    $user = mysqli_real_escape_string($mysql,$user);
    $sql = "DELETE FROM usuarios WHERE id = " . $id;
    $query = mysqli_query($mysql,$sql);
    if($query){

      // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
      guardarLog($mysql,'borra','usuario',$id,$user,"Borrado usuario",$_SESSION['user'],'','');

        Logger::info("Usuario eliminado. Usuario: $user - ID: $id");
        return "ok";
    } else {
        error_log("Error al borrar el usuario $usuario con ID $id",0);
        return "Error al borrar el usuario $usuario con ID $id";
    }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Listado usuarios para administracion
function obtenusuarios($mysql,$user,$sort,$dir){
  if ($user != 'none') {
    $sql = "SELECT id, nombre, usuario, pwd, email, grupo, activo, ultimoLogin, ultimoLoginIp FROM usuarios WHERE usuario like '%$user%'";
  } elseif($sort == 'none') {
    $sql = "SELECT id, nombre, usuario, email, grupo, activo, ultimoLogin, ultimoLoginIp FROM usuarios";
  } else {
    if($dir == 1)
      $sql = "SELECT id, nombre, usuario, email, grupo, activo, ultimoLogin, ultimoLoginIp FROM usuarios ORDER BY $sort ASC";
    else
      $sql = "SELECT id, nombre, usuario, email, grupo, activo, ultimoLogin, ultimoLoginIp FROM usuarios ORDER BY $sort DESC";
  }
  $query = mysqli_query($mysql,$sql);
  return $query;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Listado usuarios para administracion
function obtenusuario($mysql,$id){
  $id = mysqli_real_escape_string($mysql,$id);
  $sql = "SELECT id, nombre, usuario, pwd, email, grupo, activo, ultimoLogin, ultimoLoginIp FROM usuarios WHERE id = $id";
  $query = mysqli_query($mysql,$sql);
  return $query;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Listado usuarios para administracion buscarndo por user
function obtenUsuarioUser($mysql,$user){
  $user = mysqli_real_escape_string($mysql,$user);
  $sql = "SELECT * FROM usuarios WHERE usuario = '$user'";
  $query = mysqli_query($mysql,$sql);
  return $query;
}

/////////////////////////////////////////////////////////////////////
// Listado usuarios para administracion
function obtenDatosUsuario($mysql,$id){
  $id = mysqli_real_escape_string($mysql,$id);
  $sql = "SELECT id, nombre, usuario, email, grupo FROM usuarios WHERE id = '".$id."'";
  $query = mysqli_query($mysql,$sql);
  $array = mysqli_fetch_array($query);
  if($array) {
    Logger::info("Datos del usuario ".$array['usuario']." obtenidos correctamente de la db MySQL.");
    return $array;
  } else {
    error_log("Error al obtener los datos del usuario $id para su edicion.",0);
    return "ko";
  }
}

/////////////////////////////////////////////////////////////////////
// Actualiza datos de usuario
function actualizaDatosUsuario($mysql,$id,$user,$name,$email,$group){
  $id = mysqli_real_escape_string($mysql,$id);
  $user = mysqli_real_escape_string($mysql,$user);
  $name = mysqli_real_escape_string($mysql,$name);
  $email = mysqli_real_escape_string($mysql,$email);
  $group = mysqli_real_escape_string($mysql,$group);

  //$sql = "UPDATE usuarios SET usuario = '".$user."', nombre = '".$name."', email = '".$email."', grupo = '".$group."' WHERE id = '".$id."'";
  $sql = "UPDATE usuarios SET nombre = '".$name."', email = '".$email."' WHERE id = '".$id."'";
  $query = mysqli_query($mysql,$sql);

  if($query){

    // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
    guardarLog($mysql,'cambia','usuario',$id,$user,'Actualizado ID: ' . $id . ' - Usuario: ' . $user . ' - Nombre: ' . $name . ' - Email: ' . $email . ' - Grupo: ' . $group,$_SESSION['user'],'','');

    Logger::warn("Datos del usuario con ID ".$id." (".$user.") actualizados correctamente. Usuario: ".$user." - Nombre: ".$name." - Email: ".$email." - Grupo: ".$group.".");
    return "ok";
  } else {
    error_log('Error al actualizar los datos del usuario con ID '.$id.'.', 0);
    return "ko";
  }

}
/////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////
// Comprueba acceso al correo segun datos access.ini
function comprobarcorreo($log,$host,$port,$username,$pwd,$smtpauth2,$smtpsecure2){
//  require("/usr/share/php/PHPMailer/class.phpmailer.php");
  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->IsHTML(true);
  if($log == 1){
    $mail->SMTPDebug = 2;
    //$mail->Debugoutput = 'html';
  }
  $mail->SMTPAuth = $smtpauth2;     // turn on SMTP authentication (true/false).
  $mail->Host = $host;
  $mail->Port = $port;
  $mail->SMTPSecure = "$smtpsecure2"; // indicar tls para 587 o ssl para 465 para seguridad, false para openrelay puerto 25.

  if($smtpauth2 == true){
    $mail->Username = $username;  // SMTP username
    $mail->Password = $pwd; // SMTP password
  }

  $smtpresultado = $mail->smtpConnect();
  if($smtpresultado == true){
    sleep(3);
    return "ok";
    $mail->smtpClose();
  }
  else{
    return $smtpresultado;
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Enviar correo
function enviarcorreo($enlaceOpwd,$to,$tipo){
  // Creamos el array con los datos de acceso.
  $conf = parseaconf("conf/access.ini","1");
  $host = $conf['email']['emailserver'];
  if($conf['email']['smtpauth'] == true){
    $user = $conf['email']['emailuser'];
    $pwd = $conf['email']['emailpwd'];
  } else {
    $user = "";
    $pwd = "";
  }
  $auth = $conf['email']['smtpauth'];
  $port = $conf['email']['emailport'];
  //require("/usr/share/php/PHPMailer/class.phpmailer.php");
  $mail = new PHPMailer(true);
  $mail->CharSet = 'UTF-8';
  $mail->IsHTML(true);

  // SMTPAuth
  $mail->IsSMTP();
  $mail->SMTPAuth = $auth;
  $mail->SMTPSecure = $conf['email']['smtpsecurity'];
  //$mail->SMTPAutoTLS = false;

  $mail->Port = $port;
  $mail->Username = $user;
  $mail->Password = $pwd;
  $mail->Host = $host;
  $mail->From = $conf['email']['from'];
  $mail->FromName = $conf['email']['fromname'];
  $mail->AddAddress($to,"");
  $mail->WordWrap = 70;

  // Generamos el correo en HTML a partir de la plantilla en MJML.
  if($tipo == "generaEnlace") {

    // Asunto codificado para poder enviar imagenes.
    $asunto1 = '&#128273; Recuperación de credencial';
    // Generamos el contenido del mensaje de MJML a HTML.
    shell_exec("mjml -r inc/recuerdaenlace.mjml -o inc/recuerdaenlace.html");
    // Obtenemos el contenido de la plantilla generada en HTML.
    $body = file_get_contents("inc/recuerdaenlace.html");
    // Agregamos la URL aleatoria para recuperar la contraseña.
    $body = str_replace("{{urlvar}}",$enlaceOpwd,$body);

  } elseif($tipo == "nuevaPwd") {

    // Asunto codificado para poder enviar imagenes.
    $asunto1 = "&#128273; Nueva credencial";
    // Generamos el contenido del mensaje de MJML a HTML.
    shell_exec("mjml -r inc/recuerdapwd.mjml -o inc/recuerdapwd.html");
    // Obtenemos el contenido de la plantilla generada en HTML.
    $body = file_get_contents("inc/recuerdapwd.html");
    // Agregamos la PWD aleatoria para recuperar la contraseña.
    $body = str_replace("{{pwd}}",$enlaceOpwd,$body);

  } else {

    // En caso de ejecutar el comando de envio de email sin indicar alguno de los casos anteriores, enviamos un email de error.
    // Asunto codificado para poder enviar imagenes.
    $asunto1 = "&#10008; Error al ejeucar la petición";
    // No usamos MJML para generar el cuerpo del mensaje.
    $body = "<h3>Error al ejecutar la petición</h3><hr /><p>Algo ha ido mal al ejecutar la acción solicitada.</p><p>Contacta con el administrador de la aplicación.</p><p>Saludos - WikiB</p>";
  }

  $asunto=  preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $asunto1);
  $mail->Subject = $asunto;
  $mail->Body = $body;
  if(!$mail->Send()){
    echo "<script>alert('No se pudo enviar el mensaje de correo. Contacte con el administrador.');</script>";
  } else {
    echo "<script>alert('Mensaje de correo enviado a la dirección ". $to ."');</script>";
  }
  // Borramos el archivo HTML generdo para el correo enviado.
  if($tipo == "generaEnlace")
    //shell_exec("rm -rf inc/recuerdaenlace.html");
    return 'ok';
  elseif($tipo == "nuevaPwd")
    shell_exec("rm -rf inc/recuerdapwd.html");
  // Destruimos el array con los datos de acceso.
  unset($conf);
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Prueba datos SMTP
function smtpCheck($host,$port,$username,$pwd,$from,$fromName,$smtpAuth){
  require("/usr/share/php/PHPMailer/class.phpmailer.php");
  $mail = new PHPMailer();
  $mail->IsHTML(true);
  $mail->SMTPAuth = $smtpAuth;     // turn on SMTP authentication
  $mail->Host = $host;
  $mail->Port = $port;
  $mail->Username = $username;  // SMTP username
  $mail->Password = $pwd; // SMTP password
  $mail->From = $from;
  $mail->FromName = $fromName;
  if($mail->smtpConnect()){
      $mail->smtpClose();
     return "Connected";
  }
  else{
      return "Connection Failed";
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Conectar con redis.
function rediscon($host,$port,$db,$authr) {
  $redis = new Redis();
  if(!$redis->pconnect($host,$port)){
    error_log("Conexion a la base de datos Redis fallida. Host: $host - Port: $port - DB: $db - Auth: $authr.");
    return "ERROR - No se pudo conectar con la DB de Redis.";
  } else {
    Logger::info("Redis conexion OK.");
    $redis->select($db);
    return $redis;
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Comprueba navegador Chrome
function compruebaNavegador(){
  $browser = $_SERVER['HTTP_USER_AGENT'];
  if(preg_match('/Chrome/i', $browser))
    return 1;
  else
    return 0;

}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Crea cookie.
function creaCookie($nombre,$tiempo){
  setcookie($nombre, 1, time()+$tiempo);
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Obtener navegador
function alertaNavegador(){
  // Comprobamos el navegador.
  $navegador = compruebaNavegador();
  // Si no es Chrome confirmamos si existe o no la cookie.
  if($navegador == '0')
    if(!isset($_COOKIE['navegador'])){
    // Si no existe la cookie escribimos el log correspondiente y mostramos el mensaje de advertencia.
    Logger::warn("El navegador no es Chrome.");
    echo "<script>alert('Recomendamos el uso de Chrome para una correcta visualización de WikiB.');</script>";
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Comprueba si las cookies estan habilitadas.
function compruebaCookies(){
  //creaCookie('cookietest',3600);
  if(isset($_COOKIE['cookietest']))
    return "sicookies";
  else
    return "nocookies";
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Eliminar acentos
function stripAccents($str) {
    return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Redirecciones.
function redireccion($url){
  header('Location: '.$url);
}
function ventanaanterior(){
  header('Location: ' . $_SERVER['HTTP_REFERER']);
}
function inicio(){
  header("Location: http://{$_SERVER['SERVER_NAME']}/");
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Obtener email administrador.
function obtenemailadmin(){
  $emailadmin = parseaconf("conf/access.ini",1);
  return $emailadmin['admin']['emailadmin'];
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Actualiza conf.ini
function actualizaconfini($conf,$dbconfig,$redisconfig,$adminconfig,$emailconfig,$texto,$logomostrar,$logopersonal,$logo,$faviconmostrar,$faviconpersonal,$favicon){
  $file = fopen('conf/conf.ini', 'w') or die('Error al abrir el archivo conf.ini.');
  $txt = "[confinicial]\nconf = $conf\n\n[mysql]\ndbconfig = $dbconfig\n\n[redis]\nredisconfig = $redisconfig\n\n[admin]\nadminconfig = $adminconfig\n\n[email]\nemailconfig = $emailconfig\n\n[cabecera]\ntexto = \"$texto\"\nlogomostrar = $logomostrar\nlogopersonal = $logopersonal\nlogo = \"user.png\"\nfaviconmostrar = $faviconmostrar\nfaviconpersonal = $faviconpersonal\nfavicon = \"favicon.png\"";
  if(fwrite($file,$txt))
    Logger::warn('Actualizado archivo conf.ini.');
  else
    error_log("ERROR - Archivo conf.ini no actualizado.", 0);
  fclose($file);
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Actualiza access.ini
function actualizaaccessini($mysqlservername,$mysqlusername,$mysqlpassword,$mysqldbname,$mysqlport,$redisservername,$redisauth,$redisdb,$redisport,$adminname,$adminuser,$adminpassword,$adminemail,$emailserver,$emailport,$emailsmtpauth,$emailuser,$emailpwd,$emailsmtpsecurity,$emailfrom,$emailfromname){
  $adminuser = strtolower($adminuser);
  $file = fopen('conf/access.ini', 'w') or die('Error al abrir el archivo access.ini');
  $txt = "[mysql]\nservername = $mysqlservername\nusername = $mysqlusername\npassword = $mysqlpassword\ndbname = $mysqldbname\nportmysql = $mysqlport\n\n[redis]\nservernameredis = $redisservername\nauthredis = $redisauth\ndbredis = $redisdb\nportredis = $redisport\n\n[admin]\nnameadmin = \"$adminname\"\nuseradmin = $adminuser\npasswordadmin = \"$adminpassword\"\nemailadmin = $adminemail\n\n[email]\nemailserver = $emailserver\nemailport = $emailport\nsmtpauth = $emailsmtpauth\nemailuser = $emailuser\nemailpwd = \"$emailpwd\"\nsmtpsecurity = $emailsmtpsecurity\nfrom = $emailfrom\nfromname = \"$emailfromname\"";
  if(fwrite($file,$txt))
    Logger::warn('Actualizado archivo access.ini.');
  else
    error_log('ERROR - Archivo access.ini no actualizado.', 0);
  fclose($file);
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Actualiza linea en archivo.
function actualizalinea($archivo,$patron,$nuevo){
  $file1 = fopen($archivo, "r") or exit("ERROR - Error al abrir el archivo $archivo en modo lectura.");
  $contenido = "";
  while(!feof($file1)){
    $linea = fgets($file1);
    if(preg_match("/".$patron."/", $linea)){
      $contenido = $contenido.$nuevo."\n";
    } else {
      $contenido = $contenido.$linea;
    }
  }
  fclose($file1);

  $file = fopen($archivo, "w") or exit ("ERROR - Error al abrir el archivo $archivo en modo escritura.");
  if(fwrite($file, $contenido))
    return 'ok';
  else
    return 'ko';

  fclose($file);
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Convertir imagen a JPG.
function convertImage($originalImage, $outputImage, $quality){
    // jpg, png, gif or bmp?
    $exploded = explode('.',$originalImage);
    $ext = $exploded[count($exploded) - 1];

    if (preg_match('/jpg|jpeg/i',$ext))
        $imageTmp=imagecreatefromjpeg($originalImage);
    else if (preg_match('/png/i',$ext))
        $imageTmp=imagecreatefrompng($originalImage);
    else if (preg_match('/gif/i',$ext))
        $imageTmp=imagecreatefromgif($originalImage);
    else if (preg_match('/bmp/i',$ext))
        $imageTmp=imagecreatefrombmp($originalImage);
    else
        return 0;

    // quality is a value from 0 (worst) to 100 (best)
    imagejpeg($imageTmp, $outputImage, $quality);
    imagedestroy($imageTmp);

    return 1;
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Subir imagen logo o favicon.
// Tenemos que indicar en el primer parametro si es logo o favicon.
function subelogofavicon($tipo){
  $target_dir = "img/";

  Logger::info('Obteniendo el nombre del fichero.');
  if($tipo == "logo")
    $target_file = $target_dir . basename($_FILES["conf-imagen"]["name"]);
  elseif($tipo == "favicon")
    $target_file = $target_dir . basename($_FILES["conf-favicon"]["name"]);

  $uploaderror = array();
  $uploadOk = 1;
  Logger::info('Obteniendo la extension del fichero.');
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
  echo "<p>################</p>";
  echo "<p> Datos del $tipo</p>";
  echo "<p>################</p>";
  echo "<p>Fichero: $target_file</p>";
  echo "<p>Tipo: $imageFileType</p>";
  // Confirmar si el archivo es una imagen o no.
  Logger::info('Confirmamos si el archivo es una imagen.');
  if($tipo == "logo")
    $check = getimagesize($_FILES["conf-imagen"]["tmp_name"]);
  elseif($tipo == "favicon")
    $check = getimagesize($_FILES["conf-favicon"]["tmp_name"]);
  if($check !== false) {
    Logger::info('El archivo es una imagen.');
    echo "<p>El archivo es una imagen - " . $check["mime"] . ".</p>";
    echo "<p>Resoluci&oacute;n de la imagen: Width - $check[0] / Height - $check[1]</p>";
    // Procesamos la imagen.
    // Confirmamos si el archivo ya existe.
    Logger::info('Confirmamos si el archivo existe en el servidor.');
    if (file_exists($target_file)) {
      echo "<p>ERORR - el archivo $target_file ya existe. Cambia el nombre antes de subirlo, por favor.</p>";
      $uploadOk = 0;
      $error = $tipo."-existe";
      $uploaderror[$error] = "ERROR - La imagen $target_file ya existe en el servidor.";
      error_log("ERROR - La imagen $target_file ya existe en el servidor.", 0);
    }
    Logger::info("Confirmamos el tamanyo de la imagen.");
    // Confirmamos el size del archivo (500KB max).
    if($tipo == "logo")
      $imagesize = $_FILES["conf-imagen"]["size"];
    elseif($tipo == "favicon")
      $imagesize = $_FILES["conf-favicon"]["size"];
    Logger::info("Imagen con un tamanyo de $imagesize.");
    if($imagesize > 500000){
      echo "<p>ERROR - imagen superior a 500KB.</p>";
      $uploadOk = 0;
      $error = $tipo."-tamaño";
      $uploaderror[$error] = "ERROR - Tamaño de imagen superior a 500KB.";
      error_log("ERROR - Tamanyo de imagen superior a 500KB.", 0);
    }
    Logger::info("Confirmamos la resolucion de la imagen. Width: $check[0] - Height: $check[1]");
    // Confirmamos resolucion de la imagen (350 x 350 max).
    if ($check[0] > 350 || $check[1] > 350){
      echo "<p>ERROR - resoluci&oacute;n no permitida.</p>";
      $uploadOk = 0;
      $error = $tipo."-resolución";
      $uploaderror[$error] = "ERROR - Resolucion de imagen superior a 350x350.";
      error_log("ERROR - Resolucion de imagen superior a 350x350.", 0);
    }
    Logger::info("Confirmamos el formato de la imagen: $imageFileType.");
    // Confirmar formatos de imagen permitidos.
    if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg"
    && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {
      echo "<p>ERROR - formato no permitido (JPG, JPEG, PNG o GIF).</p>";
      $uploadOk = 0;
      $error = $tipo."-formato";
      $uploaderror[$error] = "ERROR - Formato de imagen no permitido.";
      error_log("ERROR - Formato de imagen no permitido.", 0);
    }

    // Confirmamos si $uploadOk fue inicializado a 0 por algun error.
    if ($uploadOk == 0) {
      echo "<p>Resultado: ERROR - imagen no subida.</p>";
      $uploaderror[$tipo] = "ERROR - Imagen no subida";
      error_log("ERROR - Imagen no subida.", 0);
    // Si todo ha ido bien, tratamos de subir el archivo.
    } else {
      Logger::info("Iniciamos la subida del $tipo.");
      if($tipo == "logo")
        $imagetmpname = $_FILES["conf-imagen"]["tmp_name"];
      elseif($tipo == "favicon")
        $imagetmpname = $_FILES["conf-favicon"]["tmp_name"];
      if (move_uploaded_file($imagetmpname, $target_file))
      {
        if($tipo == "logo"){
          Logger::info("$tipo ".basename( $_FILES['conf-imagen']['name'])." subido correctamente.");
          echo "<p>El archivo ". basename( $_FILES["conf-imagen"]["name"]). " ha sido subido.</p>";
        } elseif($tipo == "favicon"){
          Logger::info("$tipo ".basename( $_FILES['conf-favicon']['name'])." subido correctamente.");
          echo "<p>El archivo ". basename($_FILES["conf-favicon"]["name"]). " ha sido subido.</p>";
        }
        // Convertimos la imagen a PNG si no es esta su extension, de lo contrario lo copiamos sobrescribiendo el que ya hay.
        Logger::info("Movemos el $tipo sobrescribiendo el de por defecto.");
        if($tipo == "logo")
          $imagendestino = "img/user.png";
        elseif($tipo == "favicon")
          $imagendestino = "img/favicon.png";
        if($imageFileType != "png"){
          Logger::info("Primero convertimos el $tipo a PNG y sobreescribimos el archivo por defecto.");
          if(imagepng(imagecreatefromstring(file_get_contents($target_file)), $imagendestino))
          {
            Logger::info("$tipo convertido a PNG y movido correctamente.");
            echo "<p>El $tipo se ha convertido a formato PNG correctamente.</p>";
          } else {
            echo "<p>ERROR - no se ha podido convertir el $tipo a formato PNG.</p>";
            $uploadOk = 0;
            $error = $tipo."-conversión";
            $uploaderror[$error] = "ERROR - $tipo no convertido a PNG.";
            error_log("ERROR - $tipo no convertido a PNG.", 0);
          }
        } else {
          if(copy($target_file,$imagendestino)){
            Logger::info("$tipo movido correctamente.");
            echo "<p>El $tipo se ha movido correctamente.</p>";
          } else {
            echo "<p>ERROR - no se ha podido mover el $tipo correctamente.</p>";
            $uploadOk = 0;
            $error = $tipo."-mover";
            $uploaderror[$error] = "ERROR - No se ha podido mover el $tipo correctamente.";
            error_log("ERROR - No se ha podido mover el $tipo correctamente.", 0);
          }
        }

        // Borramos la imagen subida por el usuario.
        if(unlink($target_file)){
          Logger::info("$tipo subido por el usuario borrado.");
          echo "<p>$tipo subido por el usuario borrado.</p>";
        } else {
          echo "<p>ERROR - $tipo subido por el usuario no ha podido ser borrada. Se procesa la actualización del $tipo, pero revisa los archivos en el directorio img.</p>";
          $error = $tipo."-borrado";
          $uploaderror[$error] = "ERROR - $tipo subida no borrada del servidor. Se procesa la actualización del $tipo, pero revisa los archivos en el directorio img.";
          error_log("ERROR - $tipo subida no borrada del servidor. Se procesa la actualización del $tipo, pero revisa los archivos en el directorio img.", 0);
        }
      } else {
          echo "<p>ERROR - no se ha podido subir el $tipo.</p>";
          $uploadOk = 0;
          $error = $tipo."-subida";
          $uploaderror[$error] = "ERROR - $tipo no subido.";
          error_log("ERROR - $tipo no subido.", 0);
      }
    }
  } else {
      echo "<p>ERROR - El archivo no es una imagen.</p><br />";
      $uploadOk = 0;
      $error = $tipo."-archivo";
      $uploaderror[$error] = "ERROR - El archivo no es una imagen.";
      error_log("ERROR - El archivo no es una imagen.", 0);
  }
  if($uploadOk == 1){
    Logger::info("Subida finalizada correctamente.");
    echo "<p>Resultado: <span class='resultado-ok'>$tipo subido correctamente</span>.</p>";
    return "ok";
  } else {
    error_log('ERROR - Subida no finalizada correctamente.', 0);
    echo "<p>Resultado: <span class='resultado-ko'>ERROR - Error al subir el $tipo. Revisa los pasos anteriores</span>.</p>";
    return $uploaderror;
  }
}
/////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////
// Indices

// Obten indice/contenido.
function obtenerIndicesContenidos($mysql,$busqueda,$id){
    if ( $busqueda == 'indice' OR $busqueda == 'indiceAgrega' OR $busqueda == 'indiceBorra' ){
        $sql = "SELECT * FROM indice ORDER BY posicion";
    } else if ( $busqueda == 'contenido' OR $busqueda == 'contenidoAgrega' OR $busqueda == 'contenidoBorra' ){
        $id = mysqli_real_escape_string($mysql,$id);
        $sql = "SELECT * FROM contenido WHERE padre_id = ".$id." ORDER BY posicion";
    }

    $query = mysqli_query($mysql,$sql);
    return $query;
}

// Obten indice/contenido especifico.
function indiceContenido($mysql,$busqueda,$id){
  $busqueda = mysqli_real_escape_string($mysql,$busqueda);
  $id = mysqli_real_escape_string($mysql,$id);
  $sql = "SELECT * from $busqueda WHERE id = $id";
  $query = mysqli_query($mysql,$sql);

  return $query;
}

// Obtener ultima posicion.
function obtenerUltimaPosicion($mysql,$elemento,$id){
    $elemento = mysqli_real_escape_string($mysql,$elemento);
    $id = mysqli_real_escape_string($mysql,$id);
    if ($elemento == 'indiceAgrega'){

        $sql = "select posicion from indice where posicion > 0 order by posicion desc limit 1";
        $query = mysqli_query($mysql,$sql);

    } else {

        $sql = "select posicion from contenido where padre_id = '".$id."' and posicion > 0 order by posicion desc limit 1";
        $query = mysqli_query($mysql,$sql);

    }
        $posicion = mysqli_fetch_row($query);
        $ultimo = $posicion[0] * 1 + 1;
        return $ultimo;
}

// Crea indice/contenido
function crearIndiceContenido($mysql,$elemento,$titulo,$id){
    $elemento = mysqli_real_escape_string($mysql,$elemento);
    $titulo = mysqli_real_escape_string($mysql,$titulo);
    $id = mysqli_real_escape_string($mysql,$id);

    // Obtenemos la ultima posicion.
    $posicion = obtenerUltimaPosicion($mysql,$elemento,$id);

    // Creamos el nuevo indice.
    if ( $elemento == 'indiceAgrega'){
        $sql = "INSERT INTO `indice` (`id`, `titulo`, `posicion`, `activo`) VALUES (NULL, '".$titulo."', '".$posicion."', '1')";
        if ( mysqli_query($mysql,$sql) ){

            // Guardamos el log para mostrarlo en el Wiki seccion log.
            guardarLog($mysql,'crea','indice',mysqli_insert_id($mysql),$titulo,'Indice creado',$_SESSION['user'],'','');

            Logger::warn("Creado indice nuevo con titulo \"$titulo\" en la posicion $posicion.");
            return 'ok';
        } else {
            error_log("Error al crear el nuevo indice con titulo \"$titulo\" en la posicoin $posicion.",0);
            return "ERROR - ".mysqli_error($mysql);
        }
    } else if ( $elemento == 'contenidoAgrega' ) {
        $sql = "INSERT INTO `contenido` (`id`, `padre_id`, `titulo`, `posicion`, `activo`) VALUES (NULL, '".$id."', '".$titulo."', '".$posicion."', '1')";
        if ( mysqli_query($mysql,$sql) ){

            // Guardamos el log para mostrarlo en el Wiki seccion log.
            guardarLog($mysql,'crea','contenido',mysqli_insert_id($mysql),$titulo,"Contenido creado para el índice $id",$_SESSION['user'],'','');

            Logger::warn("Creado contenido nuevo con titulo \"$titulo\" en la posicion $posicion del indice $id.");
            return 'ok';
        } else {
            error_log("Error al crear el nuevo contenido con titulo \"$titulo\" en la posicoin $posicion del indice $id.",0);
            return "ERROR - ".mysqli_error($mysql);
        }
    }
}
/////////////////////////////////////////////////////////////////////

// Borra indice/contenido
function borrarIndiceContenido($mysql,$elemento,$id){
    $elemento = mysqli_real_escape_string($mysql,$elemento);
    $id = mysqli_real_escape_string($mysql,$id);

    // Borramos el indice.
    if ( $elemento == 'indiceBorra' ){
        $sql = "DELETE FROM indice WHERE id = $id";
    } else {
        $sql = "DELETE FROM contenido WHERE id = $id";
    }

    // Variable para indicar el elemento en los logs, para evitar contenidoAgrega o borra, e igual para indice.
    if(strpos($elemento, 'contenido') !== false)
        $element = "contenido";
    else
        $element = "indice";

    // Obtenemos los datos del indice / contenido.
    $array = indiceContenido($mysql,$element,$id);
    $datos = mysqli_fetch_array($array);

    if (mysqli_query($mysql,$sql)){

        // Guardamos el log para mostrarlo en el Wiki seccion log.
        if ( $element == 'indice')
          guardarLog($mysql,'borra',$element,$id,$datos['titulo'],ucfirst($element) ." borrado",$_SESSION['user'],'','');
        else
          guardarLog($mysql,'borra',$element,$id,$datos['titulo'],ucfirst($element) ." borrado para el índice " . $datos['padre_id'],$_SESSION['user'],$datos['contenido'],'');

        Logger::warn("Borrado el $element con id $id y titulo \"" . $datos['titulo'] ."\".");
        return 'ok';
    } else {
        error_log("Error al borrar el $element con id $id y titulo \"" . $datos['titulo'] . "\"." . mysqli_error($mysql),0);
        return "ERROR - ".mysqli_error($mysql);
    }
}

/////////////////////////////////////////////////////////////////////
// Obtener estado indice/contenido
function estadoIndiceContenido($mysql,$elemento,$id,$orden,$nuevoestado){
    $elemento = mysqli_real_escape_string($mysql,$elemento);
    $id = mysqli_real_escape_string($mysql,$id);
    $nuevoestado = mysqli_real_escape_string($mysql,$nuevoestado);
    if ($orden == 'estado'){
        $sql = "SELECT activo FROM $elemento where id = $id";
        $query = mysqli_query($mysql,$sql);
        $estado = mysqli_fetch_array($query);
        return $estado;
    } else if ($orden == 'cambia') {
        $sql = "UPDATE $elemento SET activo = '".$nuevoestado."' WHERE id = '".$id."'";

        // Obtenemos los datos del indice / contenido.
        $array = indiceContenido($mysql,$elemento,$id);
        $datos = mysqli_fetch_array($array);

        if(mysqli_query($mysql,$sql)){

            if ( $nuevoestado == 0 )
              $estado = 'deshabilitado';
            else
              $estado = 'habilitado';

            // Guardamos el log para mostrarlo en el Wiki seccion log.
            if ( $elemento == 'contenido' )
              guardarLog($mysql,'cambia',$elemento,$id,$datos['titulo'],ucfirst($elemento) . " $estado para el índice " . $datos['padre_id'],$_SESSION['user'],'','');
            else
              guardarLog($mysql,'cambia',$elemento,$id,$datos['titulo'],ucfirst($elemento) . " $estado",$_SESSION['user'],'','');

            Logger::warn("Cambiado estado del $elemento con id $id y titulo \"" . $datos['titulo'] ."\" - Nuevo estado: $nuevoestado.");

        } else {

            error_log("Error al cambiar el estado al $elemento con id $id y titulo \"" . $datos['titulo'] ."\".", 0);

        }
    }
}

// Actualizar orden de indices / contenidos .
function actualizaOrdenIndice($mysql,$tabla,$idindice,$posicion){
    $idindice = mysqli_real_escape_string($mysql,$idindice);
    $posicion = mysqli_real_escape_string($mysql,$posicion);

    // Obtenemos los datos del indice / contenido.
    $array = indiceContenido($mysql,$tabla,$idindice);
    $datos = mysqli_fetch_array($array);

    if ( $tabla == 'contenido' ){
      $indice = indiceContenido($mysql,'indice',$datos['padre_id']);
      $indiceArray = mysqli_fetch_array($indice);
    }

    $sql = "UPDATE $tabla SET posicion = ".$posicion." WHERE id = ".$idindice;
    if(mysqli_query($mysql,$sql)){
        Logger::warn("$tabla: $idindice - Titulo: \"" . $datos['titulo'] . "\" - Nueva posicion: $posicion.");

        // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
        if ( $tabla == 'contenido' )
          guardarLog($mysql,'ordena',$tabla,$idindice,$datos['titulo'],"Indice id " . $indiceArray['id'] . " - título \"" . $indiceArray['titulo'] . "\" - posición $posicion",$_SESSION['user'],'','');
        else
          guardarLog($mysql,'ordena',$tabla,$idindice,$datos['titulo'],"Posición $posicion",$_SESSION['user'],'','');

    } else {
        error_log("Error al ordenar el indice con id $idindice y titulo \"" . $datos['titulo'] . "\".", 0);
      }
}

// Actualiza idpadre a contenido.
function actualizaIdPadreContenido($mysql,$id,$idpadre){
    $id = mysqli_real_escape_string($mysql,$id);
    $idpadre = mysqli_real_escape_string($mysql,$idpadre);
    $sql = "UPDATE contenido SET padre_id = '".$idpadre."' WHERE id = '".$id."'";

    // Obtenemos los datos del indice / contenido.
    $array = indiceContenido($mysql,'contenido',$id);
    $datos = mysqli_fetch_array($array);

    $indiceAntiguo = indiceContenido($mysql,'indice',$datos['padre_id']);
    $indiceAntiguoArray = mysqli_fetch_array($indiceAntiguo);
    $indiceNuevo = indiceContenido($mysql,'indice',$idpadre);
    $indiceNuevoArray = mysqli_fetch_array($indiceNuevo);


    if(mysqli_query($mysql,$sql)){

        // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
        guardarLog($mysql,'cambia','contenido',$id,$datos['titulo'],'Cambiado id padre ' . $datos['padre_id'] . ' - \'\'' . $indiceAntiguoArray['titulo'] .'\'\' por ' . $idpadre . ' -\'\'' . $indiceNuevoArray['titulo'] . '\'\'',$_SESSION['user'],'','');

        Logger::warn("Actualizado indice padre a contenido. Contenido: $id - Titulo: \"" . $datos['titulo'] . "\" - IndicePadre: $idpadre");
        return "ok";
    } else {
        error_log("Error al actualizar el id padre al contenido. Contenido: $id - Titulo: \"" . $datos['titulo'] . "\" - IndicePadre: $idpadre. " . mysqli_error($mysql),0);
        return "ERROR - ".mysqli_error($mysql);
    }
}
/////////////////////////////////////////////////////////////////////

// Comprobar si indice tiene contenidos.
function compruebaIndiceConContenidos($mysql,$id){
  $id = mysqli_real_escape_string($mysql,$id);
  $sql = "SELECT id FROM contenido WHERE padre_id = $id AND activo = 1";
  $query = mysqli_query($mysql,$sql);
  $array = mysqli_fetch_row($query);
  return $array;
}

// Mostrar indices activos menu lateral izquierdo.
function mostrarIndicesContenidosActivos($mysql,$elemento,$id){
  $id = mysqli_real_escape_string($mysql,$id);

  if ( $elemento == 'indices'){
    $sql = "SELECT * FROM indice WHERE activo = 1 ORDER BY posicion";
    $query = mysqli_query($mysql,$sql);
    return $query;

  } else if ( $elemento == 'contenidos' ) {
    $sql = "SELECT * FROM contenido WHERE padre_id = $id AND activo = 1 ORDER BY posicion";
    $query = mysqli_query($mysql,$sql);
    return $query;
  }
}

/////////////////////////////////////////////////////////////////////
// CONTENDIOS

// Obtener contenido.
function muestraContenido($mysql,$contenido){
  $contenido = mysqli_real_escape_string($mysql,$contenido);

  $sql = "SELECT contenido,ultimo_usuario,ultimo_update FROM contenido WHERE id = $contenido";
  $query = mysqli_query($mysql,$sql);
  //$result = mysqli_fetch_array($query);
  //return $result['contenido'];
  return $query;
}

function actualizaContenido($mysql,$contenidoNuevo,$usuario,$id){
  $contenidoNuevo = mysqli_real_escape_string($mysql,$contenidoNuevo);
  $usuario = mysqli_real_escape_string($mysql,$usuario);
  $id = mysqli_real_escape_string($mysql,$id);

  $query =  "UPDATE `contenido` SET contenido = '$contenidoNuevo', ultimo_usuario = '$usuario', ultimo_update = now() WHERE id = $id";
  if(mysqli_query($mysql,$query))
    return 'ok';
  else
    return 'ERROR - ' . mysqli_error($mysql);

}


/////////////////////////////////////////////////////////////////////
// LOGS

/////////////////////////////////////////////////////////////////////
// Guardar registros de Log.
function guardarLog($mysql,$accion,$elemento,$id_elemento,$titulo_elemento,$comentario,$usuario,$contenido_antiguo,$contenido_nuevo) {
  $accion = mysqli_real_escape_string($mysql,$accion);
  $elemento = mysqli_real_escape_string($mysql,$elemento);
  $id_elemento = mysqli_real_escape_string($mysql,$id_elemento);
  $titulo_elemento = mysqli_real_escape_string($mysql,$titulo_elemento);
  $comentario = mysqli_real_escape_string($mysql,$comentario);
  $usuario = mysqli_real_escape_string($mysql,$usuario);
  $contenido_antiguo = mysqli_real_escape_string($mysql,$contenido_antiguo);
  $contenido_nuevo = mysqli_real_escape_string($mysql,$contenido_nuevo);

  $sql = "INSERT INTO logs (accion,elemento,id_elemento,titulo_elemento,comentario,fecha,usuario,contenido_antiguo,contenido_nuevo) VALUES ('".$accion."','".$elemento."','".$id_elemento."','".$titulo_elemento."','".$comentario."',now(),'".$usuario."','".$contenido_antiguo."','".$contenido_nuevo."')";

  if(mysqli_query($mysql,$sql)){
    Logger::warn("Log guardado.");
  } else {
    error_log("Error al guardar el log " . mysqli_error($mysql), 0);
  }
}
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Obtener logs
function obtenerLogs($mysql,$datei,$datef,$action,$element,$user){
  $datei = mysqli_real_escape_string($mysql,$datei);
  $datef = mysqli_real_escape_string($mysql,$datef);
  $action = mysqli_real_escape_string($mysql,$action);
  $element = mysqli_real_escape_string($mysql,$element);
  $user = mysqli_real_escape_string($mysql,$user);

  $today = date("Y-m-d G:i:s");
  $lastmonth = date("Y-m-d G:i:s", strtotime("-1 months"));

  if ( $datei == '' AND $datef != '' )
    $datequery = "fecha <= '$datef'";
  else if ( $datei != '' AND  $datef == '' )
    $datequery = "fecha >= '$datei'";
  else if ( $datei != '' AND $datef != '' )
    $datequery = "fecha BETWEEN '$datei' AND '$datef'";
  else
    //$datequery = "fecha LIKE '%'";
    $datequery = "fecha BETWEEN '$lastmonth' AND '$today'";


  if ( $action == '' )
    $actionquery = "accion LIKE '%'";
  else
    $actionquery = "accion = '$action'";

  if ( $element == '')
    $elementquery = "elemento LIKE '%'";
  else
    $elementquery = "elemento = '$element'";

  if ( $user == '0' )
    $userquery = "usuario LIKE '%'";
  else
    $userquery = "usuario LIKE '%$user%'";



  $sql = "SELECT * FROM logs WHERE $actionquery AND $elementquery AND $userquery AND $datequery ORDER BY fecha DESC";
  $query = mysqli_query($mysql,$sql);
  return $query;

}

/////////////////////////////////////////////////////////////////////
// Obtener contenido para logs
function obtenerContenidoLogs($mysql,$id,$content){
  $id = mysqli_real_escape_string($mysql,$id);
  $content = mysqli_real_escape_string($mysql,$content);

  $sql = "SELECT $content FROM logs WHERE id = $id";
  $query = mysqli_query($mysql,$sql);
  $array = mysqli_fetch_row($query);
  return $array[0];
}
?>
