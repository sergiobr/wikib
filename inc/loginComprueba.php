<?php
// Pagina de login.
//Comprobamos si se llama desde el index.php
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/loginComprueba.php";
if (!strpos($url,$patern)) {

  // Nos conectamos a la db uasndo la siguiente funcion.
  $mysql = conectar_db();

  // Confirmamos la conexion a la db. Si falla mostramos un popup con el error.
  if (is_string($mysql) and preg_match("#^ERROR - #",$mysql)) {
    echo '<script>alert("'.$mysql.'");</script>';

    } else {

    // Si la conexion a la db es correcta, comprobamos el login.
    Logger::info("Conectado a la base de datos correctamente. Comprobando login.");
    $usuario = $_POST['usuario'];
    $pwd = $_POST['passwordcrypt'];
    $ipusuario = $_SERVER['REMOTE_ADDR'];

    // Llamamos a la funcion que consulta la db de usuarios para el login.
    $array = compruebalogin($mysql,$usuario,$pwd);

    // Si la consulta SQL para validar al usuario responde OK creamos las sesiones de usuario.
    if(isset($array['id'])){

      Logger::info("Login correcto para el usuario $usuario.");
      $_SESSION=array("id"=>$array['id'],"user"=>$array['usuario'],"name"=>$array['nombre'],"email"=>$array['email'],"group"=>$array['grupo'],"ip"=>$ipusuario);
      // Guardos fecha e IP del ultimo login.
      $fechaLogin = date('Y-m-d H:i:s');
      $ipLogin = $_SERVER['REMOTE_ADDR'];
      $actualizaUltimoAcceso = ultimologin($mysql,$usuario,$fechaLogin,$ipLogin);
      if($actualizaUltimoAcceso == 'ok')
        Logger::info("Actualizada fecha e IP del ultimo login realizado para el usuario $usuario.");
      else
        error_log("Datos del ultimo login no actualizados para el usuario $usuario.",0);
    } else {
    // De lo contrario si las credenciales de usuario no son correctas mostramos un popup con el error de credenciales.
      error_log("Autenticacion fallida - Usuario: ".$usuario." - IP: ".$ipusuario.".", 0);
      echo "<script>alert('Usuario y/o contraseña incorrectos.');</script>";
    }
    // Cerramos la conexion a la db tras hacer login.
    mysqli_close($mysql);
  }
  // Volvemos a la URL anterior después de pulsar el botón de login.
  echo "<script>volver();</script>";
} else {
  // Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
  echo "<div style='margin: 20px 10px;'>";
    echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
    echo "<p>La ventana de login solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
  echo "</div>";
  error_log("Acceso a la ventana de login no permitido. No se esta accediendo desde el index.");
}
?>
