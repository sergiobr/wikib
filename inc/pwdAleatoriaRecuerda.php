<?php
// Pagina de login.
//Comprobamos si se llama desde el index.php
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/pwdAleatoriaRecuerda.php";
  if (!strpos($url,$patern)) {

  // Nos conectamos a la db uasndo la siguiente funcion.
  $mysql = conectar_db();

  // Confirmamos la conexion a la db. Si falla mostramos un popup con el error.
  if (is_string($mysql) and preg_match("#^ERROR - #",$mysql)) {
    echo '<script>alert("'.$mysql.'");</script>';

    } else {
    // Si la conexion a la db es correcta, confirmamos datos de usuario.
    Logger::info("Conectado a la base de datos correctamente. Comprobando datos para recuerdo de pwd.");
    $usuario = $_POST['usuario2'];
    $email = $_POST['email'];

    // Comprobamos si los datos introducidos por el usuario son correctos.
    $array = compruebauseremail($mysql,$usuario,$email);

    // Llamamos a la funcion que consulta la db de usuarios para confirmar datos.
    // Si son corretos enviamos el email con un enlace para generar una pwd aleatoria.
    if(isset($array['id'])){
      Logger::info("Datos para la recuperacion de pwd del usuario $usuario con email $email correctos.");

      // Enviar email con enlace temporal para generar pwd aleatoria.
      // Generamos un valor aleatorio unico.
      $urlrand = md5(uniqid());

      // Guardamos la cadena aleatoria para el usuario que solicita la nueva credencial en Redis
      // Primero nos conectamos a la db de Redis.
      // Creamos la variable con los datos de acceso parseados.
      $config = parseaconf("conf/access.ini","1");
      $hostr = $config['redis']['servernameredis'];
      $portr = $config['redis']['portredis'];
      $dbr = $config['redis']['dbredis'];
      if( $config['redis']['authredis'] != 'nul' OR $config['redis']['authredis'] != "" )
        $authr = $config['redis']['authredis'];
      else
        $authr = "";

        // Eliminamos la variable con los datos de acceso parseados.
      unset($config);

      // Conexion a Redis.
      $redis = rediscon($hostr,$portr,$dbr,$authr);

      // Confirmamos que no hay ningun error de conexion.
      if ( is_string($redis) and preg_match("#^ERROR - #",$redis) ){
        echo "<script>alert('$redis');</script>";

      } else {
        // Guardamos la URL generada en Redis y la enviamos por correo.
        $redis->set($array['usuario'],$urlrand);
        //Le asignamos un tiempo de expiracion de 5 minutos a esta URL.
        $redis->setTimeout($array['usuario'], 300);
        $enlacepwd = $_SERVER['HTTP_REFERER'].'?recuperapwd='.$array['usuario'].':'.$urlrand;
        $destinatario = $array['email'];

        // Enviamos el correo a los datos del usuario almacenamos en el sistema con la URL para recuperar la pwd.
        enviarcorreo($enlacepwd,$destinatario,'generaEnlace');
      }

    } else {
      // De lo contrario si los datos de usuario y email no son correctas mostramos un popup con el error.
      error_log("Recordatorio de pwd fallida, los datos introducidos no pertenecen a ningun usuario del sistema - Usuario: ".$usuario." - Email: ".$email.".",0);
      echo '<script>alert("Error al indicar usuario '.$usuario.' y email asociado '.$email.'");</script>';
    }

    // Cerramos la conexion a la db tras hacer login.
    mysqli_close($mysql);
  }

  // Volvemos a la URL anterior después de pulsar el botón de login.
  echo "<script>volver();</script>";

} else {
  // Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
  echo "<div style='margin: 20px 10px;'>";
    echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
    echo "<p>La ventana de login solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
  echo "</div>";
  error_log("Acceso a la ventana de login no permitido. No se esta accediendo desde el index.");
}
?>
