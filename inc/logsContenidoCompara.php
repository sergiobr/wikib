<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}


// Incluimos el fichero con las funciones.
include(dirname(__FILE__)."/../fun/funciones.php");

// Función para conectarnos a la base de datos.
function conectar_db2(){
  // Incluimos el fichero con los datos de configuracion de la plataforma.
  $conf = parse_ini_file(dirname(__FILE__)."/../conf/access.ini",TRUE);
  // Nos conectamos al MySQL, ya que desde el fichero de funciones falla al no ser llamado desde el index.
  $mysql = mysqli_connect($conf['mysql']['servername'],$conf['mysql']['username'],$conf['mysql']['password'],$conf['mysql']['dbname'],$conf['mysql']['portmysql']);
  mysqli_set_charset($mysql,"utf8");
  unset($conf);
  return $mysql;
}

// Nos conectamos al MySQL usando la funcion anterior.
$mysql = conectar_db2();

// Obtenemos las variables pasadas por el AJAX de JQUERY.
$id = $_GET['id'];
$content = $_GET['content'];

// Obtenemos el contenido que le pasamos por AJAX, nuevo o antiguo.
$contenido = obtenerContenidoLogs($mysql,$id,$content);

// Devolvemos el resultado.
echo $contenido;

?>
