<?php

$mysql = conectar_db();

// Si estamos navegando por algun contenido, lo mostramos.
if (isset($_GET['indice'])){
  $indiceId = $_GET['indice'];
  $contenidoId = $_GET['contenido'];

  // Obtenemos el contenido almacenado en MySQL.
  $muestraContenidoQuery = muestraContenido($mysql,$contenidoId);
  $muestraContenido = mysqli_fetch_array($muestraContenidoQuery);

  // Obtenemos la fecha.
  if($muestraContenido['ultimo_update'] != NULL)
    $fecha = date('M j Y g:i A', strtotime($muestraContenido['ultimo_update']));
  else
    $fecha = "";

  // Mostramos una breve cabecera del contenido.
  echo "<div id='cabeceraContenido' class='cabeceraContenido'>";

  // Si hemos iniciado sesion mostramos el boton de edicion.
    if (isset($_SESSION['id'])){
      echo "<img id='editaContenido' src='img/editicon.png' title='Editar el contenido actual.' />";
    }

    echo "<p>&Uacute;ltima actualizaci&oacute;n: <b>" . $fecha . "</b></p>";
    echo "<p>Usuario: <b>" . $muestraContenido['ultimo_usuario'] . "</b></p>";
  echo "</div>";


  // Si hemos iniciado sesion agregamos el editor HTML.
  if (isset($_SESSION['id'])){
    echo '
    <div class="editor">
    <form method="post" name="tinymce" id="tinymce">

       <textarea name="texto" id="texto">' . $muestraContenido['contenido'] . '</textarea>
       <input type="hidden" id="contenidooculto" name="contenidooculto" />
       <input style="display: none;" id="img" type="file" name="img"  />
       <br><button type="button" id="botoncontenido" onclick="actualizaContenido('.$contenidoId.',\''.$_SESSION['user'].'\')">Guarda</button>
       <button type="button" id="cerrarEditor">Cancelar</button>

    </form>
    </div>
    ';
  }

  // Mostramos el contenido.
  echo "<div id='muestraContenido'>";
    echo $muestraContenido['contenido'];
  echo "</div>";


} else {
  // Si no estamos navegando por algun contenido mostramos un indice general.
  echo "<h1>Indice y contenidos activos</h1>";

  echo "<div class='indiceGeneral'>";
  // Obtenemos los indices activos.
  $indicesActivos = mostrarIndicesContenidosActivos($mysql,'indices','none');

  // Recorremos todos los indices obtenidos.
  while($array = mysqli_fetch_array($indicesActivos)){

      echo "<h2>" . $array['titulo'] . "</h2>";

      // Confirmamos si tiene algun contenido activo asociado.
      $contenidoExiste = compruebaIndiceConContenidos($mysql,$array['id']);

      // Si lo tuviera los pintamos en un desplegable.
      if (isset($contenidoExiste)){

        // Recorremos los contenidos activos para el indice.
        $contenidosActivos = mostrarIndicesContenidosActivos($mysql,'contenidos',$array['id']);

        while ($array2 = mysqli_fetch_array($contenidosActivos)){

          echo "<ul class='list-conf'>";
            echo "<li><a onclick='compruebaExpande(" . $array['id'] . "," . $array2['id'] . ")' href='?indice=".$array['id']."&contenido=".$array2['id']."'>" . $array2['titulo'] . "</a></li>";
          echo "</ul>";

        }
      }

  }

echo "</div>";

}

?>
