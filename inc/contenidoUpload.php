<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}

  /*******************************************************
   * Only these origins will be allowed to upload images *
   ******************************************************/

   isset($_SERVER['HTTPS']) ? $protocol = "https" : $protocol = "http";
   $url = $protocol."://".$_SERVER['HTTP_HOST'];

   $accepted_origins = $url;
  //$accepted_origins = array("http://localhost", "http://192.168.1.1", "http://example.com");

  /*********************************************
   * Change this line to set the upload folder *
   *********************************************/
  $imageFolder = "../img/subidas/";

  reset ($_FILES);
  $temp = current($_FILES);
  if (is_uploaded_file($temp['tmp_name'])){
//    if (isset($_SERVER['HTTP_ORIGIN'])) {
//      // same-origin requests won't set an origin. If the origin is set, it must be valid.
//      if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
//        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
//      } else {
//        header("HTTP/1.0 403 Origin Denied");
//        return;
//      }
//    }

    /*
      If your script needs to receive cookies, set images_upload_credentials : true in
      the configuration and enable the following two headers.
    */
    // header('Access-Control-Allow-Credentials: true');
    // header('P3P: CP="There is no P3P policy."');

    $error = [];

    // Sanitize input
    if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
        //header("HTTP/1.0 500 Invalid file name.");
        $error['sanitize'] = 'Nombre de archivo no permitido.';
        //return;
    }

    // Verify extension
    if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
        //header("HTTP/1.0 500 Invalid extension.");
        $error['extension'] = 'Extension no permitida.';
        //return;
    }

    // Confirmamos size del archivo menor a 5MB.
    if ( $temp['size'] > 2000000 ) {
        //header("HTTP/1.0 500 Invalid size.");
        $error['size'] = 'Size de archivo superior a 5MB.';
        //return;
    }

    // Accept upload if there was no origin, or if it is an accepted origin.
    // Renombramos el archivo para que su nombre sea unico.
    $fileparts = pathinfo($temp['name']);
    $time = time() . "-" . mt_rand();
    $file = $fileparts['filename'];
    $extension = $fileparts['extension'];
    $filetowrite = $imageFolder . $file . "-" . $time . "." . $extension;

    // Confirmamos si el archivo renombrado ya existe.
    if(file_exists($filetowrite)){
      //header("HTTP/1.0 500 File exists.");
      $error['exists'] = 'El archivo ya existe.';
      //return;
    }

    // Si no hay errores subimos el archivo.
    if(empty($error)){
        move_uploaded_file($temp['tmp_name'], $filetowrite);

        // Respond to the successful upload with JSON.
        // Use a location key to specify the path to the saved image resource.
        // { location : '/your/uploaded/image/file'}
        $result = array('filename' => $file . "-" . $time . "." . $extension);
        echo json_encode($result);
        //echo json_encode(array('location' => $filetowrite));
    } else {

        // Devolvemos los errores.
        echo json_encode($error);

    }

  } else {
    // Notify editor that the upload failed
    //header("HTTP/1.0 500 Server Error");
    $error['exists'] = 'El archivo ya existe.';
  }
?>
