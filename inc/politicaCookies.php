<div id="politica-cookies" class="politica-cookies">
  <div id="contenido-politica-cookies" class="contenido-politica-cookies scroll-up">
    <button onclick="volver()">Volver</button>
    <br />
    <hr />
  <h1>POLITICA DE COOKIES</h1>

  <p>Cookie es un fichero que se descarga en su ordenador al acceder a determinadas p&aacute;ginas web. Las cookies permiten a una p&aacute;gina web, entre otras cosas, almacenar y recuperar informaci&oacute;n sobre los h&aacute;bitos de navegaci&oacute;n de un usuario o de su equipo y, dependiendo de la informaci&oacute;n que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.. El navegador del usuario memoriza cookies en el disco duro solamente durante la sesi&oacute;n actual ocupando un espacio de memoria m&iacute;nimo y no perjudicando al ordenador. Las cookies no contienen ninguna clase de informaci&oacute;n personal espec&iacute;fica, y la mayor&iacute;a de las mismas se borran del disco duro al finalizar la sesi&oacute;n de navegador (las denominadas cookies de sesi&oacute;n).</p>

  <p>La mayor&iacute;a de los navegadores aceptan como est&aacute;ndar a las cookies y, con independencia de las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o memorizadas.</p>

  <p>Sin su expreso consentimiento &ndash;mediante la activaci&oacute;n de las cookies en su navegador&ndash;<strong>WikiB</strong> no enlazar&aacute; en las cookies los datos memorizados con sus datos personales proporcionados en el momento del registro o la compra..</p>

  <h1>&iquest;Qu&eacute; tipos de cookies utiliza esta p&aacute;gina web?</h1>

  <p>- Cookies t&eacute;cnicas: Son aqu&eacute;llas que permiten al usuario la navegaci&oacute;n a trav&eacute;s de una p&aacute;gina web, plataforma o aplicaci&oacute;n y la utilizaci&oacute;n de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tr&aacute;fico y la comunicaci&oacute;n de datos, identificar la sesi&oacute;n, acceder a partes de acceso restringido, recordar los elementos que integran un pedido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripci&oacute;n o participaci&oacute;n en un evento, utilizar elementos de seguridad durante la navegaci&oacute;n, almacenar contenidos para la difusi&oacute;n de videos o sonido o compartir contenidos a trav&eacute;s de redes sociales.</p>

  <p>- Cookies de personalizaci&oacute;n: Son aqu&eacute;llas que permiten al usuario acceder al servicio con algunas caracter&iacute;sticas de car&aacute;cter general predefinidas en funci&oacute;n de una serie de criterios en el terminal del usuario como por ejemplo serian el idioma, el tipo de navegador a trav&eacute;s del cual accede al servicio, la configuraci&oacute;n regional desde donde accede al servicio, etc.</p>

  <p>- Cookies de an&aacute;lisis: Son aqu&eacute;llas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el n&uacute;mero de usuarios y as&iacute; realizar la medici&oacute;n y an&aacute;lisis estad&iacute;stico de la utilizaci&oacute;n que hacen los usuarios del servicio ofertado. Para ello se analiza su navegaci&oacute;n en nuestra p&aacute;gina web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.</p>

  <p>- Cookies publicitarias: Son aqu&eacute;llas que, bien tratadas por nosotros o por terceros, nos permiten gestionar de la forma m&aacute;s eficaz posible la oferta de los espacios publicitarios que hay en la p&aacute;gina web, adecuando el contenido del anuncio al contenido del servicio solicitado o al uso que realice de nuestra p&aacute;gina web. Para ello podemos analizar sus h&aacute;bitos de navegaci&oacute;n en Internet y podemos mostrarle publicidad relacionada con su perfil de navegaci&oacute;n.</p>

  <p>- Cookies de publicidad comportamental: Son aqu&eacute;llas que permiten la gesti&oacute;n, de la forma m&aacute;s eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una p&aacute;gina web, aplicaci&oacute;n o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan informaci&oacute;n del comportamiento de los usuarios obtenida a trav&eacute;s de la observaci&oacute;n continuada de sus h&aacute;bitos de navegaci&oacute;n, lo que permite desarrollar un perfil espec&iacute;fico para mostrar publicidad en funci&oacute;n del mismo.</p>

  <p>El Usuario acepta expresamente, por la utilizaci&oacute;n de este Site, el tratamiento de la informaci&oacute;n recabada en la forma y con los fines anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o informaci&oacute;n rechazando el uso de Cookies mediante la selecci&oacute;n de la configuraci&oacute;n apropiada a tal fin en su navegador. Si bien esta opci&oacute;n de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website.</p>

  <p>Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuraci&oacute;n de las opciones del navegador instalado en su ordenador:</p>

  <a href="https://support.google.com/accounts/answer/61416?hl=es" target="_blank">Chrome</a><br />
  <a href="https://support.microsoft.com/es-es/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank">Explorer</a><br />
  <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias" target="_blank">Firefox</a><br />
  <a href="https://support.apple.com/kb/ph21411?locale=es_ES" target="_blank">Safari</a><br />
  <a href="http://help.opera.com/Linux/10.60/es-ES/cookies.html" target="_blank">Opera</a>

  <p>Si tiene dudas sobre esta política de cookies, puede contactar con el administrador en <a href="mailto:<?php $email = obtenemailadmin(); echo $email; ?>?Subject=Duda%20sobre%20cookies" target="_top"><?php echo $email;?></a></p>
  <hr />
  <button onclick="volver()">Volver</button>
  <button onclick="topFunction()" id="topBtn" title="Ir al inicio">Inicio</button>
  <br />
  </div>
</div>
