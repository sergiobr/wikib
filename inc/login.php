<?php
  // Pagina de login.
  //Comprobamos si se llama desde el index.php
  $url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
  $patern = "inc/login.php";
  if (!strpos($url,$patern)) {
    // Si se muestra el contenido tras darle al boton de login en index.php mostramos el contenido.
    ?>
      <div id='login-usuario' class='form-oculto'>
        <div id="login-form">
          <button id="botonCerrarLogin" class="botonCerrarLogin">&#10006;</button>
          <h4 style="margin: 10px auto;">Login de usuario</h4>
          <form class="login" id="loginUsuario" name="loginUsuario" method="post" action="<?php $_SERVER['PHP_SELF']; ?>" onsubmit="">
            <input type="text" id="usuario" name="usuario" placeholder="Usuario" required autofocus>
            <input type="password" id="password" name="password" placeholder="Password" required oncopy="return false" oncut="return false"><br />
            <img src="img/eye.png" class="eye" onmouseover="muestraPwd('password','none')" onmouseout="ocultaPwd('password','none')" >
            <input type="hidden" id="passwordcrypt" name="passwordcrypt">
            <br>
            <button id="boton-login" type="submit">Login</button>
            <button type="reset">Borrar</button>
          </form>
          <p id="recordarPwd">Recordar contrase&ntilde;a</p>
          <form id="recordar" name="recordar" method="post" action="<?php $_SERVER['PHP_SELF']; ?>" onsubmit="">
            <input type="text" id="usuario2" name="usuario2" placeholder="Usuario" required />
            <input type="email" id="email" name="email" placeholder="Email" required />
            <br />
            <button id="boton-recordar" type="submit">Enviar</button>
            <button type="reset">Borrar</button>
          </form>
        </div>
      </div>
    <?php
    } else {
  // Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
    echo "<div style='margin: 20px 10px;'>";
      echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
      echo "<p>La ventana de login solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
    echo "</div>";
    error_log("Acceso a la ventana de login no permitido. No se esta accediendo desde el index.");
  }
?>
