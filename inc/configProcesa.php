<?php
//Comprobamos si se llama desde el index.php
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/procesaconfig.php";
if (!strpos($url,$patern)) {
?>

<div id="conf-procesado">
<?php

Logger::info("Procesando datos de configuracion enviados desde el formulario.");
// Inicializamos un array con los errores.
$configerrores= array();
// Parseamos la configuracion de los archivos .ini
$config = parseaconf("conf/access.ini",1);
$config2 = parseaconf("conf/conf.ini",1);

echo "<h1>Valores</h1>";
echo "<br />";

// MySQL
// ####################
echo "<h2>MySQL:</h2>";
// Si esta marcado el check procesamos datos formulario, sino lo ignoramos.
if(isset($_POST['conf-mysql'])){
  // Confirmamos si el módulo mysqli para PHP esta instalado.
  if(!extension_loaded('mysqli')){
    // Si el modulo no esta cargado.
    $configerrores['mysql'] = "ERROR - El modulo MySLQi para PHP no esta configurado o tienes configurado el modulo MySQL.";
    echo "<p>Resultado: <span class='resultado-ko'>ERROR - El m&oacute;dulo MySLQi para PHP no esta configurado o tienes configurado el m&oacute;dulo MySQL.</span></p>";
    error_log('ERROR - El modulo MySLQi para PHP no esta configurado o tienes configurado el modulo MySQL.', 0);
    $dbconfig = 0;
  } else {
      Logger::info('Modulo MySQLi para PHP instalado.');
      // Si el modulo esta cargado.
      echo "<p>Host: ".$_POST['conf-mysqlservername']."</p>";
      echo "<p>Usuario: ".$_POST['conf-mysqlusername']."</p>";
      echo "<p>Pwd: ".$_POST['conf-mysqlpwd']."</p>";
      echo "<p>Base de datos: ".$_POST['conf-mysqldbname']."</p>";
      echo "<p>Puerto: ".$_POST['conf-mysqlport']."</p>";
      // Confirmamos conectividad a la DB MySQL. actualiza_db($servername,$username,$password,$dbname,$portmysql)
      $mysqlcheck = actualiza_db($_POST['conf-mysqlservername'],$_POST['conf-mysqlusername'],$_POST['conf-mysqlpwd'],$_POST['conf-mysqldbname'],$_POST['conf-mysqlport']);
      if(is_string($mysqlcheck) and preg_match("#^ERROR - #",$mysqlcheck)){
        // Si da error la conexion, almacenamos este en el array.
        $configerrores['mysql'] = $mysqlcheck;
        echo "<p>Resultado: <span class='resultado-ko'>$mysqlcheck</span></p>";
        $dbconfig = 0;
        error_log('ERROR - Datos de configuracion para MySQL no actualizados.', 0);
      } else {
        echo "<p>Resultado: <span class='resultado-ok'>Correcto</span>.</p>";

        // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
        if( $config2['confinicial']['conf'] != 0 )
          guardarLog($mysql,'cambia','configuración','-','MySQL','Cambiada configuración de MySQL.',$_SESSION['user'],'','');

        $dbconfig = 1;
        Logger::info('Datos de configuracion para MySQL actualizados.');
        $mysqlservername = $_POST['conf-mysqlservername'];
        $mysqlusername = $_POST['conf-mysqlusername'];
        $mysqlpwd = $_POST['conf-mysqlpwd'];
        $mysqldbname = $_POST['conf-mysqldbname'];
        $mysqlport = $_POST['conf-mysqlport'];
      }
  }
} else {
  $mysqlservername = $config['mysql']['servername'];
  $mysqlusername = $config['mysql']['username'];
  $mysqlpwd = $config['mysql']['password'];
  $mysqldbname = $config['mysql']['dbname'];
  $mysqlport = $config['mysql']['portmysql'];
  echo "<p>MySQL sin datos para actualizar.</p>";
  $dbconfig = 2;
  Logger::info('Sin datos para actualizar la configuracion MySQL.');
}

// Carga inicial de tablas en MySQL.

// Si la configuracion inicial no se habia cargado y los chequeos fueron correctos, generamos le log y cargamos las tablas de la base de datos MySQL.
if ( $config2['confinicial']['conf'] == 0 AND $dbconfig == 1 ){

  // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
  $mysql = conectar_db_inicial($_POST['conf-mysqlservername'],$_POST['conf-mysqlusername'],$_POST['conf-mysqlpwd'],$_POST['conf-mysqldbname'],$_POST['conf-mysqlport']);

  if( $config2['confinicial']['conf'] != 0 )
    guardarLog($mysql,'crea','configuración','-','Configuración inicial','Creada configuración inicial del Wiki.',$_SESSION['user'],'','');

  // Cargamos las tablas en la base de datos durante la configuracion inicial.
  $cargaInicial = cargaTablasInicial($mysql);

  // Si la carga inicial fue correcta pintamos el resultado y si no pintamos el error.
  echo "<br />";
  echo "<h2>Carga inicial MySQL.</h2>";

  if ( $cargaInicial == 'ok' ){

    echo "<p>Resultado: <span class='resultado-ok'>Carga inicial MySQL correcta.</span></p>";
    Logger::info('Configuracion inicial realizada.');

  } else {

    echo "<p>Resultado: <span class='resultado-ko'>" . $cargaInicial . "</span></p>";
    $configerrores['cargaMySQLinicial'] = $cargaInicial;
    error_log($cargaInicial, 0);

  }
}






echo "<br />";

// Redis.
// ###########
echo "<h2>Redis:</h2>";
// Si esta marcado el check procesamos datos formulario, sino lo ignoramos.
if(isset($_POST['conf-redis'])){
  // Confirmamos si el módulo mysqli para PHP esta instalado.
  if(!extension_loaded('mysqli')){
    // Si el modulo no esta cargado.
    $configerrores['redis'] = "ERROR - El modulo Redis para PHP no esta configurado.";
    echo "<p>Resultado: <span class='resultado-ko>ERROR - El modulo Redis para PHP no esta configurado.</span></p>";
    error_log('ERROR - El modulo Redis para PHP no esta configurado.', 0);
    $redisconfig = 0;
    } else {
      Logger::info('Modulo Redis para PHP instalado.');
      // Si el modulo esta cargado.
      echo "<p>Host: ".$_POST['conf-redisservername']."</p>";
      echo "<p>Pwd: ".$_POST['conf-redisauth']."</p>";
      echo "<p>Base de datos: ".$_POST['conf-redisdb']."</p>";
      echo "<p>Puerto: ".$_POST['conf-redisport']."</p>";
      // Confirmamos conectividad a la DB Redis.
      $redischeck = rediscon($_POST['conf-redisservername'],$_POST['conf-redisport'],$_POST['conf-redisdb'],$_POST['conf-redisauth']);
      if(is_string($redischeck) and preg_match("#^ERROR - #",$redischeck)){
        // Si da error la conexion, almacenamos este en el array.
        $configerrores['redis'] = $redischeck;
        echo "<p>Resultado: <span class='resultado-ko'>$redischeck</span>.</p>";
        $redisconfig = 0;
        error_log('ERROR - Datos de configuracion para Redis no actualizados.', 0);
      } else {
        echo "<p>Resultado: <span class='resultado-ok'>Correcto</span>.</p>";
        $redisconfig = 1;

        // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
        if( $config2['confinicial']['conf'] != 0 )
          guardarLog($mysql,'cambia','configuración','-','Redis','Cambiada configuración de Redis.',$_SESSION['user'],'','');

        Logger::info('Datos de configuracion para Redis actualizados.');
        $redisservername = $_POST['conf-redisservername'];
        $redisauth = $_POST['conf-redisauth'];
        $redisdb = $_POST['conf-redisdb'];
        $redisport = $_POST['conf-redisport'];
    }
  }
} else {
  echo "<p>Redis sin datos para actualizar.</p>";
  $redisconfig = 2;
  $redisservername = $config['redis']['servernameredis'];
  $redisauth = $config['redis']['authredis'];
  $redisdb = $config['redis']['dbredis'];
  $redisport = $config['redis']['portredis'];
  Logger::info('Sin datos para actualizar la configuracion Redis.');
}
echo "<br />";

// Usuario Admin.
// ###################
echo "<h2>Admin user:</h2>";
// Si esta marcado el check procesamos datos formulario, sino lo ignoramos.
if(isset($_POST['conf-admin'])){
  echo "<p>Nombre: ".$_POST['conf-nameadmin']."</p>";
  echo "<p>Usuario: ".$_POST['conf-useradmin']."</p>";
  echo "<p>Pwd: ".$_POST['conf-passwordadmin']."</p>";
  echo "<p>Email: ".$_POST['conf-emailadmin']."</p>";
  // Si MySQL no respondio ok mostramos el error, de lo contrario actualizamos.
  if($dbconfig == 0){
    // Si el MySQL fallo no actualizamos las credenciales.
    $configerrores['admin'] = "ERROR - Credenciales de administrador no actualizadas por problemas al actualizar las credenciales de MySQL.";
    error_log("ERROR - Credenciales de administrador no actualizadas por problemas al actualizar las credenciales de MySQL.", 0);
    echo "<p>Resultado: <span class='resultado-ko'>ERROR - Credenciales de administrador no actualizadas por problemas al actualizar las credenciales de MySQL</span>.</p>";
    $adminconfig = 0;
  } else {
    // Creamos la conexion al MySQL y las variables en funcion de si se actualizaron los datos MySQL en el mismo formulario.
    if($dbconfig == 1){
      // Si se actualizaron nos conectamos en funcion de lo indicado en el formulario.
      $adminmysql = $mysqlcheck;
    } elseif($dbconfig == 2){
      // Si no se actualizaron nos conectamos consultando los datos del arhivo .ini
      $adminmysql = conectar_db();
    }
    // Confirmamos la conexion al MySQL.
    if (is_string($adminmysql) and preg_match("#^ERROR - #",$adminmysql)) {
      echo '<script>alert("'.$adminmysql.'");</script>';
      $configerrores['admin'] = "ERROR - Usuario administracion, error al conectarse a MySQL: ".$adminmysql;
      echo "<p>Resultado: <span class='resultado-ko'>ERROR -  Error al conectar con MySQL, $adminmysql</span>.</p>";
      $adminconfig = 0;
      error_log('ERROR - Datos de configuracion para el usuario Admin no actualizados por problemas al conectarse con MySQL.', 0);
    } else {

      // Comprobamos si existe algun usuario en el grupo administrador y de existir si coincide o no con el indicado en el formulario.
      $existeadmin = compruebaadmin($adminmysql);
      if((isset($existeadmin['usuario'])) && ($existeadmin['usuario'] != strtolower($_POST['conf-useradmin']))){
        // Si el usuario en el grupo administrador ya existe no actualizamos ni creamos nada.
        echo "<p>Resultado: <span class='resultado-ko'>Error - El usuario admin (".$existeadmin['usuario'].") ya existe.</span></p>";
        $configerrores['admin'] = "ERROR - El usuario admin (".$existeadmin['usuario'].") ya existe.";
        $adminconfig = 0;
        error_log("ERROR - El usuario admin (".$existeadmin['usuario'].") ya existe.");
      } else {
        // Si no existe un usuario en el grupo administrador o si este coincide con el introducido en el formulario, creamos/actualizamos.
        // Actualizamos la pwd de admin si el usuario existe, y si no existe lo creamos.
        $usuarioadmin = compruebauser($adminmysql,$_POST['conf-useradmin']);
        // Si existe el usuario actualizamos sus datos.
        if(isset($usuarioadmin['id'])){
          $actualiza = actualizausuario($adminmysql,$_POST['conf-nameadmin'],$_POST['conf-useradmin'],$_POST['conf-passwordadmin'],$_POST['conf-emailadmin'],"administrador",'1');
          // Comprobamos el resultado de la actualizacion.
          if($actualiza == "ok"){
            echo "<p>Resultado: <span class='resultado-ok'>Correcto, usuario administrador ".$_POST['conf-useradmin']." actualizado</span>.</p>";
            $adminconfig = 1;

            // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
            guardarLog($mysql,'actualiza','configuración','-','Administrador','Actualizada configuración para usuario de administración.',$_SESSION['user'],'','');

            $nameadmin = $_POST['conf-nameadmin'];
            $useradmin = $_POST['conf-useradmin'];
            $passwordadmin = $_POST['conf-passwordadmin'];
            $emailadmin = $_POST['conf-emailadmin'];
            Logger::info("Datos de configuracion para el usuario ".$_POST['conf-useradmin']." actualizados.");
          } else {
            $configerrores['admin'] = $actualiza;
            echo "<p>Resultado: <span class='resultado-ko'>ERROR - Error al actualizar el usuario administrador ".$_POST['conf-useradmin'].", $actualiza</span>.</p>";
            $adminconfig = 0;
            error_log("ERROR - Datos de configuracion para el usuario Admin ".$_POST['conf-useradmin']." no actualizados.", 0);
          }
        } else {
          // Si no existe el usuario lo creamos.
          $crea = creausuario($adminmysql,$_POST['conf-nameadmin'],$_POST['conf-useradmin'],$_POST['conf-passwordadmin'],$_POST['conf-emailadmin'],"administrador","1",0,"conf/conf.ini");
          // Comprobamos el resultado de la creacion.
          if($crea == "ok"){
            echo "<p>Resultado: <span class='resultado-ok'>Correcto, usuario administrador ".$_POST['conf-useradmin']." creado</span>.</p>";
            $adminconfig = 1;

            // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
            if( $config2['confinicial']['conf'] != 0 )
              guardarLog($mysql,'cambia','configuración','-','Administrador','Cambiada configuración para usuario de administración.',$_SESSION['user'],'','');

              $nameadmin = $_POST ['conf-nameadmin'];
              $useradmin = $_POST['conf-useradmin'];
              $passwordadmin = $_POST['conf-passwordadmin'];
              $emailadmin = $_POST['conf-emailadmin'];
              Logger::info('Usuario Admin '.$_POST['conf-useradmin'].' creado.');
            } else {
              $configerrores['admin'] = $crea;
              echo "<p>Resultado: <span class='resultado-ko'>ERROR - Error al crear el usuario administrador ".$_POST['conf-useradmin'].",  ".$crea."</span>.</p>";
              $adminconfig = 0;
              error_log('ERROR - Datos de configuracion para el usuario Admin '.$_POST['conf-useradmin'].' no creados.', 0);
            }
          }
        }
      }
    }
  } else {
    echo "<p>Administrador sin datos para actualizar.</p>";
    $adminconfig = 2;
    $nameadmin = $config['admin']['nameadmin'];
    $useradmin = $config['admin']['useradmin'];
    $passwordadmin = $config['admin']['passwordadmin'];
    $emailadmin = $config['admin']['emailadmin'];
    Logger::info('Sin datos para actualizar/crear la configuracion del usuario Admin.');
  }


echo "<br />";

// Correo.
// ############
echo "<h2>Correo:</h2>";
// Si esta marcado el check procesamos datos formulario, sino lo ignoramos.
if(isset($_POST['conf-email'])){

  // Comprobamos si el PHPMailer esta instalado.
  //$phpmailerinstall = compruebaPHPMailerConf();
  //if($phpmailerinstall == 'ok'){
  if ( $phpmailerinstall == 'ok' ) {
      Logger::info('PHPMailer instalado correctamente.');
      // Comprobamos la funcion del PHPMailer.
      $phpmailer = compruebaPHPMailer();
      if($phpmailer == 'ko'){
        $configerrores['email'] = "ERROR - PHPMailer funciones.";
        echo "<p>Resultado: <span class='resultado-ko>ERROR - las funciones de PHPMailer no funcionan correctamente. Revisa la configuración del servicio en el servidor.</span></p>";
        error_log('ERROR - las funciones de PHPMailer no funcionan correctamente. Revisa la configuración del servicio en el servidor.', 0);
        $emailconfig = 0;
      } else {
        Logger::info('Las funciones de PHPMailer funcionan correctamente.');
        // Si esta instalado el PHPMailer, continuamos.
        echo "<p>Servidor: ".$_POST['conf-emailserver']."</p>";
        echo "<p>Puerto: ".$_POST['conf-emailport']."</p>";
        if(isset($_POST['conf-emailsmtpauth'])){
            $smtpauth = true;
            echo "<p>SMTP auth: true</p>";
            echo "<p>Usuario: ".$_POST['conf-emailuser']."</p>";
            echo "<p>PWD: ".$_POST['conf-emailpwd']."</p>";
            $emailuser = $_POST['conf-emailuser'];
            $emailpwd = $_POST['conf-emailpwd'];
        } else {
            $smtpauth = false;
            $emailuser = "";
            $emailpwd = "";
            echo "<p>SMTP auth: false</p>";
        }
        echo "<p>Tipo seguridad: ".$_POST['conf-emailseguridad']."</p>";
        if(!empty($_POST['conf-emailfrom']))
            echo "<p>From: ".$_POST['conf-emailfrom']."</p>";
        else
            echo "<p>From: vacio</p>";
        if(!empty($_POST['conf-emailfromname']))
            echo "<p>FromName: ".$_POST['conf-emailfromname']."</p>";
        else
            echo "<p>FromName: vacio</p>";

        // Confirmamos si la config de email es correcta.
        $emailcheck = comprobarcorreo('0',$_POST['conf-emailserver'],$_POST['conf-emailport'],$_POST['conf-emailuser'],$_POST['conf-emailpwd'],$smtpauth,$_POST['conf-emailseguridad']);

        // Dependiendo del resultado del check.
        if($emailcheck == 'ok'){
            echo "<p>Resultado: <span class='resultado-ok'>Configuracion de correo correcta</span>.</p>";
            $emailconfig = 1;
            $emailserver = $_POST['conf-emailserver'];
            $emailport = $_POST['conf-emailport'];
            if($smtpauth == 1)
              $emailsmtpauth = 'true';
            else
              $emailsmtpauth = 'false';
            $emailuser = $_POST['conf-emailuser'];
            $emailpwd = $_POST['conf-emailpwd'];
            $emailseguridad = $_POST['conf-emailseguridad'];
            $emailfrom = $_POST['conf-emailfrom'];
            $emailfromname = $_POST['conf-emailfromname'];

            // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
            if( $config2['confinicial']['conf'] != 0 )
              guardarLog($mysql,'cambia','configuración','-','Correo','Cambiada configuración para el correo.',$_SESSION['user'],'','');

            Logger::info('Datos de configuracion para el email actualizados.');
        } else {
            echo "<p>Resultado: <span class='resultado-ko'>ERROR - Revisa los datos de  configuraci&oacute;n de correo, por favor</span>.</p>";
            $configerrores['email'] = "ERROR - Error al confirmar datos de conexión al servicio de correo.";
            $emailconfig = 0;
            error_log('ERROR - Datos de configuracion para el email no actualizados.', 0);
        }
      }
  } else {
    echo "<p>Resultado: <span class='resultado-ko'>ERROR - PHPMailer no instalado/configurado en /usr/share/php/PHPMailer/</p>";
    $configerrores['email'] = "ERROR - PHPMailer no está instalado/configurado en la ruta /usr/share/php/PHPMailer/. Revisa la configuración del servidor.";
    $emailconfig = 0;
    error_log("ERROR - PHPMailer no esta instalado/configurado en la ruta /usr/share/php/PHPMailer/. Revisa la configuracion del servidor.", 0);
  }
} else {
  echo "<p>Email sin datos para actualizar.</p>";
  $emailconfig = 2;
  $emailserver = $config['email']['emailserver'];
  $emailport = $config['email']['emailport'];
  if($config['email']['smtpauth'] == 1)
    $emailsmtpauth = 'true';
  else
    $emailsmtpauth = 'false';
  $emailuser = $config['email']['emailuser'];
  $emailpwd = $config['email']['emailpwd'];
  $emailseguridad = $config['email']['smtpsecurity'];
  $emailfrom = $config['email']['from'];
  $emailfromname = $config['email']['fromname'];
  Logger::info('Sin datos para actualizar la configuracion del email.');
}
echo "<br />";

// Cabecera.
// ##############
echo "<h2>Cabecera:</h2>";
// Si esta marcado el check de cabecera procesamos los datos.
if(isset($_POST['conf-header'])){
  $cabeceratexto = $_POST['conf-cabeceratexto'];

  // Texto de cabecera.
  if($cabeceratexto != "")
    echo "<p>Cabecera texto: ".$cabeceratexto."</p>";
  else
    echo "<p>Cabecera texto: ###sin texto de cabecera###.</p>";

  // Imagen de cabecera.
  //---------------------
  if(!isset($_POST['conf-cabeceraimagen'])){
    echo "<p>Cabecera imagen: ###sin imagen de cabecera###.</p>";
    $cabeceralogomostrar = "false";
    $cabeceralogopersonal = "false";
    Logger::info('Sin logo de cabecera para mostrar.');
  } else {
    echo "<p>Cabecera imagen: ".$_POST['conf-cabeceraimagen']."</p>";
    $cabeceralogomostrar = "true";
    // Imagen personal o default.
    if(!isset($_POST['conf-cabeceraimagenperso'])){
      echo "<p>Imagen personalizada: no</p>";
      echo "<p>Imagen: default.png</p>";
      $cabeceralogopersonal = "false";
      Logger::info('Con logo de cabecera por defecto para mostrar.');
    } else {
      echo "<p>Imagen personalizada: si</p>";
      echo "<p>Imagen: user.png</p>";
      $cabeceralogopersonal = "true";
      Logger::info('Con logo de cabecera personalizado para mostrar.');
      // SUBIDA imagen, comprobamos si se ha seleccionado una imagen para subir.
      if(file_exists($_FILES['conf-imagen']['tmp_name'])){
        // Guardamos en una variable el resultado de la llamada a la funcion para subir la imagen.
        $subelogocheck = subelogofavicon("logo");
        // Confirmamos la subida del logo.
        if($subelogocheck != "ok"){
          // Si no fue bien.
          $cabecerasubelogo = 0;
          error_log('ERROR - Error al subir el logo personalizado de cabecera.', 0);
          foreach($subelogocheck as $key=>$value){
            $configerrores[$key] = $value;
          }
        } else {
          // Si fue bien.
          $cabecerasubelogo = 1;

          // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
          if( $config2['confinicial']['conf'] != 0 )
            guardarLog($mysql,'cambia','configuración','-','Cabecera','Cambiado logo de cabecera.',$_SESSION['user'],'','');

          Logger::info('Logo de cabecera personalizado subido correctamente.');
        }
      } else {
        // Si no hay nada que subir.
        $cabecerasubelogo = 2;
        echo "<p>Sin imagen para subir.</p>";
        Logger::info('Sin logo de cabecera personalizado para subir.');
      }
    }
  }
  $cabeceralogo = "user.png";

  // Favicon.
  //---------
  if(!isset($_POST['conf-faviconimagen'])){
    echo "<p>Favicon: ###sin favicon###</p>";
    $cabecerafaviconmostrar = "false";
    $cabecerafaviconpersonal = "false";
    Logger::info('Sin favicon para mostrar.');
  } else {
    echo "<p>Favicon: " .$_POST['conf-faviconimagen']."</p>";
    $cabecerafaviconmostrar = "true";
    // Favicon personal o default.
    if(!isset($_POST['conf-faviconpersonal'])){
      echo "<p>Favicon personal: no</p>";
      echo "<p>Favicon imagen: default.png</p>";
      $cabecerafaviconpersonal = "false";
      Logger::info('Con favicon por defecto para mostrar.');
    } else {
      echo "<p>Favicon personal: si</p>";
      echo "<p>Favicon imagen: favicon.png</p>";
      $cabecerafaviconpersonal = "true";
      Logger::info('Con favicon personalizado para mostrar.');
      // SUBIDA favicon, comprobamos si se ha seleccionado una imagen para subir.
      if(file_exists($_FILES['conf-favicon']['tmp_name'])){
        $subefaviconcheck = subelogofavicon("favicon");
        // Confirmamos la subida del favicon.
        if($subefaviconcheck != "ok"){
          // Si no fue bien.
          $cabecerasubefavicon = 0;
          foreach($subefaviconcheck as $key=>$value){
            $configerrores[$key] = $value;
          }
          error_log('ERROR - Error al subir el favicon personalizado.', 0);
        } else {
          // Si fue bien.

          // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
          if( $config2['confinicial']['conf'] != 0 )
            guardarLog($mysql,'cambia','configuración','-','Cabecera','Cambiado favicon.',$_SESSION['user'],'','');

          $cabecerasubefavicon = 1;
          Logger::info('Favicon personalizado subido correctamente.');
        }
      } else {
        // Si no hay que subir nada.
        $cabecerasubefavicon = 2;
        echo "<p>Sin favicon para subir.</p>";
        Logger::info('Sin favicon personalizado para subir.');
      }
    }
  }
  $cabecerafavicon = "favicon.png";

  // Confirmamos si se realizo alguna subida de imagen y/o favicon para dar de paso o no la subida de cabecera.
  if((isset($cabecerasubefavicon) && $cabecerasubefavicon == 0) || (isset($cabecerasubelogo) && $cabecerasubelogo == 0))
    $cabeceraconfig = 0;
  else

    // Guardamos el nuevo orden del incide o contenido en los logs para mostrar en el wiki.
    if( $config2['confinicial']['conf'] != 0 )
      guardarLog($mysql,'cambia','configuración','-','Cabecera','Cambiada configuración de cabecera.',$_SESSION['user'],'','');

    $cabeceraconfig = 1;

} else {
  // De lo contrario, si no esta el check de cabecera marcado no procesamos nada nuevo.
  echo "<p>Cabecera sin datos para actualizar.</p>";
  $cabeceraconfig = 2;
  $cabeceratexto = $config2['cabecera']['texto'];
  if($config2['cabecera']['logomostrar'] == 1)
    $cabeceralogomostrar = 'true';
  else
    $cabeceralogomostrar = 'false';
  if($config2['cabecera']['logopersonal'] == 1)
    $cabeceralogopersonal = 'true';
  else
    $cabeceralogopersonal = 'false';
  $cabeceralogo = $config2['cabecera']['logo'];
  if($config2['cabecera']['faviconmostrar'] == 1)
    $cabecerafaviconmostrar = 'true';
  else
    $cabecerafaviconmostrar = 'false';
  if($config2['cabecera']['faviconpersonal'] == 1)
    $cabecerafaviconpersonal = 'true';
  else
    $cabecerafaviconpersonal = 'false';
  $cabecerafavicon = $config2['cabecera']['favicon'];
  Logger::info('Sin datos para actualizar la configuracion de la cabecera ');
}
echo "<br />";


// Aplicamos la escritura de los archivos .ini en funcion de los resultados anteriores.

// Primero el parametro de configuracion inicial.
if($config2['confinicial']['conf'] == 0){

  // Si algun servicio no se configura correctamente no damos de paso la conf inicial.
  if($dbconfig != 0 && $redisconfig != 0 && $adminconfig != 0 && $emailconfig != 0 && $cabeceraconfig != 0)
    $conf = 1;
  else
    $conf = 0;

} else {

  $conf = 1;

}

// Si ha fallado la configuracion de alguno de los servicios actualizamos el archivo conf.ini con el servicio afectado.
if($dbconfig == 0 ||  $redisconfig == 0 || $adminconfig == 0 || $emailconfig == 0 || $cabeceraconfig == 0){

    error_log("ERROR - Error al guardar los datos de configuracion. MySQL: $dbconfig - Redis: $redisconfig - Admin: $adminconfig - Email: $emailconfig - Cabecera: $cabeceraconfig.", 0);

    actualizaconfini($conf,$dbconfig,$redisconfig,$adminconfig,$emailconfig,$cabeceratexto,$cabeceralogomostrar,$cabeceralogopersonal,$cabeceralogo,$cabecerafaviconmostrar,$cabecerafaviconpersonal,$cabecerafavicon);

  } else {

    // De lo contrario actualizamos los ficheros de acceso y el de los datos de configuracion.
    // conf.ini
    //////////////////////
    actualizaconfini($conf,$dbconfig,$redisconfig,$adminconfig,$emailconfig,$cabeceratexto,$cabeceralogomostrar,$cabeceralogopersonal,$cabeceralogo,$cabecerafaviconmostrar,$cabecerafaviconpersonal,$cabecerafavicon);
    // access.ini
    ///////////////
    actualizaaccessini($mysqlservername,$mysqlusername,$mysqlpwd,$mysqldbname,$mysqlport,$redisservername,$redisauth,$redisdb,$redisport,$nameadmin,$useradmin,$passwordadmin,$emailadmin,$emailserver,$emailport,$emailsmtpauth,$emailuser,$emailpwd,$emailseguridad,$emailfrom,$emailfromname);

  }



 // Mostramos el resultado final con errores si los hubiera.
  echo "<br />";
  echo "<hr />";

  // Comprobamos si hubo errores durante el chequeo de la configuracion.
  echo "<h3><u>Resumen de errores:</u></h3>";
  if(!empty($configerrores)){
    echo "<table id='conf-tab-res'>";
      echo "<tr>";
        echo "<th>Secci&oacute;n</th>";
        echo "<th>Error</th>";
      echo "</tr>";
    foreach($configerrores as $key=>$error){
      echo "<tr>";
        echo "<td>".$key."</td>";
        if($error == "ERROR - Error al confirmar datos de conexión al servicio de correo."){
          echo "<td>";
          // Si existiera algun error de correo mostramos el error devuelto durante la comprobacion.
          comprobarcorreo('1',$_POST['conf-emailserver'],$_POST['conf-emailport'],$emailuser,$emailpwd,$smtpauth,$_POST['conf-emailseguridad']);
          echo "</td>";
        } else {
          echo "<td>".$error."</td>";
        }
      echo "</tr>";
    }
    echo "</table>";
  } else {
    echo "<p><span class='resultado-ok' style='margin: 5px 0px;'>Todo ok</span></p>";
  }
  echo "<hr /><br />";
  ?>
  <button onclick="volver()">Volver</button>
  <button onclick="inicio()">Inicio</button>
  </div>

  <?php

} else {
  // Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
  echo "<div style='margin: 20px 10px;'>";
    echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
    echo "<p>No tienes permisos para acceder a este espacio. Volver a la p&aacute;gina principal pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
  echo "</div>";
  error_log("Acceso al procesado del formulario de configuracion no permitido. No se esta accediendo desde el indexs.");
}
?>
