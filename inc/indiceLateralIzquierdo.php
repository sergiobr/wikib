<?php
// Conectamos con MySQL.
$mysql = conectar_db();

// Obtenemos los indices activos.
$indicesActivos = mostrarIndicesContenidosActivos($mysql,'indices','none');

// Recorremos todos los indices obtenidos.
while($array = mysqli_fetch_array($indicesActivos)){

  // Pintamos el nombre del indice.
  echo "<h4>".$array['titulo']."</h4>";

  // Confirmamos si tiene algun contenido activo asociado.
  $contenidoExiste = compruebaIndiceConContenidos($mysql,$array['id']);

  // Si lo tuviera los pintamos en un desplegable.
  if (isset($contenidoExiste)){

    // Contenidos.
    // Comprobamos si existe o no la cookie para mostrar el menu de contenidos por indice desplegado o no.
    $cookie = "indice-".$array['id'];
    if(isset($_COOKIE[$cookie])){
      // Flecha para desplegar o replegar el indice de contenidos.
      echo "<div class='expande'><span>&#9660;</span></div>";
      echo "<div style='display: block;' class='contenidos' id='indice-".$array['id']."'>";
    } else {
      echo "<div class='expande'><span>&#9658;</span></div>";
      echo "<div style='display: none;' class='contenidos' id='indice-".$array['id']."'>";
    }
    // Recorremos los contenidos activos para el indice.
    $contenidosActivos = mostrarIndicesContenidosActivos($mysql,'contenidos',$array['id']);
    while ($array2 = mysqli_fetch_array($contenidosActivos)){

      if (isset($_GET['contenido']) AND $_GET['contenido'] == $array2['id'] )
        echo "<a href='?indice=".$array['id']."&contenido=".$array2['id']."' class='enlaceContenidoActivo'><span>&#8250;</span>&nbsp;".$array2['titulo']."</a>";
      else
        echo "<a href='?indice=".$array['id']."&contenido=".$array2['id']."' class='enlaceContenido'><span>&#8250;</span>&nbsp;".$array2['titulo']."</a>";

    }
    echo "</div>";

  }
}

?>
