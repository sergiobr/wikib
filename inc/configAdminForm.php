<?php
//Comprobamos si se llama desde el index.php
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/configAdminForm.php";
if (!strpos($url,$patern)) {
?>
    <div id="conf-config" class="conf-flex-item" >
      <form id="form-conf" action="" method="post" enctype="multipart/form-data" novalidate>

        <!-- Datos MySQL -->
        <?php if($config2['confinicial']['conf'] == "1")
          echo "<h2>MySQL&nbsp;&nbsp;<input type='checkbox' id='conf-mysql' name='conf-mysql' value='si' />&nbsp;<img src='img/info.png' id='conf-info' title='Configuración de acceso a la base de datos MySQL.'/></h2>";
        else
          echo "<h2>MySQL&nbsp;<input type='hidden' id='conf-mysql' name='conf-mysql' value='oculto'/>&nbsp;<img src='img/info.png' id='conf-info' title='Configuración de acceso a la base de datos MySQL.'/></h2>";?>
        <div id="mysql">
          <p>MySQL host (<span>*</span>)</p><input type="text" id="conf-mysqlservername" name="conf-mysqlservername" placeholder="MySQL host"  value="<?php echo $config['mysql']['servername']?>" required/>
          <p>MySQL usuario (<span>*</span>)</p><input type="text" id="conf-mysqlusername" name="conf-mysqlusername" placeholder="MySQL usuario" value="<?php echo $config['mysql']['username']?>" required/>
          <p>MySQL pwd (Opcional)</p><input type="password" id="conf-mysqlpwd" name="conf-mysqlpwd" placeholder="MySQL password" value="" />  <br />
          <img src="img/eye.png" class="eye" onmouseover="muestraPwd('conf-mysqlpwd','none')" onmouseout="ocultaPwd('conf-mysqlpwd','none')" >
          <p>MySQL DB (<span>*</span>)</p><input type="text" id="conf-mysqldbname" name="conf-mysqldbname" placeholder="MySQL DB" value="<?php echo $config['mysql']['dbname']; ?>" required/>
          <p>MySQL puerto (<span>*</span>)</p><input type="number" id="conf-mysqlport" name="conf-mysqlport" placeholder="MySQL puerto" value="<?php echo $config['mysql']['portmysql'];?>" required/>
        </div>

        <hr />

        <!-- Datos Redis -->
        <?php if($config2['confinicial']['conf'] == "1")
          echo "<h2>Redis&nbsp;&nbsp;<input type='checkbox' id='conf-redis' name='conf-redis' value='si' />&nbsp;<img src='img/info.png' id='conf-info' title='Configuración de acceso a la base de datos Redis.'/></h2>";
        else
          echo "<h2>Redis&nbsp;<input type='hidden' id='conf-redis' name='conf-redis' value='oculto'/>&nbsp;<img src='img/info.png' id='conf-info'/></h2>";?>
        <div id='redis'>
          <p>Redis host (<span>*</span>)</p><input type="text" id="conf-redisservername" name="conf-redisservername" placeholder="Redis host" value="<?php echo $config['redis']['servernameredis'];?>" required />
          <p>Redis pwd (Opcional)</p><input type="password" id="conf-redisauth" name="conf-redisauth" placeholder="Redis password" value="" /><br />
          <img src="img/eye.png" class="eye" onmouseover="muestraPwd('conf-redisauth','none')" onmouseout="ocultaPwd('conf-redisauth','none')" >
          <p>Redis DB (<span>*</span>)</p><input type="number" id="conf-redisdb" name="conf-redisdb" placeholder="Redis DB" value="<?php echo $config['redis']['dbredis'];?>" required/>
          <p>Redis puerto (<span>*</span>)</p><input type="number" id="conf-redisport" name="conf-redisport" placeholder="Redis puerto" value="<?php echo $config['redis']['portredis'];?>" required/>
        </div>

        <hr />

        <!-- Datos de usuario admin -->
        <?php if($config2['confinicial']['conf'] == 1)
          echo "<h2>Administrador&nbsp;&nbsp;<input type='checkbox' id='conf-admin' name='conf-admin' value='si' />&nbsp;<img src='img/info.png' id='conf-info' title='Credenciales para el usuario administrador de WikiB.'/></h2>";
        else
          echo "<h2>Administrador&nbsp;&nbsp;<input type='hidden' id='conf-admin' name='conf-admin' value='oculto' />&nbsp;<img src='img/info.png' id='conf-info' title='Credenciales para el usuario administrador de WikiB.'/></h2>";?>
        <div id='admin'>
          <p>Nombre y apellidos (<span>*</span>)&nbsp;<img src='img/info.png' id='conf-info' title='Nombre y apellidos del administrador. Nos permite identificar al usuario en el sistema.' /></p><input type="text" id="conf-nameadmin" name="conf-nameadmin" placeholder="Admin nombre" value="<?php echo $config['admin']['nameadmin'];?>" required>
          <p>Usuario (<span>*</span>)&nbsp;<img src='img/info.png' id='conf-info' title='Usuario administrador. El usuario indicado pertenecer&aacute; al grupo de administradores. Por favor, introduce el usuario con letras min&uacute;sculas.'/></p><input type="text" id="conf-useradmin" name="conf-useradmin" placeholder="Admin usuario" value="<?php echo $config['admin']['useradmin'];?>" required/>
          <p>Pwd (<span>*</span>)</p><input type="password" id="conf-passwordadmin" name="conf-passwordadmin" placeholder="Admin password" value="" required /><br />
          <img src="img/eye.png" class="eye" onmouseover="muestraPwd('conf-passwordadmin','none')" onmouseout="ocultaPwd('conf-passwordadmin','none')" >
          <input type="hidden" id="conf-passwordadmincrypt" name="conf-passwordadmincrypt" />
          <p>Admin email (<span>*</span>)</p><input type="email" id="conf-emailadmin" name="conf-emailadmin" placeholder="Admin email" value="<?php echo $config['admin']['emailadmin'];?>" required/>
        </div>

        <hr />

        <!-- Datos de correo -->
        <?php if($config2['confinicial']['conf'] == 1)
          echo "<h2>Email&nbsp;&nbsp;<input type='checkbox' id='conf-email' name='conf-email' value='si' />&nbsp;<img src='img/info.png' id='conf-info' title='Datos de acceso a cuenta de email para el envío de notificaciones.'/></h2>";
        else
          echo "<h2>Email&nbsp;&nbsp;<input type='hidden' id='conf-email' name='conf-email' value='oculto' />&nbsp;<img src='img/info.png' id='conf-info' title='Datos de acceso a cuenta de email para el envío de notificaciones.'/></h2>";?>
        <div id='email'>
          <p>Email servidor (<span>*</span>)</p><input type="text" id="conf-emailserver" name="conf-emailserver" placeholder="Servidor de correo" value="<?php echo $config['email']['emailserver'];?>" required/>
          <p>Email puerto (<span>*</span>)</p><input type="number" id="conf-emailport" name="conf-emailport" placeholder="Email puerto" value="<?php echo $config['email']['emailport']; ?>" required/>
          <p>Email smtp auth (Opcional)&nbsp;&nbsp;
          <!-- segun lo indicado en access.ini mostramos la conf smtpAuth o no, al final con JS-->
          <?php if($config['email']['smtpauth'] == true)
            echo "<input type='checkbox' id='conf-emailsmtpauth' name='conf-emailsmtpauth' value='true' checked/>&nbsp;<img src='img/info.png' id='conf-info' title='Credenciales de validación para la cuenta de email. Consulta la configuración necesaria con tu proveedor del servicio.'/></p>";
          else
            echo "<input type='checkbox' id='conf-emailsmtpauth' name='conf-emailsmtpauth' value='false' />&nbsp;<img src='img/info.png' id='conf-info' title='Credenciales de validación para la cuenta de email. Consulta la configuración necesaria con tu proveedor del servicio.'/></p>";
          ?>
          <div id="conf-emailcredenciales">
            <p>Email usuario</p><input type="text" id="conf-emailuser" name="conf-emailuser" placeholder="Email usuario" value="<?php echo $config['email']['emailuser']; ?>" />
            <p>Email pwd</p><input type="password" id="conf-emailpwd" name="conf-emailpwd" placeholder="Email password" value="" /><br />
            <img src="img/eye.png" class="eye" onmouseover="muestraPwd('conf-emailpwd','none')" onmouseout="ocultaPwd('conf-emailpwd','none')" >
          </div>
          <p style='margin: 8px auto;'>Seguridad (<span>*</span>)&nbsp;<img src='img/info.png' id='conf-info' title='Tipo de seguridad para el SMTP saliente. Consulta la configuraci&oacute;n necesaria con tu proveedor del servicio.'/>&nbsp;&nbsp;<select name="conf-emailseguridad" id="conf-emailseguridad" required></p>
            <option id="none" value="false">Ninguna</option>
            <option id="tls" value="tls">TLS</option>
            <option id="ssl" value="ssl">SSL</option>
          </select>
          <?php
          // Indicamos que hay configurado en el archivo access.ini en relacion a la seguridad email para indicar la opcion por defecto.
          if($config['email']['smtpsecurity'] == 'tls')
            $seguridad = "tls";
          elseif($config['email']['smtpsecurity'] == 'ssl')
            $seguridad = "ssl";
          else
            $seguridad = "none";
          ?>
          <p>From email(<span>*</span>)&nbsp;<img src='img/info.png' id='conf-info' title='Direcci&oacute;n de remitente que aparecer&aacute; en las notificaciones que env&iacute;a WikiB. Consulta la configuraci&oacute;n con tu proveedor del servicio.'/></p><input type="text" id="conf-emailfrom" name="conf-emailfrom" value="<?php echo $config['email']['from'];?>" required />
          <p>From nombre (Opcional)&nbsp;<img src='img/info.png' id='conf-info' title='Nombre de remitente que aparecer&aacute; en las notificacoines que env&iacute;a WikiB.'/></p><input type="text" id="conf-emailfromname" name="conf-emailfromname" value="<?php echo $config['email']['fromname'];?>" />
        </div>

        <hr />

        <!-- Datos cabecera OPCIONAL -->
        <?php if($config2['confinicial']['conf'] == 1)
          echo "<h2>Cabecera&nbsp;&nbsp;<input type='checkbox' id='conf-header' name='conf-header' value='si' />&nbsp;<img src='img/info.png' id='conf-info' title='Datos de configuración para la cabecera: título, logotipo, logotipo personalizado, favicon personalizado.'/></h2>";
        else
          echo "<h2>Cabecera&nbsp;&nbsp;<input type='hidden' id='conf-header' name='conf-header' value='oculto' />&nbsp;<img src='img/info.png' id='conf-info' title='Datos de configuración para la cabecera: título, logotipo, logotipo personalizado, favicon personalizado.'/></h2>";
        ?>
        <div id="header">
          <p>Texto de cabecera (Opcional)</p><input type="text" id="conf-cabeceratexto" name="conf-cabeceratexto" placeholder="Texto cabecera" value="<?php echo $config2['cabecera']['texto']; ?>" />
          <p>Mostrar imgaen en cabecera (Opcional)&nbsp;&nbsp;
          <!-- Indicamos si queremos que se muestre imagen en la cabecera o no -->
          <?php if($config2['cabecera']['logomostrar'] == true){
            echo "<input type='checkbox' id='conf-cabeceraimagen' name='conf-cabeceraimagen' value='true' checked /></p>";
          } else {
            echo "<input type='checkbox' id='conf-cabeceraimagen' name='conf-cabeceraimagen' value='true' /></p>";
          }
          ?>
          <!-- Si indicamos que se muestre imagen descubrimos las opciones de imagen -->
          <div id="logo-personalizado">
            <!-- Indicamos si queremos la imagen por defecto o la personalizada -->
            <p>Imagen de cabecera personalizada (Opcional)&nbsp;&nbsp;
            <?php if($config2['cabecera']['logopersonal'] == true)
              echo "<input type='checkbox' id='conf-cabeceraimagenperso' name='conf-cabeceraimagenperso' value='true' checked /></p>";
            else
              echo "<input type='checkbox' id='conf-cabeceraimagenperso' name='conf-cabeceraimagenperso' value='true' /></p>";?>
            <div id="defaultimagen">
              <img id="conflogo" src='img/default.png' />
            </div>
            <div id='subeimagen'>
              <p>Sube imagen</p>
              <input type='file' name='conf-imagen' id='conf-imagen' />
              <img id='conflogo' src='img/user.png' />
            </div>
          </div>
          <p>Mostrar favicon (Opcional)&nbsp;&nbsp;
          <!-- Indicamos si queremos que se muestre favicon o no -->
          <?php if($config2['cabecera']['faviconmostrar'] == true){
            echo "<input type='checkbox' id='conf-faviconimagen' name='conf-faviconimagen' value='true' checked />";
          } else {
            echo "<input type='checkbox' id='conf-faviconimagen' name='conf-faviconimagen' value='true' />";
          }
          ?>
          </p>
          <div id="favicon">
          <!-- Indicamos si queremos favicon personalizado o por defecto -->
          <p>Favicon personalizado(Opcional)&nbsp;&nbsp;
          <?php if($config2['cabecera']['faviconpersonal'] == ""){
            echo "<input type='checkbox' id='conf-faviconpersonal' name='conf-faviconpersonal' value='true'  /></p>";
          } else {
            echo "<input type='checkbox' id='conf-faviconpersonal' name='conf-faviconpersonal' value='true' checked /></p>";
          }?>
            <div id='defaultfavicon'>
              <img id="faviconlogo" src='img/default.png' />
            </div>
            <div id="subefavicon">
              <p>Sube favicon</p>
              <input type='file' name='conf-favicon' id='conf-favicon' />
              <img id="faviconlogo" src='img/favicon.png' />
            </div>
          </div>

        </div>
        <hr />

        <button type="submit" method="post" name="guardaconfig">Guardar</button>
      </form>
    </div>
  </div>

  <script type="text/javascript">
    // Mostrar o no las opciones de mysql al cargar pagina.
    mostrarmysql();
    // Mostrar o no las opciones de redis al cargar pagina.
    mostrarredis();
    // Mostrar o no las opciones de admin al cargar pagina.
    mostraradmin();
    // Mostrar o no las opciones de smtp al cargar pagina.
    mostrarcorreo();
    // Mostrar o no las opciones de smtpauth al cargar la pagina.
    mostrarsmtpauth();
    // Mostrar o no las opciones de cabecera al cargar pagina.
    mostrarcabecera();
    // Mostramos o no las opciones de imagen al cargar la pagina.
    mostrarlogo();
    // Mostrar imagen personalizada o por defecto al cargar la imagen de config general.
    mostrarlogopersonalizado();
    // Mostrar o no la seccion favicon.
    mostrarfavicon();
    // Mostramos o no la imagen favicon personal o default.
    mostrarfaviconpersonal();

    // Indicamos la opcion por defecto de la seguridad email en funcion de lo indicado en el archivo .ini
    document.getElementById('<?php echo $seguridad;?>').defaultSelected = true;

  </script>

<?php

} else {
// Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
echo "<div style='margin: 20px 10px;'>";
  echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
  echo "<p>La ventana de configuraci&oacute;n solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
echo "</div>";
error_log("Acceso al de configuracion para el administrador no permitido. No se esta accediendo desde el index.");
} ?>
