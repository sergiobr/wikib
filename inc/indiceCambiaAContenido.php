<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}

  // Incluimos el fichero con las funciones.
  include(dirname(__FILE__)."/../fun/funciones.php");

  // Función para conectarnos a la base de datos.  
  function conectar_db2(){
    // Incluimos el fichero con los datos de configuracion de la plataforma.
    $conf = parse_ini_file(dirname(__FILE__)."/../conf/access.ini",TRUE);
    // Nos conectamos al MySQL, ya que desde el fichero de funciones falla al no ser llamado desde el index.
    $mysql = mysqli_connect($conf['mysql']['servername'],$conf['mysql']['username'],$conf['mysql']['password'],$conf['mysql']['dbname'],$conf['mysql']['portmysql']);
    mysqli_set_charset($mysql,"utf8");
    unset($conf);
    return $mysql;
  }

    // Nos conectamos al MySQL usando la funcion anterior.
    $mysql = conectar_db2();

    // Obtenemos las variables pasadas por AJAX.
    $comando = $_POST['command'];
    $padre = $_POST['padre'];
    $id = $_POST['id'];
    
    // Confirmamos si mostramos div o cambiamos el indice al contenido.
    if ( $comando == 'mostrar' ){
        
        // Obtenemos todos los indices.
        $query = obtenerIndicesContenidos($mysql,'indice','none');
        // Devolvemos el resultado para pintar el desplegable con todos los indices a escoger para el contenido.
        while ( $array = mysqli_fetch_array($query) )
            echo "<option value='".$array['id']."'>".$array['id']." - ".$array['titulo']."</option>";
    
    } else {
    
        // Actualizamos el idpadre del contenido.
        $actualiza = actualizaIdPadreContenido($mysql,$id,$padre);
    
        echo $actualiza;
    }
    
?>