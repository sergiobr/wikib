<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}

  // Incluimos el fichero con las funciones.
  include(dirname(__FILE__)."/../fun/funciones.php");

  // Función para conectarnos a la base de datos.  
  function conectar_db4(){
    // Incluimos el fichero con los datos de configuracion de la plataforma.
    $conf = parse_ini_file(dirname(__FILE__)."/../conf/access.ini",TRUE);
    // Nos conectamos al MySQL, ya que desde el fichero de funciones falla al no ser llamado desde el index.
    $mysql = mysqli_connect($conf['mysql']['servername'],$conf['mysql']['username'],$conf['mysql']['password'],$conf['mysql']['dbname'],$conf['mysql']['portmysql']);
    mysqli_set_charset($mysql,"utf8");
    unset($conf);
    return $mysql;
  }
  
    // Nos conectamos al MySQL usando la funcion anterior.
    $mysql = conectar_db4();
  
    // Obtenemos los elementos enviados por el AJAX.
    $elemento = $_POST['elemento'];
    $id = $_POST['id'];

    // Primero obtenemos el estado del elemento.
    $estado = estadoIndiceContenido($mysql,$elemento,$id,"estado",'none');
    
    // Comprobamos el estado en la base de datos para cambiarselo.
    if ( $estado['activo'] == '1' )
        $nuevoestado = 0;
    else
        $nuevoestado = 1;
    
    // Cambiamos el estado del elemento.
    estadoIndiceContenido($mysql,$elemento,$id,"cambia",$nuevoestado);
  
?>