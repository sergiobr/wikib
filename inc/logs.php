
<h1>Logs de actividad</h1>
<p>A continuaci&oacute;n se muestran los registros de actividades realizadas en el Wiki <b style="text-decoration: underline;">durante el &uacute;ltimo mes</b>.</p>
<p>Para consultar otras fechas usa el filtro.</p>

<!-- Formulario para el filtro de logs -->
<form name='filtradoLogs' id='filtradoLogs' method="get">
  <input type="hidden" name="logs" value='1' />
  <p>Inicio:&nbsp;</p><input type='date' name='datei' />
  <p>Fin:&nbsp;</p><input type='date' name='datef' />
  <p>Acci&oacute;n:&nbsp;</p><select name='action'>
    <option value=''>Todo</option>
    <option value='actualiza'>Actualiza</option>
    <option value='cambia'>Cambia</option>
    <option value='ordena'>Ordena</option>
    <option value='crea'>Crea</option>
    <option value='borra'>Borra</option>
    <option value='solicita'>Solicita</option>
  </select>
  <p>Elemento:&nbsp;</p><select name='element'>
    <option value=''>Todo</option>
    <option value='indice'>Indice</option>
    <option value='contenido'>Contenido</option>
    <option value='usuario'>Usuario</option>
    <option value='pwd'>Contrase&ntilde;a</option>
    <option value='configuracion'>Configuraci&oacute;n</option>
  </select>
  <input type='text' name='user' id='userLogFilter' onkeyup="evitaEspacios(this)" placeholder="Usuario"/ value='0'>
  <button id="filtrarLogs">Filtrar</button>&nbsp;<button type="reset">Reinicia</button>
</form>

<?php

// Mostramos el filtro aplicado por el usuario.

// Primero obtenemos las fechas actual y un mes atras.
$today = date("Y-m-d");
$lastmonth = date("Y-m-d", strtotime("-1 months"));

if(isset($_GET['datei'])){
  $datei = $_GET['datei'];
  $datef = $_GET['datef'];
  $action = $_GET['action'];
  $element = $_GET['element'];
  $user = $_GET['user'];

  if($datei == "" AND $datef == ""){
    $datei = $lastmonth;
    $datef = $today;
  } else {
    if($datei == "")
      $datei = " - ";
    if($datef == "")
      $datef = " - ";
  }
  if($action == "")
    $action = "Todas";
  if($element == "")
    $element = "Todos";
  if($user == "")
    $user = "Todos";

  echo "<div id='filtradoLogsActivo'>

          <p><span class='div-resultado-cerrar-filtro-log' title='Borrar filtro aplicado.'>X</span>&nbsp;Filtro aplicado</p><p>Inicio: <b>" . $datei . "</b> | Fin: <b>" . $datef . "</b> | Acci&oacute;n: <b>" . $action . "</b> | Elemento: <b>" . $element . "</b> | Usuario: <b>" . $user . "</b></p>

        </div>";
} else {

  echo "<div id='filtradoLogsActivo'>
      <p>Filtro por defecto</p><p>Inicio: <b>" . $lastmonth . "</b> | Fin: <b>" . $today . "</b> | Acci&oacute;n: <b>Todas</b> | Elemento: <b>Todos</b> | Usuario: <b>Todos</b></p>
  </div>";
}
?>

<!-- Seccion para mostrar logs logs -->
<div id="resultadoLogs"></div>

<!-- Seccion para comparar versiones de contenidos -->
<div id='logsComparaOculto'>
  <button id="botonCerrarLogin" class="botonCerrarLogin">&#10006;</button>
  <div>
    <button id='copiaAntiguo' onclick='copiaContenido("#logsComparaAntiguo")'>Copia HTML antiguo</button>
    <button id='copiaNuevo' onclick='copiaContenido("#logsComparaNuevo")'>Copia HTML nuevo</button>
  </div>
  <div id='logsCompara'>
    <div id='logsComparaAntiguo'></div>
    <div class='separador'></div>
    <div id='logsComparaNuevo'></div>
  </div>
</div>

<?php

// Si no usamos el filtro, mostramos todo el contenido de logs del ultimo mes.
if(!isset($_GET['datei'])){
  echo "<script>mostrarLogs('','','','','0');</script>";
} else {
// De lo contrario mostramos los logs segun los filtros aplicados por el usuario.
  echo "<script>mostrarLogs('" . $_GET['datei'] . "','" . $_GET['datef'] . "','" . $_GET['action'] . "','" . $_GET['element'] . "','" . $_GET['user'] . "');</script>";
}
?>
