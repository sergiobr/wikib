<?php
//Comprobamos si se llama desde el index.php
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/configUserForm.php";
if (!strpos($url,$patern)) {
?>
  <h1>Configuración de usuario</h1><br />
  <p>Datos de configuración del usuario.</p>

  <div class="div-resultado" id="usuarios-resultado">
    <div class="div-resultado-container" id="usuarios-resultado-container">
      <p></p><span class="div-resultado-cerrar" id="usuarios-resultado-cerrar" title="Cerrar">X</span>
    </div>
  </div>
  <?php
    // Nos conectamos a la base de datos para obtener los datos del usuario con el que estamos validados.
    $mysql = conectar_db();
    $usuario = obtenusuario($mysql,$_SESSION['id']);
  ?>
  <!-- Pintamos la tabla con la informacion del usuario -->
  <div id="conf-config" class="conf-flex-item">
     <table id='user-tab-res'>
        <tr>
          <th>ID</th>
          <th>Usuario</th>
          <th>Nombre</th>
          <th>Email</th>
          <th>Grupo</th>
          <th>Ultimo Login</th>
          <th>IP Ultimo Login</th>
          <th>Opciones</th>
        </tr>
        <tr>
        <?php
            while($datosUsuario = mysqli_fetch_array($usuario)){
                echo "<td>".$datosUsuario['id']."</td>";
                echo "<td>".$datosUsuario['usuario']."</td>";
                echo "<td>".$datosUsuario['nombre']."</td>";
                echo "<td>".$datosUsuario['email']."</td>";
                echo "<td>".$datosUsuario['grupo']."</td>";
                echo "<td>".$datosUsuario['ultimoLogin']."</td>";
                echo "<td>".$datosUsuario['ultimoLoginIp']."</td>";
                echo "<td><img title='Editar datos usuario' id='editar-usuario' class='user-mng-icon' onclick='mostrarFormEditUser(".$datosUsuario['id'].",\"mostrarEdita\")' src='img/useredit.png' /><img title='Cambiar pwd' id='cambia-pwd' class='user-mng-icon' onclick='mostrarFormPwdUser(".$datosUsuario['id'].",\"".$datosUsuario['usuario']."\")' src='img/forgotpwd.png' /></td>";
            }
        ?>
        </tr>
    </table>

      <!-- Edita usuario -->
    <div id="usuario-form-edit" class="form-oculto">
        <div id="usuario-form-edit-container" class="form-oculto-container">
          <button id="botonCerrarLogin" class="botonCerrarLogin">&#10006;</button>
            <h4 id="edita-usuario-id"></h4>
          <form id="edita-usuario" name="edita-usuario" method="post" action="" onsubmit="return false">
            <input type="hidden" id="edita-usuario-id" name="edita-usuario-id"  />
            <input type="hidden" id="edita-usuario-usuario" name="edita-usuario-usuario" value="<?php echo $_SESSION['user'];?>"/>
            <p>Nombre y Apellidos: </p><input type="text" id="edita-usuario-nombre" name="edita-usuario-nombre" placeholder="Nombre y apellidos" />
            <p>Email: </p><input type="email" id="edita-usuario-email" name="edita-usuario-email" placeholder="Email" pattern="[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" required/>
            <input type="hidden" id="edita-usuario-grupo" name="edita-usuario-grupo" value="<?php echo $_SESSION['group'];?>" />
            <br /><hr />
            <button id="edita-usuario-boton" type="submit" onclick="actualizaFormEditUser('actualizaDatos')">Edita</button>
            <button type="reset">Borrar</button>
          </form>
        </div>
    </div>

    <!-- Cambia pwd usuario -->
    <div id="usuario-form-pwd" class="form-oculto">
        <div id="usuario-form-pwd-container" class="form-oculto-container">
        <button id="botonCerrarLogin3" class="botonCerrarLogin">&#10006;</button>
        <h4 id="cambia-usuario-pwd"></h4>
        <form id="cambia-pwd-usuario" name="cambia-pwd-usuario" method="post" action="" onsubmit="return false">
            <input type="hidden" id="cambia-usuario-pwd-usuario" name="cambia-usuario-pwd-usuario" />
            <input type="hidden" id="cambia-usuario-pwd-id" name="cambia-usuario-pwd-id" />
            <p>Nueva pwd</p><input type="password" id="cambia-usuario-pwd-1" name="cambia-usuario-pwd-1" placeholder="Pwd" require />
            <input type="hidden" id="cambia-usuario-pwd-1-enc" name="cambia-usuario-pwd-1-enc" />
            <p>Repite pwd</p><input type="password" id="cambia-usuario-pwd-2" name="cambia-usuario-pwd-2" placeholder="Pwd" require />
            <input type="hidden" id="cambia-usuario-pwd-2-enc" mane="cambia-usuario-pwd-2-enc" /><br />
            <img src="img/eye.png" class="eye" onmouseover="muestraPwd('cambia-usuario-pwd-1','cambia-usuario-pwd-2')" onmouseout="ocultaPwd('cambia-usuario-pwd-1','cambia-usuario-pwd-2')" >
            <br /><hr />
            <button id="pwd-usuario-boton" type="submit" onclick="cambiaPwdUsuario('cambiaPwd')">Actualiza</button>
            <button type="reset">Borrar</button>
        </form>
        </div>
    </div>
  </div>

<?php
} else {
    // Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
    echo "<div style='margin: 20px 10px;'>";
      echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
      echo "<p>La ventana de configuraci&oacute;n solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
    echo "</div>";
    error_log("Acceso al de configuracion para el usuario no permitido. No se esta accediendo desde el index.");
} ?>
