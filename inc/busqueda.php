<?php

echo "<h1>Resultados de la búsqueda</h1>";

echo "<div id='resultadoBusqueda'>";

  // Obtenemos el patron de busqueda.
  $patron = $_GET['busqueda'];
  $patron = urldecode($patron);

  $patron = htmlentities($patron);

  if ($patron <> ''){

    // Contamos el numero de palabras.
    $trozos = explode(" ",$patron);
    $numero = count($trozos);

    // Inicializamos la variable con el patron a buscar en MySQL.
    $patronQuery = "";

    // Generamos el patron a buscar en la query MySQL dependiendo del numero de palabras introducidas para buscar.
    if ($numero == 1){

      // Si solo se busca una palabra.
      ////$patronQuery = '%' . $patron . '%';
      $patronQuery = " titulo LIKE '%" . $patron . "%' OR contenido LIKE '%" . $patron . "%'";

    } elseif ($numero > 1){

      // Si se buscan varias palabras, eliminamos las que tengan menos de 3 letras (la, el, con,...) y buscamos el resto.

      // Para identificar si es la primera cadena o no para agregar un OR al comienzo.
      $loop = 0;

      $partes = preg_split('/\s+/', $patron);
      foreach($partes as $parte){
        if(strlen($parte) > 3)

          if ( $loop == 0) {

            // Si es la primera cadena comenzamos la query tras el WHERE, de lo contrario comenzamos con un OR.
            $loop = $loop + 1;

            $patronQuery = $patronQuery . " titulo LIKE '%" . $parte . "%' OR contenido LIKE '%" . $parte . "%'";

          } else {

            $patronQuery = $patronQuery . " OR titulo LIKE '%" . $parte . "%' OR contenido LIKE '%" . $parte . "%'";

          }

      }

    }

    // Buscamos las coincidencias en los contenidos.
    $sql = "SELECT id, padre_id, titulo FROM contenido WHERE $patronQuery";
    $query = mysqli_query($mysql,$sql);

    // Mostramos cada resultado indicando el indice padre del contenido y el contenido que contiene el patron buscado.
    echo "Resultados para la búsqueda del patrón <mark><b>$patron</b><br /><br /></mark>";
    $resultados = 0;
    while($contenidos = mysqli_fetch_array($query,MYSQLI_ASSOC)){

        $resultados = 1;


        // Buscamos el titulo del indice padre.
        $sql2 = "SELECT titulo FROM indice WHERE id = '" . $contenidos['padre_id'] . "'";
        $query2 = mysqli_query($mysql,$sql2);
        $indice = mysqli_fetch_array($query2,MYSQLI_ASSOC);

        // Mostramos el resultado.
        printf ("Indice: <b>%s</b> <br /> Contenido: <b>%s</b><br />", $indice['titulo'], $contenidos['titulo']);
        echo "<p id='enlaceBusqueda' onclick='compruebaExpande(" . $contenidos['padre_id'] . "," . $contenidos['id'] . ")'>Enlace</p>";
        echo "<br /><br />";

      }

    if($resultados == 0)
      echo "No hay resultados para la búsqueda <b>$patron</b>";

  } else {

    // Si no se introduce texto en el buscador.
    echo "Introduce texto en el buscador, por favor.";

  }

echo "</div>";

?>
