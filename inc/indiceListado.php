<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}


  // Incluimos el fichero con las funciones.
  include(dirname(__FILE__)."/../fun/funciones.php");

  // Obtenemos los datos del archivo de configuracion para evitar errores de PHP previo a la config inicial.
  $conf2 = parse_ini_file(dirname(__FILE__)."/../conf/conf.ini",TRUE);

  // Función para conectarnos a la base de datos.
  function conectar_db2(){
    // Incluimos el fichero con los datos de configuracion de la plataforma.
    $conf = parse_ini_file(dirname(__FILE__)."/../conf/access.ini",TRUE);
    $conf2 = parse_ini_file(dirname(__FILE__)."/../conf/conf.ini",TRUE);

    // Nos conectamos al MySQL, ya que desde el fichero de funciones falla al no ser llamado desde el index.
    if ( $conf2['confinicial']['conf'] != 0 ) {
      $mysql = mysqli_connect($conf['mysql']['servername'],$conf['mysql']['username'],$conf['mysql']['password'],$conf['mysql']['dbname'],$conf['mysql']['portmysql']);
      mysqli_set_charset($mysql,"utf8");
      unset($conf);
      return $mysql;
    }

  }

  // Nos conectamos al MySQL usando la funcion anterior.
  $mysql = conectar_db2();

  $mostrar = $_POST['mostrar'];
  $id = $_POST['id'];

  // Mostrar indices/contenio crear indices/contenido.
  // Contenido.
  if ($mostrar == 'contenido' OR $mostrar == 'contenidoBorra' OR $mostrar == 'contenidoAgrega'){

        // Inicializamos la variable por si hay que crear o borrar algo.
        $crearborrar = 'ok';

        // Si creamos un contenido nuevo.
        if ($mostrar == 'contenidoAgrega'){
            $titulo = $_POST['titulo'];
            // Creamos el indice que le pasamos a la funcion del archivo funciones.php
            $crearborrar = crearIndiceContenido($mysql,$mostrar,$titulo,$id);
            // Si fue todo ok pintamos de nuevo los indices.
        }

        // Si borramos un contenido existente.
        if ($mostrar == 'contenidoBorra'){
            $crearborrar = borrarIndiceContenido($mysql,$mostrar,$id);
            // Renombramos la variable $id con el id del indice que le pasamos en la variable $_POST['titulo'] por JS, para mostrar el resto de contenidos del indice una vez borrado.
            $id = $_POST['titulo'];
            // Si fue todo ok pintamos de nuevo los indices.
        }

        if ( $crearborrar == 'ok' ){

            if ( $conf2['confinicial']['conf'] != 0 ) {
              // Llamamos a la funcion del archivo funciones.php para obtener los indices.
              $query = obtenerIndicesContenidos($mysql,$mostrar,$id);

            // Pintamos los contenidos.
            while($array = mysqli_fetch_array($query)){

                echo "<li class='ui-state-default' id='item-".$array['id']."'><span title='Cambia la posición pinchando y arrastrando.' class='ui-icon ui-icon-arrowthick-2-n-s'></span>(".$array['id'].") ".$array['titulo'];
                if ($array['activo'] == 1)
                    //echo "<input id='habilita' type='checkbox' checked title='Habilita o deshabilita el contenido.' onclick='cambiaEstado(\"contenido\",".$array['id'].")'>";
                    echo "<div title='Habilita o deshabilita el contenido' id='habilita-".$array['id']."'' class='usuario-activo habilita' onclick='cambiaEstado(\"contenido\",".$array['id'].")'></div>";
                else
                    //echo "<input id='habilita' type='checkbox' title='Habilita o deshabilita el contenido.' onclick='cambiaEstado(\"contenido\",".$array['id'].")'>";
                    echo "<div title='Habilita o deshabilita el contenido' id='habilita-".$array['id']."'' class='usuario-inactivo habilita' onclick='cambiaEstado(\"contenido\",".$array['id'].")'></div>";
                // Si borramos un contenido usamos el cambio titulo de la funcion JS crearIndiceContenidoSortable para especificar el ID del índice para mostrar el resto de contenidos una vez borrado.
                echo "<img title='Cambia el índice asociado.' src='img/key.png' onclick='cambiaIndiceAContenido(\"mostrar\",".$id.",".$array['id'].")'><img title='Borrar contenido' src='img/delete.png' onclick='mostrarBorrarIndiceContenido(\"contenidoBorra\",\"".$id."\",\"".$array['id']."\")'>";
                echo "</li>";

            }
          }
        } else {

            echo $crearborrar;

        }
  }
  // Indices.
  else {
        // Inicializamos la variable por si hay que crear o borrar algo.
        $crearborrar = 'ok';

        // Si creamos un indice nuevo.
        if ($mostrar == 'indiceAgrega'){
            $titulo = $_POST['titulo'];
            // Creamos el indice que le pasamos a la funcion del archivo funciones.php
            $crearborrar = crearIndiceContenido($mysql,$mostrar,$titulo,$id);
            // Si fue todo ok pintamos de nuevo los indices.
        }

        // Si borramos un indice existente.
        if ($mostrar == 'indiceBorra'){
            $crearborrar = borrarIndiceContenido($mysql,$mostrar,$id);
            // Si fue todo ok pintamos de nuevo los indices.
        }

    if ( $crearborrar == 'ok' ){

        if ( $conf2['confinicial']['conf'] != 0 ) {
          // Llamamos a la funcion del archivo funciones.php para obtener los indices.
          $query = obtenerIndicesContenidos($mysql,$mostrar,$id);

          // Pintamos cada indice.
          while($array = mysqli_fetch_array($query)){

              echo "<li class='ui-state-default' id='item-".$array['id']."'><span title='Cambia la posición pinchando y arrastrando.' class='ui-icon ui-icon-arrowthick-2-n-s'></span>(".$array['id'].") ".$array['titulo'];
              if ($array['activo'] == 1)
                  //echo "<input id='habilita' type='checkbox' checked title='Habilita o deshabilita el índice.' onclick='cambiaEstado(\"indice\",\"".$array['id']."\")'>";
                  echo "<div title='Habilita o deshabilita el índice' id='habilita-".$array['id']."' class='usuario-activo habilita' onclick='cambiaEstado(\"indice\",\"".$array['id']."\")'></div>";
              else
                  //echo "<input id='habilita' type='checkbox' title='Habilita o deshabilita el índice.' onclick='cambiaEstado(\"indice\",\"".$array['id']."\")'>";
                  echo "<div title='Habilita o deshabilita el índice' id='habilita-".$array['id']."' class='usuario-inactivo habilita' onclick='cambiaEstado(\"indice\",\"".$array['id']."\")'></div>";
              echo "<img title='Muestra contenidos del índice.' src='img/logs.png' onclick='mostrarIndiceContenidoSortable(\"contenido\",".$array['id'].")'><img title='Borrar índice' src='img/delete.png'onclick='mostrarBorrarIndiceContenido(\"indiceBorra\",\"none\",".$array['id'].")'>";
              echo "</li>";

        }
      }
    } else {

        echo $crearborrar;

    }
  }

?>
