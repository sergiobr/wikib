<?php

  // Pagina para administrar los usuarios dados de alta.
  $url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
  $patern = "inc/usuariosMostrar.php";
  if (!strpos($url,$patern)) {
    if($_SESSION['group'] == 'administrador'){
    $mysql = conectar_db();

    // Pintamos la tabla con la informacion de usuarios dados de alta en la plataforma.
?>

  <h1>Gesti&oacute;n de usuarios</h1>
  <br />
  <p>En la presente sección se gestionan los usuarios de la plataforma.</p>
  <p>Solo se podr&aacute;n dar de alta usuarios con el perfil <b>"usuario"</b>.</p>
  <p>Cada usuario es <b>&uacute;nico</b>, no pudiendo haber otro con el mismo nombre de usuario.</p>
  <p>El usuario con el perfil <b>"administrador"</b> es &uacute;nico y se da de alta durante la instalaci&oacute;n inicial.</p>
  <br />

  <!-- Resultado de las acciones sobre los buzones -->
  <div id="usuarios-resultado" class="div-resultado">
    <div id="usuarios-resultado-container" class="div-resultado-container">
      <p></p><span class="div-resultado-cerrar" title="Cerrar">X</span>
    </div>
  </div>
  <br />

  <!-- Menu con las opciones de usuarios -->
  <div id="opciones-usuario">
    <button id="crea-usuario-boton" type="button" onclick="mostrarFormNewUser()">Nuevo</button>
    <!--<button id="filtro-usuario-boton" type="button">Filtro</button>-->
    <input list="filtroUsuario" class="input-buscar" type="search" id="filtro-usuario-busca" name="filtro-usuario-busca" onkeydown="filtroUsuarios('filtra')" />&nbsp;Presiona intro
    <datalist id="filtroUsuario">
    <?php
    $listadoUsuarios = obtenusuarios($mysql,"none","none",0);
    while($listado = mysqli_fetch_array($listadoUsuarios)){
        echo '<option value="'.$listado['usuario'].'">';
    }
    ?>
    </datalist>
    <?php
        if(isset($_GET['filtroUsuario']))
            echo '<div id="patron_busqueda" onclick="filtroUsuarios(\'borrarFiltro\')"><span>'.$_GET['filtroUsuario'].'&nbsp;&nbsp;x</span></div>';
        else
            echo '<div id="patron_busqueda"></div>';
    ?>
  </div>

  <!-- Tabla con los usuarios -->
  <div id="usuarios">
  <table id='user-tab-res'>
    <tr>
      <th><a title='Ordena usuario por id' onclick="ordenaUsuarios('id')">ID</a></th>
      <th><a title='Ordena usuario por nombre de usuario' onclick="ordenaUsuarios('usuario')">Usuario</a></th>
      <th><a title='Ordena usuario por nombre' onclick="ordenaUsuarios('nombre')">Nombre</a></th>
      <th><a title='Ordena usuario por email' onclick="ordenaUsuarios('email')">Email</a></th>
      <th><a title='Ordena usuario por grupo' onclick="ordenaUsuarios('grupo')">Grupo</a></th>
      <th><a title='Ordena usuario por último login' onclick="ordenaUsuarios('ultimoLogin')">Ultimo Login</a></th>
      <th><a title='Ordena usuario por IP último login' onclick="ordenaUsuarios('ultimoLoginIp')">IP Ultimo Login</a></th>
      <th><a title='Ordena usuario por estado' onclick="ordenaUsuarios('activo')">Activo/Inactivo</a></th>
      <th title='Opciones sobre el usuario'>Opciones</th>
    </tr>

<?php

    // Obtenemos los usuarios dados de alta.
    if(!isset($_GET['by'])){
      if(!isset($_GET['filtroUsuario'])){
        $usuarios = obtenusuarios($mysql,"none","none",0);
      } else {
        $usuarios = obtenusuarios($mysql,$_GET['filtroUsuario'],"none",0);
      }
    }
    elseif(isset($_GET['filtroUsuario']))
      $usuarios = obtenusuarios($mysql,$_GET['filtroUsuario'],$_GET['by'],$_GET['sort']);
    else
      $usuarios = obtenusuarios($mysql,"none",$_GET['by'],$_GET['sort']);

      while($usuario = mysqli_fetch_array($usuarios)){
      echo "<tr>";
        echo "<td>".$usuario['id']."</td>";
        echo "<td>".$usuario['usuario']."</td>";
        echo "<td>".$usuario['nombre']."</td>";
        echo "<td>".$usuario['email']."</td>";
        echo "<td>".$usuario['grupo']."</td>";
        echo "<td>".$usuario['ultimoLogin']."</td>";
        echo "<td>".$usuario['ultimoLoginIp']."</td>";

        if($usuario['activo'] == 1) {
          if ( $usuario['grupo'] == 'administrador' )
            echo "<td><img title='Acciones no permitidas para el usuario administrador.' id='notallowed-usuario' class='user-mng-icon' src='img/notallowed.png'/></td>";
          else
            echo "<td><div title='Estado del usuario' id='estado-usuario-".$usuario['id']."' class='usuario-activo' onclick='usuarioCambiaEstado(".$usuario['id'].",\"cambiaEstado\")'></div></td>";
        } else {
          echo "<td><div title='Estado del usuario' id='estado-usuario-".$usuario['id']."' class='usuario-inactivo' onclick='usuarioCambiaEstado(".$usuario['id'].",\"cambiaEstado\")'></div></td>";
        }

        if($usuario['grupo'] == 'administrador')
            echo "<td><img title='Acciones no permitidas para el usuario administrador.' id='notallowed-usuario' class='user-mng-icon' src='img/notallowed.png'/></td>";
        else
            echo "<td><img title='Editar datos usuario' id='editar-usuario' class='user-mng-icon' onclick='mostrarFormEditUser(".$usuario['id'].",\"mostrarEdita\")' src='img/useredit.png' /><img title='Cambiar pwd' id='cambia-pwd' class='user-mng-icon' onclick='mostrarFormPwdUser(".$usuario['id'].",\"".$usuario['usuario']."\")' src='img/forgotpwd.png' /><img title='Borrar usuario' id='borrar-usuario-icon' class='user-mng-icon' onclick='mostrarBorrarUsuario(".$usuario['id'].",\"".$usuario['usuario']."\")' src='img/delete.png' /></td>";
      echo "</tr>";
    }

  echo "</table>";
  ?>
  </div>
  <button id="crea-usuario-boton" type="button" onclick="mostrarFormNewUser()">Nuevo</button>

  <!-- Edita usuario -->
  <div id="usuario-form-edit" class="form-oculto">
    <div id="usuario-form-edit-container" class="form-oculto-container">
      <button id="botonCerrarLogin" class="botonCerrarLogin">&#10006;</button>
        <h4 id="edita-usuario-id"></h4>
      <form id="edita-usuario" name="edita-usuario" method="post" action="" onsubmit="return false">
        <input type="hidden" id="edita-usuario-id" name="edita-usuario-id"  />
        <input type="hidden" id="edita-usuario-usuario" name="edita-usuario-usuario" />
        <p>Nombre y Apellidos: </p><input type="text" id="edita-usuario-nombre" name="edita-usuario-nombre" placeholder="Nombre y apellidos" />
        <p>Email: </p><input type="email" id="edita-usuario-email" name="edita-usuario-email" placeholder="Email" pattern="[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" required/>
        <input type="hidden" id="edita-usuario-grupo" name="edita-usuario-grupo" value="usuario" />
        <br /><hr />
        <button id="edita-usuario-boton" type="submit" onclick="actualizaFormEditUser('actualizaDatos')">Edita</button>
        <button type="reset">Borrar</button>
      </form>
    </div>
  </div>

  <!-- Crea nuevo usuario -->
  <div id="usuario-form-new" class="form-oculto">
    <div id="usuario-form-new-container" class="form-oculto-container">
      <button id="botonCerrarLogin2" class="botonCerrarLogin">&#10006;</button>
      <h4>Alta de usuario <img src='img/info.png' id='conf-info' title='Dar de alta un nuevo usuario. Recuerda que el nombre de usuario es único y no se puede repetir. La contraseña debe de contener al menos una letra mayúscula, una minúscula y un número.' /></h4>
      <form id="crea-usuario" name="crea-usuario" method="post" action="" onsubmit="return false">
        <p>Usuario: </p><input type="text" id="crea-usuario-usuario" name="crea-usuario-usuario" placeholder="Usuario" required/>
        <p>Nombre y Apellidos: </p><input type="text" id="crea-usuario-nombre" name="crea-usuario-nombre" placeholder="Nombre y apellidos" />
        <p>Email: </p><input type="email" id="crea-usuario-email" name="crea-usuario-email" placeholder="Email" pattern="[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" required/>
        <input type="hidden" id="crea-usuario-grupo" name="crea-usuario-grupo" value="usuario" />
        <p>Password: </p><input type="password" id="crea-usuario-pwd" name="crea-usuario-pwd" placeholder="Password" require />
        <input type="hidden" id="crea-usuario-pwd-oculta" name="crea-usuario-pwd-oculta" />
        <br />
        <img src="img/eye.png" class="eye" onmouseover="muestraPwd('crea-usuario-pwd','none')" onmouseout="ocultaPwd('crea-usuario-pwd','none')" >
        <p>Estado: </p><select id="crea-usuario-estado" name="crea-usuario-estado" required>
          <option id="habilitado" value="1">Habilitado</option>
          <option id="deshabilitado" value="0">Deshabilitado</option>
        </select>
        <br /><hr />
        <button id="crea-usuario-boton" type="submit" onclick="creaNuevoUsuario('nuevoUsuario')">Crea</button>
        <button type="reset">Borrar</button>
      </form>
    </div>
  </div>

  <!-- Cambia pwd usuario -->
  <div id="usuario-form-pwd" class="form-oculto">
    <div id="usuario-form-pwd-container" class="form-oculto-container">
        <button id="botonCerrarLogin3" class="botonCerrarLogin">&#10006;</button>
        <h4 id="cambia-usuario-pwd"></h4>
        <form id="cambia-pwd-usuario" name="cambia-pwd-usuario" method="post" action="" onsubmit="return false">
            <input type="hidden" id="cambia-usuario-pwd-usuario" name="cambia-usuario-pwd-usuario" />            
            <p>Nueva pwd</p><input type="password" id="cambia-usuario-pwd-1" name="cambia-usuario-pwd-1" placeholder="Pwd" require />
            <input type="hidden" id="cambia-usuario-pwd-1-enc" name="cambia-usuario-pwd-1-enc" />
            <p>Repite pwd</p><input type="password" id="cambia-usuario-pwd-2" name="cambia-usuario-pwd-2" placeholder="Pwd" require />
            <input type="hidden" id="cambia-usuario-pwd-2-enc" mane="cambia-usuario-pwd-2-enc" /><br />
            <img src="img/eye.png" class="eye" onmouseover="muestraPwd('cambia-usuario-pwd-1','cambia-usuario-pwd-2')" onmouseout="ocultaPwd('cambia-usuario-pwd-1','cambia-usuario-pwd-2')" >
            <br /><hr />
            <button id="pwd-usuario-boton" type="submit" onclick="cambiaPwdUsuario('cambiaPwd')">Actualiza</button>
            <button type="reset">Borrar</button>
        </form>
    </div>
  </div>

  <!-- Borrar usuario -->
  <div id="usuario-form-borrar" class="form-oculto">
    <div id="usuario-form-borrar-container" class="form-oculto-container">
        <button id="botonCerrarLogin4" class="botonCerrarLogin">&#10006;</button>
        <h4 id="borrar-usuario"></h4>
        <form id="usuario-borrar" name="usuario-borrar" method="post" action="" onsubmit="return false">
            <p>¿Est&aacute;s seguro de querer borrar al usuario?</p>
            <hr />
            <input type="hidden" id="borrar-usuario-id" name="borrar-usuario-id" />
            <input type="hidden" id="borrar-usuario-usuario" name="borrar-usuario-usuario" />
            <button id="borrar-usuario-boton" type="submit" onclick="borrarUsuario('borrar')">Si</button>
            <button id="borrar-usuario-boton" type="submit" onclick="borrarUsuario('noborrar')">No</button>
        </form>
    </div>
  </div>
<?php
    } else {
      echo "<h1>ERROR</h1>";
      echo "<hr />";
      echo "<p>No tienes permisos para acceder a este espacio.</p>";
    }
  } else {
        // Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
        echo "<div style='margin: 20px 10px;'>";
          echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
          echo "<p>Esta ventana de solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
        echo "</div>";
        error_log("Acceso a la gestion de usuarios no permitido. No se esta accediendo desde el index.");
  }
?>
