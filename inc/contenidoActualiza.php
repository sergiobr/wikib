<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}

// Obtenemos los datos pasados por AJAX.
$contenidoAntiguo = $_POST['contenidoAntiguo'];
$contenidoNuevo = $_POST['contenidoNuevo'];
$id = $_POST['id'];
$user = $_POST['user'];

// Obtenemos la hora actual, fecha y hora en formato dd-mm-YY hh:mm:ss
//$fechaUpdate = date('Y-d-m H:i:s', time());

// Incluimos el fichero con las funciones.
include(dirname(__FILE__)."/../fun/funciones.php");

// Función para conectarnos a la base de datos.
function conectar_db2(){
  // Incluimos el fichero con los datos de configuracion de la plataforma.
  $conf = parse_ini_file(dirname(__FILE__)."/../conf/access.ini",TRUE);
  // Nos conectamos al MySQL, ya que desde el fichero de funciones falla al no ser llamado desde el index.
  $mysql = mysqli_connect($conf['mysql']['servername'],$conf['mysql']['username'],$conf['mysql']['password'],$conf['mysql']['dbname'],$conf['mysql']['portmysql']);
  mysqli_set_charset($mysql,"utf8");
  unset($conf);
  return $mysql;
}

// Nos conectamos al MySQL usando la funcion anterior.
$mysql = conectar_db2();

// Actualizamos los datos del contenido.
$resultado = actualizaContenido($mysql,$contenidoNuevo,$user,$id);

// Si todo va bien guardamos el log de la actualizacion del contenido.
if ($resultado == 'ok') {

  // Obtenemos el titulo del contenido.
  $query = indiceContenido($mysql,'contenido',$id);
  $titulo = mysqli_fetch_array($query);

  // Guardamos el log para mostrarlo en el Wiki seccion log.
  guardarLog($mysql,'actualiza','contenido',$id,$titulo['titulo'],'',$user,$contenidoAntiguo,$contenidoNuevo);

}

// Devolvemos el contenido.
echo $resultado;

?>
