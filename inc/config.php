<?php
//Comprobamos si se llama desde el index.php
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/config.php";
if (!strpos($url,$patern)) {

  // Creamos variables con los datos de configuracion y acceso.
  $config = parseaconf("conf/access.ini",1);
  $config2 = parseaconf("conf/conf.ini",1);



  Logger::info("Acceso a configuracion de WikiB.");
  // Se se ejecuta el formulario de configuracion procesamos los datos desde el archivo indicado a continuacion, pero solo para administradores debido a la cantidad de datos a procesar. De lo contrario mostramos el resultado en la misma pagina.

  // Config Admin.
  if(isset($_POST['guardaconfig'])){
    include("inc/configProcesa.php");
  } else {

  // Formulario de configuracion.
  ?>

  <!-- Comprobamos los datos del archivo de configuracion -->
  <div id="conf-contenido" class="conf-flex-container">

  <?php // Configuracion inicial de la plataforma.
      if($config2['confinicial']['conf'] == 0){
    ?>
          <h1>Configuraci&oacute;n inicial</h1>
          <br />
          <p>Antes de comenzar a utilizar la Wiki colaborativa es necesario que configures una serie de par&aacute;metros esenciales para su uso.</p>
      <?php } else {?>
          <h1>Configuración de la plataforma</h1><br />
      <?php } ?>

    <div id="conf-cabecera">
      <p>Datos de configuración de la plataforma.</p>
      <p>Los campos con el s&iacute;mbolo (<span>*</span>) son obligatorios.</p>
      <p>Es <b>requisito fundamental</b> disponer de la siguiente configuraci&oacute;n previa en el servidor:</p>
      <ul class="list-conf">
        <li>Motor de base de datos MySQL instalado (desde repositorio).</li>
        <li>M&oacute;dulo MySQLi para PHP (<a href="http://php.net/manual/es/book.mysqli.php">enlace</a>).</li>
        <li>Base de datos creada en MySQL para WikiB (<a href="https://dev.mysql.com/doc/refman/5.7/en/creating-database.html">enlace</a>).</li>
        <li>Usuario para conexi&oacute;n a MySQL (<a href="https://dev.mysql.com/doc/refman/5.7/en/adding-users.html">enlace</a>).</li>
        <li>Motor de base de datos Redis instalado (desde repositorio).</li>
        <li>M&oacute;dulo Redis para PHP (<a href="https://serverpilot.io/community/articles/how-to-install-the-php-redis-extension.html">enlace</a>).</li>
        <li>Base de datos reservada en Redis para WikiB.</li>
        <li>PHPMailer instalado y configurado en el servidor (<a href="https://github.com/PHPMailer/PHPMailer">enlace</a>). Recomendamos el uso de <i style="text-decoration: underline;">Composer</i>.</li>
        <li>Los siguientes archivos o link simbólicos a los ficheros originales de PHPMalier en la ruta <b>/usr/share/php/PHPMailer</b></li>
        <ul class="list-conf" style="margin-left: 30px;">
            <li>Exception.php</li>
            <li>PHPMailer.php</li>
            <li>SMTP.php</li>
        </ul>
        <li>Una cuenta de correo v&aacute;lida y sus datos de configuraci&oacute;n.</li>
      </ul>
    </div>

    <p id='expandeInfo' onclick="muestraInfo(this);">Leer m&aacute;s</p>

    <!-- Si no se hizo la configuracion inicial o el usuario es el administrador, mostramos el formuladio de configuracion de admin -->
    <?php if($config2['confinicial']['conf'] == 0 || $_SESSION['group'] == 'administrador'){
        // Incluimos el formulario de configuracion para administradores.
        include("inc/configAdminForm.php");
    }
  }
} else {
// Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
echo "<div style='margin: 20px 10px;'>";
  echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
  echo "<p>La ventana de configuraci&oacute;n solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
echo "</div>";
error_log("Acceso a la ventana de configuracion no permitido. No se esta accediendo desde el index.");
} ?>
