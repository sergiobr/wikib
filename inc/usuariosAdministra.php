<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}

  // Incluimos el fichero con las funciones.
  include(dirname(__FILE__)."/../fun/funciones.php");

  function conectar_db2(){
    // Incluimos el fichero con los datos de configuracion de la plataforma.
    $conf = parse_ini_file(dirname(__FILE__)."/../conf/access.ini",TRUE);
    // Nos conectamos al MySQL, ya que desde el fichero de funciones falla al no ser llamado desde el index.
    $mysql = mysqli_connect($conf['mysql']['servername'],$conf['mysql']['username'],$conf['mysql']['password'],$conf['mysql']['dbname'],$conf['mysql']['portmysql']);
    mysqli_set_charset($mysql,"utf8");
    unset($conf);
    return $mysql;
  }


  // Nos conectamos al MySQL usando la funcion anterior.
  $mysql = conectar_db2();

  if($_POST['command'] == "cambiaEstado"){
    // CAMBIA ESTADO USUARIO.
    // Obtenemos los datos que se pasan por el Ajax de JQuery.
    $id = $_POST['userid'];

    // Obtenemos el estado de la cuenta.
    $estado = estadoCuenta($mysql,$id);

    // Ejecutamos el comando MySQL.
    if($estado['activo'] == '1')
      $comando = 'desactiva';
    else
      $comando = 'activa';

    $cambiaEstado = cambiaEstadoUsuario($mysql,$comando,$id);

    // Devolvemos el resultado del comando.
    if($cambiaEstado == 'ok')
      echo "ok - $comando";
    else
      echo "ko";

  } elseif ($_POST['command'] == "mostrarEdita") {
    // MOSTRAR FORMULARIO PARA EDITAR USUARIO.
    // Obtenemos los datos que se pasan por el Ajax de JQuery.
    $id = $_POST['userid'];

    // Obtenemos los datos del usuario.
    $datos = obtenDatosUsuario($mysql,$id);

    if($datos != "ko"){

      // Devolvemos los datos del usuario para su edicion.
      $idusuario = $datos['id'];
      $usuario = $datos['usuario'];
      $nombre = $datos['nombre'];
      $email = $datos['email'];
      $grupo = $datos['grupo'];
      echo "$idusuario;;;$usuario;;;$nombre;;;$email;;;$grupo";

    } else {

      echo "ko";

    }

  } elseif ($_POST['command'] == 'actualizaDatos'){

    // ACTUALIA DATOS DEL USUARIO.
    // Obtenemos los datos que se pasan por el Ajax de JQuery.
    $id = $_POST['userid'];
    $user = $_POST['user'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $group = $_POST['group'];

    $datos = actualizaDatosUsuario($mysql,$id,$user,$name,$email,$group);
     // Devolvemos el resultado de la funcion anterior.
    if($datos == 'ok') {

      // Si el usuario es el administrador actualizamos los datos del archivo access.ini
      if ( $_POST['group'] == 'administrador' ) {
        actualizalinea('../conf/access.ini','nameadmin = ','nameadmin = "' . $_POST['name'] . '"');
        actualizalinea('../conf/access.ini','useradmin = ','useradmin = "' . $_POST['user'] . '"');
        actualizalinea('../conf/access.ini','emailadmin = ','emailadmin = "' . $_POST['email'] . '"');
      }

      echo 'ok';
    } else {
      echo 'ko';
    }

  } elseif ($_POST['command'] == 'nuevoUsuario'){

    // CREAR NUEVO USUARIO.
    // Obtenemos los datos que se pasan por el Ajax de JQuery.
    $user = $_POST['user'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $group = $_POST['group'];
    $pwd = $_POST['pwd'];
    $state = $_POST['state'];

    $crea = creausuario($mysql,$name,$user,$pwd,$email,$group,$state,1,"../conf/conf.ini");
    echo $crea;

  } elseif ($_POST['command'] == 'cambiaPwd'){

        // CAMBIA PWD.
        // Obtenemos los datos que se pasan por el Ajax de JQuery.
        $user = $_POST['user'];
        $id = $_POST['id'];
        $pwd = $_POST['pwd'];

        $cambia = cambiapwdrecuerda($mysql,$user,$pwd,'si');


        $datos = obtenusuario($mysql,$id);
        $array = mysqli_fetch_array($datos);

        if ( $array['grupo'] == 'administrador' )
          actualizalinea('../conf/access.ini','passwordadmin = ','passwordadmin = "' . $_POST['pwdplain'] . '"');

        echo $cambia;

  } elseif ($_POST['command'] == 'borrar'){

        // BORRAR USUARIO.
        // Obtenemos los datos que se pasan por el Ajax de JQuery.
        $id = $_POST['id'];
        $user = $_POST['user'];

        $borra = borrarUsuario($mysql,$id,$user);
        echo $borra;
  }

?>
