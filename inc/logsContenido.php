<?php

// Bloqueamos el acceso directo al archivo php desde el navegador.
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
    die('<h1>Acceso restringido.</h1><hr><p>No tienes permiso para acceder a este archivo.');
}


// Incluimos el fichero con las funciones.
include(dirname(__FILE__)."/../fun/funciones.php");

// Función para conectarnos a la base de datos.
function conectar_db2(){
  // Incluimos el fichero con los datos de configuracion de la plataforma.
  $conf = parse_ini_file(dirname(__FILE__)."/../conf/access.ini",TRUE);
  // Nos conectamos al MySQL, ya que desde el fichero de funciones falla al no ser llamado desde el index.
  $mysql = mysqli_connect($conf['mysql']['servername'],$conf['mysql']['username'],$conf['mysql']['password'],$conf['mysql']['dbname'],$conf['mysql']['portmysql']);
  mysqli_set_charset($mysql,"utf8");
  unset($conf);
  return $mysql;
}

$mysql = conectar_db2();

// Obtenemos las variables pasadas por AJAX de JQUERY.
$datei = $_GET['datei'];
$datef = $_GET['datef'];
$action = $_GET['action'];
$element = $_GET['element'];
$user = $_GET['user'];

$logs = obtenerLogs($mysql,$datei,$datef,$action,$element,$user);

?>

  <div id='logs'>
    <table id='logsTabla' cellspacing='5' cellpadding='2'>

      <tr>
        <th>Acci&oacute;n</th>
        <th>Elemento</th>
        <th>ID elemento</th>
        <th>T&iacute;tulo elemento</th>
        <th>Comentario</th>
        <th>Fecha</th>
        <th>Usuario</th>
        <th class='responsiveLogs'>Cambios</th>
      </tr>

  <?php
      while($log = mysqli_fetch_array($logs)){

          echo "<tr>";

            echo "<td>" . $log['accion'] . "</td>";
            echo "<td>" . $log['elemento'] . "</td>";
            echo "<td style='text-align: center;'>" . $log['id_elemento'] . "</td>";
            echo "<td>" . $log['titulo_elemento'] . "</td>";
            echo "<td>" . $log['comentario'] . "</td>";
            echo "<td>" . $log['fecha'] . "</td>";
            echo "<td style='text-align: center;'>" . $log['usuario'] . "</td>";
            if ($log['accion'] == 'actualiza' OR $log['accion'] == 'borra'){

              if ($log['elemento'] == 'contenido')

                echo "<td class='responsiveLogs'><img class='user-mng-icon' src='img/searchicon.png' id='logsComparaEnlace' onclick='muestraLogsCompara(" . $log['id'] . ")' title='Compara contenidos antiguo y nuevo.' /></td>";

              else
                echo "<td class='responsiveLogs'></td>";

            } else {

              echo "<td class='responsiveLogs'></td>";

            }

          echo "</tr>";
        }
    ?>

    </table>
  </div>
