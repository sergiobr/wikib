<?php
//Comprobamos si se llama desde el index.php
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/pwdAleatoriaEnvia.php";
if (!strpos($url,$patern)) {

  // Nos conectamos a Redis y confirmamos que la URL aun esta operativa.
  // Creamos la variable con los datos de acceso parseados.
  $config = parseaconf("conf/access.ini","1");
  $hostr = $config['redis']['servernameredis'];
  $portr = $config['redis']['portredis'];
  $dbr = $config['redis']['dbredis'];
  if( $config['redis']['authredis'] != 'nul' OR $config['redis']['authredis'] != "" )
    $authr = $config['redis']['authredis'];
  else
    $authr = "";

    // Eliminamos la variable con los datos de acceso parseados.
  unset($config);

  // Conexion a Redis.
  $redis = rediscon($hostr,$portr,$dbr,$authr);

  // Confirmamos que no hay ningun error de conexion.
  if ( is_string($redis) and preg_match("#^ERROR - #",$redis) ){
    echo "<script>alert('$redis');</script>";

  } else {
    // Confirmamos que la URL aun existe.
    $url = explode(":",$_GET['recuperapwd']);
    $key = $url[0];
    $value = $url[1];
    $result = $redis->get($key);

    if($result == 'nul' or $result == ""){
      // Si no existe mostramos una advertencia y redirigimos al inicio tras 5 segundos.
      echo "<h1>Enlace caducado</h1>";
      echo "<p>El enlace para la recuperaci&oacute;n de la contrase&ntilde;a ha caducado.</p>";
      echo "<p>Debes de generar una nueva credencial.</p>";
      echo "<p>Ser&aacute;s redirigido al inicio en <b id='counter'>5</b> segundos.</p>";
      echo "<button id='botonInicio'>Inicio</button>";
      echo "<script>redirectCountdown(5,'/');</script>";
      error_log("Enlace de recordatorio de pwd caducado - Usuario: ".$key." - Codigo: ".$value.".",0);

    } elseif ($value != $result) {
      // Si el codigo que aparece en la URL no coincide con el generado y almacenado en Redis mostramos un error y redirigimos al inicio tras 5 segundos.
      echo "<h1>Enlace incorrecto.</h1>";
      echo "<p>El enlace al que est&aacute;s accediendo no es correcto.</p>";
      echo "<p>Por favor, genera una nueva credencial.</p>";
      echo "<p>Ser&aacute;s redirigido al inicio en <b id='counter'>5</b> segundos.</p>";
      echo "<button id='botonInicio'>Inicio</button>";
      echo "<script>redirectCountdown(5,'/');</script>";
      error_log("Enlace de recordatorio de pwd incorrecto, no coincide con el almacenado el Redis - Usuario: ".$key." - CodigoRedis: ".$result." - CodigoURL: ".$value.".",0);

    } else {
      // Si existe generamos una pwd aleatoria, la actualizamos para el usuario y la reenviamos por correo.
      // Conectamos con mysql y confirmamos conexion.
      $mysql = conectar_db();

      if (is_string($mysql) and preg_match("#^ERROR - #",$mysql)) {
        // Si la conexión va mal...
        echo '<script>alert("'.$mysql.'");</script>';

      } else {
        // Si va bien...
        Logger::info("Conectado a la base de datos correctamente. Cambiando pwd al usuario $key.");

        // Generamos una pwd aleatoria.
        $pwd = bin2hex(openssl_random_pseudo_bytes(4));

        // Cambiamos la pwd encriptada.
        $pwdCambio = cambiapwdrecuerda($mysql,$key,$pwd,'no');

        // Si el comando anterior devuelve ok enviamos el mensaje con la nueva credencial, de lo contrario informamos del error.
        if( $pwdCambio == "ok" ){
          Logger::info("Cambiada pwd al usuario $key.");

          // Confirmamos si el usuario es el administrador o no para actualizar el archivo access.ini
          $usuarioadmin = compruebaadmin($mysql);
          if($usuarioadmin['usuario'] == strtolower($key)){

            // Si el usuario pertenece al grupo administrador, actualizamos el archivo access.ini tambien.
            $actualiza = actualizalinea('conf/access.ini','passwordadmin = ',"passwordadmin = \"$pwd\"");

            if($actualiza == 'ok')
              Logger::warn("Actualizado archivo access.ini");
            else
              error_log("ERROR - Archivo access.ini no actualizado", 0);
          }

          // Obtenemos el email para enviar el correo.
          $email = obtenemail($mysql,$key);

          if(isset($email['email'])){
            // Enviamos el correo con la nueva credencial.
            enviarcorreo($pwd,$email['email'],'nuevaPwd');
            // Eliminamos el string en Redis para restablecer la pwd.
            $redis->delete($key);
            // Mostramos mensaje de PWD cambiada y redirigimos al inicio tras 5 segundos.
            echo "<h1>Contrase&ntilde;a cambiada</h1>";
            echo "<p>Tu contrase&ntilde;a ha sido cambiada.</p>";
            echo "<p>Recibir&aacute;s los datos de acceso por correo electr&oacute;nico.</p>";
            echo "<p>Recuerda cambiarla una vez accedas, por favor.</p>";
            echo "<p>Ser&aacute;s redirigido al inicio en <b id='counter'>5</b> segundos.</p>";
            echo "<button id='botonInicio'>Inicio</button>";
            echo "<script>redirectCountdown(5,'/');</script>";

          } else {
            error_log('Error al intentar obtener el email para el usuario $key',0);
            echo "<script>alert('Error al intentar obtener el email para el usuario ". $key .".');inicio();</script>";
          }

          mysqli_close($mysql);
        } else {
          error_log("Error al cambiar la pwd al usuario $key",0);
          echo "<script>alert('Error al cambiar la pwd al usuario ". $key .".'); inicio();</script>";
        }
      }
    }
  }
} else {
  // Si no se llama desde el index.php mostramos un error, ya que se esta intentando acceder directamente al archivo que contiene el login.
  echo "<div style='margin: 20px 10px;'>";
    echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
    echo "<p>La ventana de login solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
  echo "</div>";
  error_log("Acceso a la ventana de login no permitido. No se esta accediendo desde el index.");
}
?>
