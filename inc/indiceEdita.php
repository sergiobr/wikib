<?php
// Pagina para administrar el indice y los contenidos.
$url = "http://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
$patern = "inc/editaIndice.php";

// Confirmamos si este espacio se llama desde el index.php o de lo contrario si se intenta acceder al php directamente.
if (!strpos($url,$patern)) {
    // Se se inicio sesion mostramos el contenido con el formulario para la gestion del indice y los contenidos.
    if(isset($_SESSION['id'])){
       
        echo "<h1>Editor del &iacute;ndice y contenidos</h1><br>";
        echo "<p>A continuación podrás editar el contenido y orden de los elementos del menú lateral izquierdo, en donde se muestra el índice y los contenidos de cada uno de estos.</p>";
        echo "<p>Ten en cuenta que no podrás eliminar un índice si tiene contenidos asociados. Antes tendrás o bien que borrar los contenidos asociados al índice o cambiar su índice padre.</p>";
        echo "<p>Para cambiar el orden de los elementos solo tienes que <b>pinchar</b> y <b>arrastrar</b> al mismo tiempo, soltando en la posición deseada.</p>";
        
        
        // Seccion para administrar el indice y los contenidos.
?>

        <div class="div-resultado" id="indice-resultado">
            <div class="div-resultado-container">
                <p></p><span class="div-resultado-cerrar" title="Cerrar">X</span>
            </div>
        </div>

        <div id="gestion-indice-contenido" class="flex-container">
            <div id="gestion-indice" class="flex-item">
                
                <h3>Indices</h3>           
                <form id="formNuevoIndice" name="formNuevoIndice" onsubmit="return false">
                    <input id="casillanuevoindice" type="text" placeholder="Nuevo índice" required>                
                    <button id="botonnuevoindice" type="submit" onclick="nuevoIndice('indiceAgrega')">Nuevo</button>                
                </form>
                
                <ul id="sortable-indice" class="sortable"></ul>

            </div>
             
            <div id="gestion-contenidos" class="flex-item">
                
                <h3>Contenidos del índice <span id="indiceId"></span></h3>
                <form id="formNuevoContenido" name="formNuevoContenido" onsubmit="return false">
                    <input id="casillanuevocontenido" type="text" placeholder="Nuevo contenido" required>                
                    <button id="botonnuevoindice" type="submit" onclick="nuevoIndice('contenidoAgrega')">Nuevo</button>                
                </form>
                
                <ul id="sortable-contenido" class="sortable"></ul>
            </div>
            
        </div>
        
          <!-- Borrar indice / contenido -->
          <div id="indicecontenido-form-borrar" class="form-oculto">
            <div id="indicecontenido-form-borrar-container" class="form-oculto-container">
                <button id="botonCerrar" class="botonCerrarLogin">&#10006;</button>
                <h4 id="borrar-indicecontenido"></h4>
                <form id="indicecontenido-borrar" name="indicecontenido-borrar" method="post" action="" onsubmit="return false">
                    <p>¿Est&aacute;s seguro de querer borrar el &iacute;ndice o el contenido?</p>
                    <hr />
                    <input type="hidden" id="indicecontenido-mostrar" name="indicecontenido-mostrar" />
                    <input type="hidden" id="indicecontenido-titulo" name="indicecontenido-titulo" />
                    <input type="hidden" id="indicecontenido-id" name="indicecontenido-id" />
                    <button id="borrar-usuario-boton" type="submit" onclick="borrarIndiceContenido('borrar')">Si</button>
                    <button id="borrar-usuario-boton" type="submit" onclick="borrarIndiceContenido('noborrar')">No</button>
                </form>
            </div>
          </div>
          
          <!-- Cambiar indice al contenido -->
          <div id="contenido-form-cambia-indice" class="form-oculto">
            <div id="contenido-form-cambia-indice-container" class="form-oculto-container">
                <button id="botonCerrar" class="botonCerrarLogin">&#10006;</button>
                <h4 id="cambiar-contenido-indice"></h4>
                <form id="contenido-cambia-indice" name="contenido-cambia-indice" method="post" action="" onsubmit="return false">
                    <p>Selecciona el nuevo &iacute;ndice para el contenido.</p>
                    <hr />
                    <p>Nuevo &iacute;ndice</p>
                    <input type="hidden" id="contenido-cambia-indice-contenido" name="contenido-cambia-indice-contenido">
                    <input type="hidden" id="contenido-cambia-indice-indiceanterior" name="contenido-cambia-indice-indice">
                    <select id="nuevo-indice-selecciona" name="nuevo-indice-selecciona" required>
                    
                    </select>
                    <br><hr>
                    <button id="cambiar-contenido-indice-boton" type="submit" onclick="cambiaIndiceAContenido('cambiar','none','none')">Cambiar</button>
                    <button id="cambiar-contenido-indice-boton" type="submit" onclick="cambiaIndiceAContenido('nocambiar','none','none')">Cancelar</button>
                </form>
            </div>
          </div>

<?php        
        
    } else {
    
        // De lo contrario mostramos un error al no estar logeado el usuario en el sistema.
        echo "<h1>ERROR</h1>";
        echo "<hr />";
        echo "<p>No tienes permisos para acceder a este espacio.</p>";
    
    }
} else {

    // Si no se llama desde el index.php mostramos un error.
    echo "<div style='margin: 20px 10px;'>";
    echo "<h3 style='border-bottom: 1px solid lightgrey;'>ERROR - acceso denegado.</h3>";
    echo "<p>Esta ventana de solo puede ser llamada desde la p&aacutegina principal, pinchando <a href='http://".$_SERVER['SERVER_NAME']."'>aqu&iacute.</a></p>";
    echo "</div>";
    error_log("Acceso a la gestion del indice y contenidos no permitido. No se esta accediendo desde el index.");
}
   
?>