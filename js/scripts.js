// Variables globales.
// Tiempo de ejecucion de efectos toggle o fadeOut.
var tiempoDisplay = 500;
var backColorOk = "lightgreen";
var backColorKo = "#ff8080";



// Codificar PWDs
// Valor aleatorio:
function randString(x){
  var s = "";
  while(s.length<x&&x>0){
    var r = Math.random();
    s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
  }
  return s;
}
// Enciptar pwd.
// Encriptar pwd:
function cambiar(pwdid,inputcrypt,inputpwd,form){
  var pwd = document.getElementById(pwdid);
  var x = pwd.value;
  // Generamos la pwd encriptada:
  var strmd5 = $().crypt({
    method: "md5",
    source: x
  });
  // Agregamos la pwd encriptada al input oculto:
  $(inputcrypt).val(strmd5);
  // Generamos un valor aleatorio de x caracteres y sustituimos el dato introducido en el formulario en texto plano:
  var randomVal = randString(10);
  $(inputpwd).val(randomVal);
  // Enviamos el contenido del formulario:
  //$(form).submit();
}

// Cuando carga la pagina cargamos los siguientes JQUERYs.
$(document).ready(function(){

    // Mensaje para la politica de cookies.
    $("#mensaje-politica-cookies").fadeIn('slow');
    // Al darle OK escondemos el mensaje de politicas de cookies.
    $("#cookie-ok").click(function() {
      var date = new Date();
      date.setTime(date.getTime()+(1*24*60*60*1000));
      var expires = "; expires="+date.toGMTString();
      document.cookie = 'politicacookies=1;expires='+expires+';path=/';
      $("#mensaje-politica-cookies").fadeOut('slow');
    });

    // Funcion para el tooltip de JQUIERYUI
    $( document ).tooltip();

    // Mostrar ventana de login
    $('#botonLogin').click(function() {
      $('#fondo-negro').fadeIn('slow');
      $('#login-usuario').fadeIn('slow');
    });

    // OCULTAR VENTANAS ACTIVAS.
    // Boton cerrar en formularios ocultos.
    $('.botonCerrarLogin').click(function() {
      $('.form-oculto').fadeOut('slow');
      $("#logsComparaOculto").css('visibility', 'hidden');
      $('#fondo-negro').fadeOut('slow');
    });
    // Si pinchamos en el fondo-negro ocultamos cualquier ventana activa.
    $('#fondo-negro').click(function(){
      $('.form-oculto').fadeOut('slow');
      $("#logsComparaOculto").css('visibility', 'hidden');
      $('#fondo-negro').fadeOut('slow');
    });
    // Si pulsamos la tecla esc ocultamos cualquier ventana activa.
    $(document).keyup(function(e){
      if(e.keyCode == 27){
        $('.form-oculto').fadeOut('slow');
        $("#logsComparaOculto").css('visibility', 'hidden');
        $('#fondo-negro').fadeOut('slow');
      }
    });

    // Funcion para mostrar formuario de recordar pwd
    // agrandando o no el alto del formulario cuando se muestra u oculta el recordar pwd
    $("#recordarPwd").click(function() {
      var alto = $('#login-usuario').css('height');
      if ( alto == '250px') {
        $("#login-usuario").animate({height:350}, tiempoDisplay);
      } else {
        $("#login-usuario").animate({height:250}, tiempoDisplay);
      }
      var estado = $('#recordar').css('display');
      if ( estado == 'none') {
        $("#recordar").fadeIn();
      } else {
        $("#recordar").fadeOut();
      }
    });

    // Botol INICIO.
    $("#botonInicio").click(function() {
      window.location = "/";
    });

    // PWD LOGIN
    //Codificamos la pwd agregada por el usuario y cambiamos la introducida en el formulario para evitar ser sustraida durante el envio de los datos al servidor.
    $('#boton-login').click(function(){
      // Ejecutamos el cambio de pwd y encriptacion de la indicada por el abonado para el login.
      cambiar("password","#passwordcrypt","#password","#loginUsuario");
      document.form.submit();
    });

    // Mostrar menu para imagen cabecera al hacer click en "Mostrar imagen en cabecera"
    $("#conf-cabeceraimagen").click(function(){
      $("#logo-personalizado").toggle('slow');
    });

    // Mostrar imagen personalizada o la de por defecto al hacer click en "Imagen de cabecera personalizada"
    $("#conf-cabeceraimagenperso").click(function(){
      mostrarocultarlogodefault();
    });

    // Mostrar imagen favicon al hacer click en "Mostrar Favicon"
    $("#conf-faviconimagen").click(function(){
      mostrarocultarfavicon();
    });

    // Mostrar imagen favicon al hacer click en "Favicon Personalizado"
    $("#conf-faviconpersonal").click(function() {
        mostrarocultarfaviconpersonal();
    });

    // Boton arriba.
    // Mostrar:
    $(".scroll-up").scroll(function(){
      if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
        $("#topBtn").show('slow');
      } else {
        $("#topBtn").hide('slow');
      }
    });
    // Ir arriba:
    $("#topBtn").click(function() {
      var x = document.getElementById("topBtn").parentElement.nodeName;
      $(x).animate({scrollTop: 0}, 'slow');
    });

    // Mostrar que campos queremos configurar en formulario.
    $("#conf-mysql").click(function(){
      $("#mysql").toggle("slow");
    });
    $("#conf-redis").click(function(){
      $("#redis").toggle("slow");
    });
    $("#conf-admin").click(function(){
      $("#admin").toggle("slow");
    });
    $("#conf-email").click(function(){
      $("#email").toggle("slow");
    });
    $("#conf-emailsmtpauth").click(function(){
      $("#conf-emailcredenciales").toggle("slow");
    });
    $("#conf-header").click(function(){
      $("#header").toggle("slow");
    });

    // Cerrar div con resultado de una accion.
    $('.div-resultado-cerrar').click(function(){
        $('.div-resultado').toggle('blind','',tiempoDisplay);
    });

    // Cerrar logs filtro aplicado, redirigir a logs global.
    $('.div-resultado-cerrar-filtro-log').click(function(){
      window.location.href = '/?logs=1';
    });

    // INDICE Y CONTENIDOS

    // Sortable para ordenar listas (indices y contenidos)
    // Indice:
    $( "#sortable-indice" ).sortable({
      axis: 'y',
      update: function (event, ui){
        var data = $(this).sortable('serialize');
        $.ajax({
          data: data,
          type: 'POST',
          url: 'inc/indiceOrden.php'
        });
      }
    });
    // Contenidos:
    $( "#sortable-contenido" ).sortable({
      axis: 'y',
      update: function (event, ui){
        var data = $(this).sortable('serialize');
        $.ajax({
          data: data,
          type: 'POST',
          url: 'inc/indiceContenidosOrden.php'
        });
      }
    });

    // Mostrar indice sortable.
    mostrarIndiceContenidoSortable('indice','none');

    // Mostrar editor contenido.
    $('#editaContenido').click(function(){
      $('.editor').toggle('blind','',tiempoDisplay);
      // Deshabilitar el tooltip con el title del area de trabajo cuando el cursor se pone encima.
      $('#texto_ifr').prop('title','');
    });

    // Cerrar el editor.
    $('#cerrarEditor').click(function(){
      $('.editor').toggle('blind','',tiempoDisplay);
    });

    // Expandir / contraer indices.
    $(".expande").click(function(){
      $header = $(this);
      $content = $header.next();
      $id = $content.attr('id');

      $content.slideToggle(tiempoDisplay, function(){
        $header.html(function(){
          return $content.is(":visible") ? "&#9660;" : "&#9658;" ;
        });

        // Creamos las cookies para mostrar el menú lateral izquierdo de indices desplegado / replegado lo que corresponda.
        if ( $content.is(":visible")){
          document.cookie =  $id + "=1";
        } else {
          document.cookie = $id + "=;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
        }
      });
    });


    // Iniciamos el editor HTML TinyMCE.
    tinyMCE.init({
        selector : "#texto",
        height : 200,
        language : "es",
        language_url : "js/editor/tinymce/js/tinymce/langs/es.js",
        theme : "modern",
        skin : "lightgray",
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern imagetools autosave'
        ],
        menubar: 'file edit view insert format table',
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | searchreplace',
        toolbar2: 'print preview media | forecolor backcolor emoticons | table | fullscreen | restoredraft | code | anchor | nonbreaking',
        branding: false,
        paste_data_images: true,
        image_advtab: true,
        statusbar: true,
        autosave_ask_before_unload: true,
        autosave_interval: "20s",
        autosave_retention: "30m",
        contextmenu_never_use_native: true,
        contextmenu: "link image inserttable | cell row column deletetable",
        paste_data_images: false,
        paste_as_text: true,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $("#img").unbind( "change" );
                $("#img").change(function(event) { onFileChosen(event,callback);});
                $("#img").click();
            }
        },
        convert_urls: false,
        nonbreaking_force_tab: true,
        forced_root_block: false,
        block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
        style_formats: [
          { title: 'CodigoPersonal', block: 'code', styles: { display: 'block', 'background-color': 'black', color: 'white', 'margin': '10px auto', 'font-weight': 'bold', 'padding': '10px', 'max-width': '80%', 'border-radius': '5px', overflow: 'auto' } },
          { title: 'Resaltar', inline: 'span', styles: { display: 'inline-block', border: '1px solid #2276d2', 'border-radius': '5px', padding: '2px 5px', margin: '0 2px', color: '#2276d2', 'font-weight': 'bold' } },
          { title: 'OjoCuidado', inline: 'span', styles: { display: 'inline-block', color: 'red', 'font-weight': 'bold', border: '1px solid #2276d2', 'border-radius': '5px', padding: '2px 5px', margin: '0 2px' } }
        ],
        formats: {

        },
        style_formats_merge: true,
        forced_root_block : '',
        force_p_newlines : true,
        force_br_newlines : true,
       });



       // Logs copiar antiguo/nuevo cambiar borde contenido afectado.
       $('#copiaAntiguo').hover(function(){
         $('#logsComparaAntiguo').css('border-color', 'red');
       },function(){
         $('#logsComparaAntiguo').css('border-color', 'black');
       });
       $('#copiaNuevo').hover(function(){
         $('#logsComparaNuevo').css('border-color', 'red');
       },function(){
         $('#logsComparaNuevo').css('border-color', 'black');
       });

       // RESPONSIVE DESIGN
       $('#muestraMenuIzq, #muestraMenuIzqOculta').click(function(){
         $('#izq').animate({width: 'toggle'});
       });


       // Buscar patron al pulsar la tecla intro en la casilla de busqueda.
       $("#buscaPatronInput").on('keyup', function(e){
         if (e.keyCode == 13){
           var busqueda = document.getElementById('buscaPatronInput').value;

           // Sustituimos los espacios en blanco por %20.
           busqueda = encodeURIComponent(busqueda.trim());

           // Redirigimos a la URL de busqueda con el patron introducido.
           var protocol = window.location.protocol;
           var domain = window.location.hostname;
           var url = protocol + '//' + domain + '/?busqueda=' + busqueda;
           window.location.href = url;
         }
       });


      // Marcar boton menu en funcion de la URL.
      // Edita indice/contenido
      if ( window.location.href.indexOf('indiceEdita=') > 0 )
        $("#editaIcon").css('background-color', 'lightblue');
      // Logs
      if ( window.location.href.indexOf('logs=') > 0 )
        $("#logsIcon").css('background-color', 'lightblue');
      // Conf usuario
      if ( window.location.href.indexOf('confusuario=') > 0 )
        $("#confUserIcon").css('background-color', 'lightblue');
      // ConfUsuarios
      if ( window.location.href.indexOf('confusuarios=') > 0 )
        $("#usersIcon").css('background-color', 'lightblue');
      // Conf plataforma
      if ( window.location.href.indexOf('confplataforma=') > 0 )
        $("#confIcon").css('background-color', 'lightblue');


// FIN JQUERY
});

// Funcion para guardar en el contenido desde el editor TinyMCE.
function actualizaContenido(id,user){

  //$('#contenidooculto').val(tinyMCE.get('texto').getContent({format: 'html'}));
  //var contenidoNuevo = document.getElementById('contenidooculto').value;
  var contenidoNuevo = tinyMCE.get('texto').getContent({format : 'html'});
  var contenidoAntiguo = document.getElementById('texto').value;

  $.ajax({
    type: 'POST',
    url: 'inc/contenidoActualiza.php',
    data: {'id': id, 'user': user, 'contenidoNuevo': contenidoNuevo , 'contenidoAntiguo': contenidoAntiguo},
    success: function(data){
      if(data == 'ok'){
        alert('Contenido actualizado.');
        tinyMCE.get('texto').save();
        location.reload();
      } else {
        alert('No se pudo actualizar el contenido\n' + data);
        location.reload();
      }
    }
  });

}

// Funcion para poder cargar imagenes en el editor HTML, que es llamada desde el JQUERY de TinyMCE.
function onFileChosen(event,callback)
{
  //Detach any current submit handlers
  $("#tinymce").unbind("submit");
  $("#tinymce").submit(function(e) {

    e.stopPropagation(); // Stop stuff happening
    e.preventDefault(); // Totally stop stuff happening

    //Prepare file in form for transmission via ajax call
    var formData = new FormData();
    $.each(event.target.files, function(i, file) {
        formData.append('file-'+i, file);
    });

    //The url that will handle the file upload
    var url = "inc/contenidoUpload.php";

    //Do ajax call
    $.ajax({
      type: "POST",
      url: url,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(data)
      {
        //You can process the response however you want, but I chose to return a json string
        //var response = JSON.parse(data)[0];
        var response = JSON.parse(data);

        if(response.filename)
        //if(response.location == true)
        {
          //This is the important part. This callback will tell TinyMCE the path of the uploaded image
          //callback(response.newPath);
          var imgpath = "img/subidas/" + response.filename;
          callback(imgpath);
        }
        else{
          var mensaje = "";
          Object.keys(response).forEach(function(key) {
            mensaje += key + ": " + response[key] + "\n";
          });
          alert("Error al subir el archivo.\n" + mensaje);
        }
      },
      error: function(data){
        alert("Error al subir el archivo.");
      },
    });

  });
  $("#tinymce").submit();
}



// Funciones para mostrar / ocultar la pwd de las casillas al poner el cursor encima del icono del ojo.
function muestraPwd(elemento1,elemento2){
  var obj1 = document.getElementById(elemento1);
  obj1.type = "text";
  if (elemento2 != 'none'){
    var obj2 = document.getElementById(elemento2);
    obj2.type = "text";
  }
}
function ocultaPwd(elemento1,elemento2){
  var obj1 = document.getElementById(elemento1);
  obj1.type = "password";
  if (elemento2 != 'none'){
    var obj2 = document.getElementById(elemento2);
    obj2.type = "password";
  }
}


// Politica credenciales.
function politicaPwd(pwd1,pwd2){
    // Creamos un array con los errores.
    var errors = [];
    // Chequeamos las politicas de seguridad.
    if(pwd1 != 'none'){
        if(pwd1 != pwd2){
            errors.push("Las contraseñas no son iguales.");
        }
    }
    if(pwd1.length < 8){
        errors.push("La contraseña tiene menos de 8 caracteres.");
    }
    if(pwd1.search(/[a-z]/) < 0){
        errors.push("Debe contener al menos una letra minúscula.");
    }
    if(pwd1.search(/[A-Z]/) < 0){
        errors.push("Debe contener al menos una letra mayúscula.");
    }
    if(pwd1.search(/[0-9]/) < 0){
        errors.push("Debe contener al menos un número.");
    }

    // Devolvemos el array con los errores, vacio si no se cumple ningun error.
    return errors;
}

// Volver a la pagina inicial.
function inicio(){
  window.location.href = '/';
}

// Volver una pagina atras en el historial.
function volver(){
  javascript:history.go(-1);
}
function volver2(){
  window.history.go(-1);
}

// Prueba de alerta
function mensajeAlerta(mensaje){
  alert(mensaje);
}

// Cuenta atras para redirigir a otra URL.
function redirectCountdown(sec,url){
  var timeleft = sec;
  var downloadTimer = setInterval(function(){
    timeleft--;
    if(timeleft == 0)
      window.location.href=url
    document.getElementById("counter").textContent = timeleft;
    if(timeleft <= 0)
      clearInterval(downloadTimer);
  }, 1000);
}

function reiniciaForm(formID){
  document.getElementById(formID).reset();
}

// Funcion mostrar mysql en configuracion.
function mostrarmysql(){
  if($("#conf-mysql").is(":checked") || $("#conf-mysql").val() == "oculto")
    document.getElementById("mysql").style.display = 'block';
  else
    document.getElementById("mysql").style.display = 'none';
}

// Funcion mostrar redis en configuracion.
function mostrarredis(){
  if($("#conf-redis").is(":checked") || $("#conf-redis").val() == "oculto")
      document.getElementById("redis").style.display = 'block';
  else
    document.getElementById("redis").style.display = 'none';
}

// Funcion mostrar admin en configuracion.
function mostraradmin(){
  if($("#conf-admin").is(":checked") || $("#conf-admin").val() == "oculto")
    document.getElementById("admin").style.display = 'block';
  else
    document.getElementById("admin").style.display = 'none';
}

// Funcion mostrar correo en configuracion.
function mostrarcorreo(){
  if($("#conf-email").is(":checked") || $("#conf-email").val() == "oculto")
    document.getElementById("email").style.display = 'block';
  else
    document.getElementById("email").style.display = 'none';
}

// Funcion para mostrar informacion smtpauth
function mostrarsmtpauth(){
  if($("#conf-emailsmtpauth").is(":checked")){
    document.getElementById('conf-emailsmtpauth').value = 'ok';
    document.getElementById('conf-emailcredenciales').style.display = 'block';
  } else {
    document.getElementById('conf-emailsmtpauth').value = 'ko';
    document.getElementById('conf-emailcredenciales').style.display = 'none';
  }
}


// Funcion mostrar datos de cabecera en configuración.
function mostrarcabecera(){
  if($("#conf-header").is(":checked") || $("#conf-header").val() == "oculto")
    document.getElementById("header").style.display = 'block';
  else
    document.getElementById("header").style.display = 'none';
}

// Funcion mostrar logo en configuración.
function mostrarlogo(){
  if($("#conf-cabeceraimagen").is(":checked"))
    document.getElementById('logo-personalizado').style.display = 'block';
  else
    document.getElementById('logo-personalizado').style.display = 'none';
}

// Funcion mostrar logo personalizado en configuración.
function mostrarlogopersonalizado(){
  if($("#conf-cabeceraimagenperso").is(":checked")){
    document.getElementById("defaultimagen").style.display = 'none';
    document.getElementById("subeimagen").style.display = 'block';
  } else {
    document.getElementById("defaultimagen").style.display = 'block';
    document.getElementById("subeimagen").style.display = 'none';
  }
}

// Funciones para mostrar u ocultar logo por defecto o personalizado y favicon.
function mostrarlogodefault(){
  $("#subeimagen").fadeOut('fast');
  $("#defaultimagen").fadeIn('slow');
}
function ocultarlogodefault(){
  $("#defaultimagen").fadeOut('fast');
  $("#subeimagen").fadeIn('slow');
}
function mostrarocultarlogodefault(){
  $("#defaultimagen").toggle('slow');
  $("#subeimagen").toggle('slow');
}
function mostrarocultarfavicon(){
  $("#favicon").toggle('slow');
}
function mostrarocultarfaviconpersonal(){
  $("#subefavicon").toggle('slow');
  $("#defaultfavicon").toggle('slow');
}

// Funcion mostrar seccion favicon.
function mostrarfavicon(){
  if($("#conf-faviconimagen").is(":checked"))
    document.getElementById("favicon").style.display = 'block';
  else
    document.getElementById("favicon").style.display = 'none';
}

// Funcion mostrar favicon personal o default.
function mostrarfaviconpersonal(){
  if($("#conf-faviconpersonal").is(":checked")){
    document.getElementById("subefavicon").style.display = 'block';
    document.getElementById("defaultfavicon").style.display = 'none';
  } else {
    document.getElementById("subefavicon").style.display = 'none';
    document.getElementById("defaultfavicon").style.display = 'block';
  }
}

// Funcion activar/desactivar usuario
function usuarioCambiaEstado(id,comando){

  $.post("inc/usuariosAdministra.php",
    {
      command : comando,
      userid : id
    },
    function(data,status){
      //alert('ID: ' + id + ' Data: ' + data + ' - Status: ' + status);
      if(status == 'success'){
        if(data == 'ok - activa'){
          $('#usuarios-resultado').hide();
          $('#estado-usuario-'+id).css('background-color','green');
          $('#usuarios-resultado-container p').html("Usuario ID: " + id + " <b>activado</b> correctamente.");
          $('#usuarios-resultado').css('background-color',backColorOk)
                                  .toggle('blind','',tiempoDisplay);
        } else if(data == 'ok - desactiva'){
          $('#usuarios-resultado').hide();
          $('#estado-usuario-'+id).css('background-color','red');
          $('#usuarios-resultado-container p').html("Usuario ID: " + id + " <b>desactivado</b> correctamente.");
          $('#usuarios-resultado').css('background-color',backColorOk)
                                  .toggle('blind','',tiempoDisplay);
        } else {
          $('#usuarios-resultado').hide();
          $('#usuarios-resultado-container p').html("ERROR - No se pudo ejecutar la acción solicitada (cambio de estado al usuario con id " + id + ").");
          $('#usuarios-resultado').css('background-color',backColorKo)
                                  .toggle('blind','',tiempoDisplay);

        }
      } else {
        $('#usuarios-resultado').hide();
        $('#usuarios-resultado-container p').html("ERROR - No se pudo ejecutar la acción solicitada (cambio de estado al usuario con id " + id + ").");
        $('#usuarios-resultado').css('background-color',backColorKo)
                                .toggle('blind','',tiempoDisplay);
      }
    }
  );
}

// Funcion para obtener los datos de un usuario para su edicion.
function mostrarFormEditUser(id,comando){

    $.post("inc/usuariosAdministra.php",
      {
        command : comando,
        userid : id
      },
      function(data,status){
        if(status == 'success'){
          if(data != 'ko'){
          var partes = data.split(";;;");
            $('#edita-usuario-id').html("Edita usuario id " + id + " - " + partes[1] + " <img src='img/info.png' id='conf-info' title='Edita los datos del usuario. Recuerda que el nombre de usuario es único y no se puede repetir.' />");
            $('#edita-usuario-id').val(partes[0]);
            $('#edita-usuario-usuario').val(partes[1]);
            $('#edita-usuario-nombre').val(partes[2]);
            $('#edita-usuario-email').val(partes[3]);
            $('#edita-usuario-grupo').val(partes[4]);
            // Mostrar seccion editar usuario.
            $('#usuario-form-edit').toggle('blind','',tiempoDisplay);
            $('#fondo-negro').fadeIn("slow");
          } else {
            $('#usuarios-resultado').css('display', 'none');
            $('#usuarios-resultado-container p').html("ERROR - No se pudo editar el usuario " + id + ".");
            $('#usuarios-resultado').css('background-color',backColorKo)
                                    .toggle('blind','',tiempoDisplay);
          }
        } else {
          $('#usuarios-resultado').css('display', 'none');
          $('#usuarios-resultado-container p').html("ERROR - No se pudo editar el usuario " + id + ".");
          $('#usuarios-resultado').css('background-color',backColorKo)
                                  .toggle('blind','',tiempoDisplay);
        }
      }
    );
}

// Funcion para actualizar los datos de un usuario para su edicion.
function actualizaFormEditUser(comando){
  $('form[name=edita-usuario]').submit(function(){
    var id = document.getElementById('edita-usuario-id').value;
    var usuario = document.getElementById('edita-usuario-usuario').value;
    var nombre = document.getElementById('edita-usuario-nombre').value;
    var correo = document.getElementById('edita-usuario-email').value;
    var grupo = document.getElementById('edita-usuario-grupo').value;

    $.post("inc/usuariosAdministra.php",
      {
        command : comando,
        userid : id,
        user : usuario,
        name : nombre,
        email : correo,
        group : grupo
      },
      function(data,status){
        if(status == 'success') {
          if(data == 'ok'){
            $('#usuarios-resultado').css('display', 'none');
            $('#usuario-form-edit').fadeOut();
            $('#fondo-negro').fadeOut();
            $('#usuarios-resultado-container p').html("Usuario ID: " + id + " <b>actualizado</b> correctamente. Recarga la página pulsanto <b onclick='location.reload();' style='cursor: pointer;'>aquí</b>.");
            $('#usuarios-resultado').css('background-color',backColorOk)
                                    .toggle('blind','',tiempoDisplay);
            // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
            document.forms['edita-usuario'].reset();
            $('#edita-usuario').unbind("submit");
          } else {
            $('#usuarios-resultado').css('display', 'none');
            $('#usuario-form-edit').fadeOut();
            $('#fondo-negro').fadeOut();
            $('#usuarios-resultado-container p').html("ERROR - No se pudo ejecutar la acción solicitada (actualizar información del usuario con id " + id + ").");
            $('#usuarios-resultado').css('background-color',backColorKo)
                                    .toggle('blind','',tiempoDisplay);
            // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
            document.forms['edita-usuario'].reset();
            $('#edita-usuario').unbind("submit");
          }
        } else {
          $('#usuarios-resultado').css('display', 'none');
          $('#usuario-form-edit').fadeOut();
          $('#fondo-negro').fadeOut();
          $('#usuarios-resultado-container p').html("ERROR - No se pudo ejecutar la acción solicitada (actualizar información del usuario con id " + id + ").");
          $('#usuarios-resultado').css('background-color',backColorKo)
                                  .toggle('blind','',tiempoDisplay);
          // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
          document.forms['edita-usuario'].reset();
          $('#edita-usuario').unbind("submit");
        }
      }
    );
  });
}

// Mostrar formulario para crear usuario
function mostrarFormNewUser(){
  $('#usuario-form-new').toggle('blind','',tiempoDisplay);
  $('#fondo-negro').fadeIn("slow");
}

// Crear usuario nuevo.
function creaNuevoUsuario(comando){
  $('form[name=crea-usuario]').submit(function(){
    cambiar("crea-usuario-pwd","#crea-usuario-pwd-oculta","#crea-usuario-pwd","#crea-usuario");
    var usuario = document.getElementById('crea-usuario-usuario').value;
    var nombre = document.getElementById('crea-usuario-nombre').value;
    var correo = document.getElementById('crea-usuario-email').value;
    var grupo = document.getElementById('crea-usuario-grupo').value;
    var credencial = document.getElementById('crea-usuario-pwd').value;
    var credencialc = document.getElementById('crea-usuario-pwd-oculta').value;
    var estado = document.getElementById('crea-usuario-estado').value;
    $.post("inc/usuariosAdministra.php",
      {
        command : comando,
        user : usuario,
        name : nombre,
        email : correo,
        group : grupo,
        pwd : credencialc,
        state : estado
      },
      function(data,status){
        if(status == 'success'){
          if(data == 'ok' ){
            $('#usuarios-resultado').css('display', 'none');
            $('#usuario-form-new').fadeOut();
            $('#fondo-negro').fadeOut();
            $('#usuarios-resultado-container p').html("Nuevo usuario <b>creado</b> correctamente. Recarga la página pulsanto <b onclick='location.reload();' style='cursor: pointer;'>aquí</b>.");
            $('#usuarios-resultado').css('background-color',backColorOk)
                                    .toggle('blind','',tiempoDisplay);
            // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
            document.forms['crea-usuario'].reset();
            $('#crea-usuario').unbind("submit");
          } else {
            $('#usuarios-resultado').css('display', 'none');
            $('#usuario-form-new').fadeOut();
            $('#fondo-negro').fadeOut();
            $('#usuarios-resultado-container p').html("ERROR - No se pudo ejecutar la acción solicitada (crear nuevo usuario). " + data + ".");
            $('#usuarios-resultado').css('background-color',backColorKo)
                                    .toggle('blind','',tiempoDisplay);
            // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
            document.forms['crea-usuario'].reset();
            $('#crea-usuario').unbind("submit");
          }
        } else {
          $('#usuarios-resultado').css('display', 'none');
          $('#usuario-form-new').fadeOut();
          $('#fondo-negro').fadeOut();
          $('#usuarios-resultado-container p').html("ERROR - No se pudo ejecutar la acción solicitada (crear nuevo usuario).");
          $('#usuarios-resultado').css('background-color',backColorKo)
                                  .toggle('blind','',tiempoDisplay);
          // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
          document.forms['crea-usuario'].reset();
          $('#crea-usuario').unbind("submit");
        }
      }
    );
  });
}

// Mostrar formulario cambio pwd
function mostrarFormPwdUser(id,usuario){
    // Escribimos el enunciado del formulario y agregamos el id al input oculto.
    $('#cambia-usuario-pwd').html("Edita pwd usuario "+usuario+" - id "+id+" <img src='img/info.png' id='conf-info' title='La contraseña debe de contener al menos una letra mayúscula, una minúscula y un número.' />");
    $('#cambia-usuario-pwd-usuario').val(usuario);
    $('#cambia-usuario-pwd-id').val(id);
    // Mostrar la seccion cambia pwd usuario.
    $('#usuario-form-pwd').toggle('blind','',tiempoDisplay);
    $('#fondo-negro').fadeIn("slow");
}

// Cambiar Pwd a usuario.
function cambiaPwdUsuario(comando){
    $('form[name=cambia-pwd-usuario]').submit(function(){
        // Obtenemos las pwd del formulario.
        var pwd1 = document.forms['cambia-pwd-usuario']['cambia-usuario-pwd-1'].value;
        var pwd2 = document.forms['cambia-pwd-usuario']['cambia-usuario-pwd-2'].value;
        // Chequeamos las politicas para las credenciales.
        var errores = [];
        errores = politicaPwd(pwd1,pwd2);
        if(errores.length > 0){
            $('#usuarios-resultado').css('display', 'none');
            $('#usuario-form-pwd').fadeOut();
            $('#fondo-negro').fadeOut();
            $('#usuarios-resultado-container p').html("ERROR - no se pudo cambiar la contraseña del usuario: <br><b>" + errores.join("<br>") + "</b>");
            $('#usuarios-resultado').css('background-color',backColorKo)
                                    .toggle('blind','',tiempoDisplay);
            document.forms['cambia-pwd-usuario'].reset();
            $('#cambia-pwd-usuario').unbind("submit");
        } else {
            // Cambiamos las dos pwd del formulario.
            cambiar("cambia-usuario-pwd-1","#cambia-usuario-pwd-1-enc","#cambia-usuario-pwd-1","#cambia-pwd-usuario");
            cambiar("cambia-usuario-pwd-2","#cambia-usuario-pwd-2-enc","#cambia-usuario-pwd-2","#cambia-pwd-usuario");
            // Cogemos el valor de una de ellas ya encriptado para guardar en MySQL. Tambien el ID de usuario.
            pwdc = document.getElementById('cambia-usuario-pwd-1-enc').value;
            usuario = document.forms['cambia-pwd-usuario']['cambia-usuario-pwd-usuario'].value;
            id = document.forms['cambia-pwd-usuario']['cambia-usuario-pwd-id'].value;
            $.post("inc/usuariosAdministra.php",
                {
                    user : usuario,
                    id : id,
                    command : comando,
                    pwdplain : pwd1,
                    pwd : pwdc
                },
                function(data,status){
                    if(status == 'success'){
                        if(data == 'ok'){
                            $('#usuarios-resultado').css('display', 'none')
                            $('#usuario-form-pwd').fadeOut();
                            $('#fondo-negro').fadeOut();
                            $('#usuarios-resultado-container p').delay(3000).html("Contraseña cambiada <b>correctamente</b> para el usuario con ID: " + usuario);
                            $('#usuarios-resultado').css('background-color',backColorOk)
                                                    .toggle('blind','',tiempoDisplay);
                            // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
                            document.forms['cambia-pwd-usuario'].reset();
                            $('#cambia-pwd-usuario').unbind("submit");
                        } else {
                            $('#usuarios-resultado').css('display', 'none');
                            $('#usuario-form-pwd').fadeOut();
                            $('#fondo-negro').fadeOut();
                            $('#usuarios-resultado-container p').html("ERROR - no se pudo cambiar la contraseña del usuario " + usuario + ".");
                            $('#usuarios-resultado').css('background-color',backColorKo)
                                                    .toggle('blind','',tiempoDisplay);
                            document.forms['cambia-pwd-usuario'].reset();
                            $('#cambia-pwd-usuario').unbind("submit");
                        }
                    } else {
                        $('#usuarios-resultado').css('display', 'none');
                        $('#usuario-form-pwd').fadeOut();
                        $('#fondo-negro').fadeOut();
                        $('#usuarios-resultado-container p').html("ERROR - no se pudo cambiar la contraseña del usuario " + usuario + ".");
                        $('#usuarios-resultado').css('background-color',backColorKo)
                                                .toggle('blind','',tiempoDisplay);
                        document.forms['cambia-pwd-usuario'].reset();
                        $('#cambia-pwd-usuario').unbind("submit");
                    }
                }
            );
        }
    });
}

// Mostrar formulario para borrar usuario
function mostrarBorrarUsuario(id,usuario){
    // Escribimos el enunciado del formulario y agregamos el id al input oculto.
    $('#borrar-usuario').html("Borrar usuario " + usuario + " <img src='img/info.png' id='conf-info' title='Al borrar el usuario no será posible recuperarlo.' />");
    $('#borrar-usuario-id').val(id);
    $('#borrar-usuario-usuario').val(usuario);
    // Mostrar la seccion cambia pwd usuario.
    $('#usuario-form-borrar').toggle('blind','',tiempoDisplay);
    $('#fondo-negro').fadeIn("slow");
}

// Borrar usuario.
function borrarUsuario(comando){
    $('form[name=usuario-borrar]').submit(function(){
        // Si se pulsa el boton no.
        if(comando == 'noborrar'){
            $('#fondo-negro').fadeOut('slow');
            $('.form-oculto').fadeOut('slow');
            document.forms['usuario-borrar'].reset();
            $('#usuario-borrar').unbind("submit");
        } else {
            var usuarioid = document.forms['usuario-borrar']['borrar-usuario-id'].value;
            var usuario = document.forms['usuario-borrar']['borrar-usuario-usuario'].value;
            $.post("inc/usuariosAdministra.php",
                {
                    id : usuarioid,
                    command : comando,
                    user : usuario
                },
                function(data,status){
                    if(status == 'success'){
                        if(data == 'ok'){
                            $('#usuarios-resultado').css('display', 'none')
                            $('#usuario-form-borrar').fadeOut();
                            $('#fondo-negro').fadeOut();
                            $('#usuarios-resultado-container p').delay(3000).html("Usuario borrado <b>correctamente</b> - Usuario: " + usuario + " - ID: " + usuarioid +"<br>Recarga la página pulsanto <b onclick='location.reload();' style='cursor: pointer;'>aquí</b>.");
                            $('#usuarios-resultado').css('background-color',backColorOk)
                                                    .toggle('blind','',tiempoDisplay);
                            // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
                            document.forms['usuario-borrar'].reset();
                            $('#usuario-borrar').unbind("submit");
                        } else {
                            $('#usuarios-resultado').css('display', 'none')
                            $('#usuario-form-borrar').fadeOut();
                            $('#fondo-negro').fadeOut();
                            $('#usuarios-resultado-container p').delay(3000).html("ERROR - no se pudo borrar al usuario " + usuario + ".");
                            $('#usuarios-resultado').css('background-color',backColorKo)
                                                    .toggle('blind','',tiempoDisplay);
                            // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
                            document.forms['usuario-borrar'].reset();
                            $('#usuario-borrar').unbind("submit");
                        }
                    } else {
                        $('#usuarios-resultado').css('display', 'none')
                        $('#usuario-form-borrar').fadeOut();
                        $('#fondo-negro').fadeOut();
                        $('#usuarios-resultado-container p').delay(3000).html("ERROR - no se pudo borrar al usuario " + usuario + ".");
                        $('#usuarios-resultado').css('background-color',backColorKo)
                                                .toggle('blind','',tiempoDisplay);
                        // Reiniciamos los datos del formulario y evitamos eliminamos el submit para evitar el reenvio de los datos anteriores.
                        document.forms['usuario-borrar'].reset();
                        $('#usuario-borrar').unbind("submit");
                    }
                }
            );
        }
    });
}

// Funcion para filtrar, borrar filtro en seccion de administracion usuarios.
function filtroUsuarios(orden){
    if(orden == 'filtra'){
        // Agregar o modificar un filtro de usuario a la tabla de usuarios.
        if (event.keyCode == 13) {
            var filtro = document.getElementById('filtro-usuario-busca').value;
            var url = window.location.href;
            var buscar = /filtroUsuario=[a-zA-Z0-9\.\-\_]+/;
            // Aplicamos una URL u otra en funcion de si hay un filtrado previo o no.
            if(url.match(buscar)){
                var nuevo = "filtroUsuario="+filtro;
                url = url.replace(buscar,nuevo);
            } else {
                url = url + "&filtroUsuario=" + filtro;
            }
            window.location.href = url;
        }
    } else if (orden == 'borrarFiltro'){
        // Borrar un filtro de usuario en la tabla usuarios.
        var url = window.location.href;
        var buscar = /&filtroUsuario=[a-zA-Z\.\-\_]+/;
        var url = url.replace(buscar,'');
        window.location.href = url;
    }
}

// Funcion para ordenar la tabla de usuarios redirigiendo la URL.
function ordenaUsuarios(por){
    var url = window.location.href;
    var buscar = /sort=[0-9]/;
    var match = url.match(buscar);
    var res = match[0];
    if(res == ''){
        url = url + '&by=' + por + '&sort=1';
        window.location.href = url;
    } else if (res == 'sort=1') {
        var matchPor = /by=[a-z]+/;
        var nuevoPor = 'by=' + por;
        url = url.replace(matchPor,nuevoPor);
        url = url.replace("sort=1","sort=2");
        window.location.href = url;
    } else if (res == 'sort=2'){
        var matchPor = /by=[a-z]+/;
        var nuevoPor = 'by=' + por;
        url = url.replace(matchPor,nuevoPor);
        url = url.replace("sort=2","sort=1");
        window.location.href = url;
    }
}



// INDICE
///////////////

// Mostrar tabla indice/contenido.
function mostrarIndiceContenidoSortable(mostrar,id){
    // Indicamos por AJAX si es indice o contenido en mostrar, y si es contenido el id del indice padre.
    $.post("inc/indiceListado.php",
            {
                mostrar: mostrar,
                id: id
            },
            function(data,status){
                if ( status == 'success'){
                    // Si es para mostrar el indice.
                    if ( mostrar == 'indice' ) {
                        if(data)
                            $('#sortable-indice').html(data);
                        else
                            $('#sortable-indice').html('<br><p><b>No existen índices.</b></p>');
                        // Si es para mostrar el contenido.
                    } else if ( mostrar == 'contenido'){
                        $('#sortable-contenido').empty();
                        if(data)
                            $('#sortable-contenido').html(data);
                         else
                            $('#sortable-contenido').html('<br><p><b>Indice sin contenidos.</b></p>');
                        // Agregamos el ID del indice al input oculto del formulario para crear nuevos contenidos.
                        document.getElementById('indiceId').value = id
                        document.getElementById('indiceId').innerHTML = id;
                    }
                } else {
                    $('#sortable-indice').html('<br><p><b>Error al obtener los índices.</b></p>');
                }
            }
    );
}

// Crear indice/contenido.
function crearIndiceContenidoSortable(mostrar,titulo,id){
    // Si borramos un contenido usamos el cambio titulo de la funcion JS crearIndiceContenidoSortable para especificar el ID del índice para mostrar el resto de contenidos una vez borrado.

        // Indicamos por AJAX si es indice o contenido en nuevo, y si es contenido el id del indice padre.
        $.post("inc/indiceListado.php",
            {
                mostrar: mostrar,
                titulo: titulo,
                id: id
            },
            function(data,status){
                if ( status == 'success'){
                    //Si se creo un indice nuevo.
                    if ( mostrar == 'indiceAgrega' ){
                            document.getElementById('formNuevoIndice').reset();
                    } else if ( mostrar == 'contenidoAgrega' ){
                            document.getElementById('formNuevoContenido').reset();
                    }
                    if ( data.match("^ERROR") ){
                        $('#indice-resultado').css('display','none');
                        $('#indice-resultado p').html(data);
                        $('#indice-resultado').css('background-color',backColorKo)
                                              .toggle('blind','',tiempoDisplay);
                    } else {
                        if ( mostrar == 'indiceAgrega' || mostrar == 'indiceBorra' ){

                            if(data)
                                    $('#sortable-indice').html(data);
                                else
                                    $('#sortable-indice').html('<br><p><b>No existen índices.</b></p>');
                            $('#indice-resultado').css('display','none');
                            if ( mostrar == 'indiceAgrega' )
                                $('#indice-resultado p').html('El nuevo índice fue creado.');
                            else
                                $('#indice-resultado p').html('El índice con id <b>'+id+'</b> fue borrado.');
                            $('#indice-resultado').css('background-color',backColorOk)
                                                  .toggle('blind','',tiempoDisplay);


                        } else if ( mostrar == 'contenidoAgrega' || mostrar == 'contenidoBorra' ) {

                            if(data)
                                $('#sortable-contenido').html(data);
                            else
                                $('#sortable-contenido').html('<br><p><b>Indice sin contenidos.</b></p>');
                            $('#indice-resultado').css('display','none');
                            if ( mostrar == 'contenidoAgrega' )
                                $('#indice-resultado p').html('El nuevo contenido para el índice con id <b>'+id+'</b> fue agregado.');
                            else
                                $('#indice-resultado p').html('El contenido con id <b>'+id+'</b> para el índice con id <b>'+titulo+'</b> fue borrado.');
                            $('#indice-resultado').css('background-color',backColorOk)
                                                  .toggle('blind','',tiempoDisplay);
                        }
                    }
                } else {
                    $('#indice-resultado').css('display','none');
                    $('#indice-resultado p').text("Error a la hora de ejeuctar el comando. Estado: "+status);
                    $('#indice-resultado').css('background-color',backColorKo)
                                          .toggle('blind','',tiempoDisplay);
                }
            }
        );

}

// Mostrar confirmar borrar indice/contenido.
function mostrarBorrarIndiceContenido(mostrar,titulo,id){
    if ( mostrar.indexOf('contenido') != -1 )
        $('#borrar-indicecontenido').text("Borrar el contenido " + id + " asociado al índice " + titulo + ".");
    else
        $('#borrar-indicecontenido').text("Borrar el índice " + id + ".");

    $('#indicecontenido-mostrar').val(mostrar);
    $('#indicecontenido-titulo').val(titulo);
    $('#indicecontenido-id').val(id);
    // Mostrar la seccion borrar.
    $('#indicecontenido-form-borrar').toggle('blind','',tiempoDisplay);
    $('#fondo-negro').fadeIn("slow");
}

// Borrar indice/contenido.
function borrarIndiceContenido(comando){
    $('form[name=indicecontenido-borrar]').submit(function(){
        // Si se pulsa el boton no.
        if(comando == 'noborrar'){
            $('#fondo-negro').fadeOut('slow');
            $('.form-oculto').fadeOut('slow');
            document.forms['indicecontenido-borrar'].reset();
            $('#indicecontenido-borrar').unbind("submit");
        } else {
            var mostrar = document.forms['indicecontenido-borrar']['indicecontenido-mostrar'].value;
            var titulo = document.forms['indicecontenido-borrar']['indicecontenido-titulo'].value;
            var id = document.forms['indicecontenido-borrar']['indicecontenido-id'].value;
            $('.form-oculto').fadeOut();
            $('#fondo-negro').fadeOut();
            crearIndiceContenidoSortable(mostrar,titulo,id);
            document.forms['indicecontenido-borrar'].reset();
            $('#indicecontenido-borrar').unbind("submit");
        }
    });
}

// Nuevo indice.
function nuevoIndice(elemento){
        if ( elemento == 'indiceAgrega' ) {
            var titulo = document.getElementById('casillanuevoindice').value;
            var id = 'none';
        } else {
            var titulo = document.getElementById('casillanuevocontenido').value;
            var id = document.getElementById('indiceId').value;
        }
        // Si el titulo del nuevo indice no esta en blanco lo creamos.
        if (titulo)
            crearIndiceContenidoSortable(elemento,titulo,id);
}

// Cambia estado.
function cambiaEstado(elemento,id){
    $.post('inc/indiceCambiaEstado.php',
          {
            elemento: elemento,
            id: id
           },
           function(data,status){
                if ( status == 'success' ){
                    $('#indice-resultado').css('display','none');
                    $('#indice-resultado p').html('El '+elemento+' con id <b>'+id+'</b> cambió de estado.');
                    $('#indice-resultado').css('background-color',backColorOk)
                                          .toggle('blind','',tiempoDisplay);

                    var attr = $('#habilita-'+id).attr('class');
                    if ( attr == 'usuario-activo habilita')
                        $('#habilita-'+id).attr('class','usuario-inactivo habilita');
                    else
                        $('#habilita-'+id).attr('class','usuario-activo habilita');
                }
           }
        );
}

// Mostrar cambia indice a contenido.
function cambiaIndiceAContenido(comando,idpadreanterior,id){
    if ( comando == 'mostrar' ){
        $('#cambiar-contenido-indice').text('Cambiar de índice al contenido ' + id);
        $('#contenido-cambia-indice-contenido').val(id);
        $('#contenido-cambia-indice-indiceanterior').val(idpadreanterior);
        $.post('inc/indiceCambiaAContenido.php',
                {
                command: comando,
                padre: idpadreanterior,
                id: id
                },
                function(data,status){
                    if ( status == 'success' ){
                        $('#nuevo-indice-selecciona').html(data);
                    }
                }
        );
        // Mostrar la seccion borrar.
        $('#contenido-form-cambia-indice').toggle('blind','',tiempoDisplay);
        $('#fondo-negro').fadeIn("slow");

    } else if ( comando == 'nocambiar' ){

        $('.form-oculto').fadeOut();
        $('#fondo-negro').fadeOut();
        document.forms['contenido-cambia-indice'].reset();
        $('#contenido-cambia-indice').unbind("submit");

    } else if ( comando == 'cambiar' ){

        var indiceanterior = document.forms['contenido-cambia-indice']['contenido-cambia-indice-indiceanterior'].value;
        var indicenuevo = document.forms['contenido-cambia-indice']['nuevo-indice-selecciona'].value;
        var contenido = document.forms['contenido-cambia-indice']['contenido-cambia-indice-contenido'].value;

        $('.form-oculto').fadeOut();
        $('#fondo-negro').fadeOut();
        document.forms['contenido-cambia-indice'].reset();
        $('#contenido-cambia-indice').unbind("submit");

        $.post('inc/indiceCambiaAContenido.php',
                {
                command: comando,
                padre: indicenuevo,
                id: contenido
                },
                function(data,status){
                    if ( status == 'success' ){
                        if ( data == 'ok' ){

                            $('#indice-resultado').css('display','none');
                            $('#indice-resultado p').html("Cambiado índice al contenido <b>" + contenido + "</b>.<br>Nuevo índice <b>" + indicenuevo + "</b>.");
                            $('#indice-resultado').css('background-color',backColorOk)
                                              .toggle('blind','',tiempoDisplay);
                            mostrarIndiceContenidoSortable('contenido',indiceanterior);

                            } else {

                            $('#indice-resultado').css('display','none');
                            $('#indice-resultado p').html(data);
                            $('#indice-resultado').css('background-color',backColorKo)
                                              .toggle('blind','',tiempoDisplay);

                            }
                    } else {

                        $('#indice-resultado').css('display','none');
                        $('#indice-resultado p').html("Alno ho ha ido bien al ejecutar el cambio de &iacute;ndice al contenido " + contenido + ".");
                        $('#indice-resultado').css('background-color',backColorKo)
                                                .toggle('blind','',tiempoDisplay);

                    }
                }
        );

    }
}


// Funcion para mostrar u ocultar cabecera config con info.
function muestraInfo(buttom){
  if(buttom.innerHTML == 'Leer más'){
    $('#conf-cabecera').css({'max-height':'max-content',flex: '1 1 80%','-webkit-flex': '1 1 80%'});
    $('#conf-config').css({flex: '1 1 0%','-webkit-flex': '1 1 0%'});
    document.getElementById('expandeInfo').innerHTML = 'Leer menos';
  } else {
    $('#conf-cabecera').css({'max-height':'50px',flex: '1 1 auto','-webkit-flex': '1 1 auto'});
    $('#conf-conf').css({flex: '1 1 auto','-webkit-flex': '1 1 auto'});
    document.getElementById('expandeInfo').innerHTML = 'Leer más';
  }
}


// Funcion para evitar espacios en blanco.
function evitaEspacios(ele){
    if(event.keyCode == 32){
      alert('Has pulsado espacio. Elemento: ' + ele.id);
      var input = '#' + ele.id;
      $(input).val('');
    }
}


// Funcion para mostrar el listado de logs.
function mostrarLogs(datei,datef,action,element,user){
  //alert('Resultado: ' + datei + ' - ' + datef + ' - ' + action + ' - ' + element + ' - ' + user);
  $.ajax({
    type: 'GET',
    url: 'inc/logsContenido.php',
    data: { 'datei': datei, 'datef': datef, 'action': action, 'element': element, 'user': user},
    success: function(data){
      $('#resultadoLogs').html(data);
    },
    error: function(data){
      $('#resultadoLogs').html('<p>ERROR - no se pueden mostrar los registros de log.</p>');
    }
  });
}

// Funcion para mostrar la comparacion de logs antiguo/nuevo
function muestraLogsCompara(id){
  // Primero obtenemos el contenido antiguo y lo agregamos al div correspondiente.
  $.ajax({
    type: 'get',
    url: 'inc/logsContenidoCompara.php',
    data: { 'id': id, content: 'contenido_antiguo'},
    success: function(data){
      $('#logsComparaAntiguo').html(data);
    }
  });

  // Luego obtenemos el contenido del nuevo y lo agregamos al div correspondiente.
  $.ajax({
    type: 'get',
    url: 'inc/logsContenidoCompara.php',
    data: { 'id': id, content: 'contenido_nuevo'},
    success: function(data){
      $('#logsComparaNuevo').html(data);
    }
  });

  // Mostramos el div con los contenidos para su comparación, antiguo y nuevo, sobre el fondo negro.
  $('.fondo-negro').toggle();
  $('#logsComparaOculto').css({'visibility': 'visible'});

}

// Copia contenido antiguo o nuevo al comparar contenido en la seccion de logs.
function copiaContenido(elemento){
  //var $temp = $("<input>");

  var $temp = $("<textarea>");
  //var brRegex = /<br\s*[\/]?>/gi;

  $("body").append($temp);
  $temp.val($(elemento).html()).select();
  //$temp.val($(elemento).html().replace(brRegex, "\r\n")).select();
  document.execCommand("copy");
  $temp.remove();
  if ( elemento == '#logsComparaAntiguo' )
    alert('Copiado contenido antiguo.');
  else
    alert('Copiado contenido nuevo.');
}

// Muestra casilla buscar.
function muestraBuscar(){
  $('#buscaPatron').toggle("blind",'',tiempoDisplay);
  var color = document.getElementById('buscarIcon').style.backgroundColor;
  if ( color == 'lightblue')
    $('#buscarIcon').css('background-color', 'transparent');
  else
    $('#buscarIcon').css('background-color', 'lightblue');
}


// Funcion para mostrar el contenido del indice si esta oculto al buscar.
function compruebaExpande(id_padre,id_contenido){
  var estado = document.getElementById('indice-' + id_padre).style.display;
  if ( estado == 'none' )
    document.cookie = "indice-" + id_padre + "=1";
    var protocolo = document.location.protocol;
    var dominio = document.location.hostname;
    var url = protocolo + '//' + dominio + '?indice=' + id_padre + '&contenido=' + id_contenido;
    document.location.href = url;
}
