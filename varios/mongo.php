<?php

$conexion = new MongoClient();
echo "Conexion correcta <br>";
// Seleccionamos la db.
$bd = $conexion->pruebas;
echo "Seleccionada DB pruebas <br>";
// Seleccionamos la coleccion.
$coleccion = $bd->entradas;
echo "Seleccionado documento entradas <br>";
// Array con entrada nueva.
$doc = array(
	"nombre" => "Segunda entrada",
	"tipo" => "PHP",
	"count" => 1,
	"info" => (object)array('x' => 203, 'y' => 102),
	"version" => array('0.9.7','0.9.8','0.9.9')
);
// Insertamosa.
echo "Agregamos contenido al documento<br>";
$coleccion->insert( $doc );
echo "Agregado contenido al documento<br><hr><br>";

// Comprobar que se agrego correctamente anteriormente.
$documento = $coleccion->findOne();
var_dump( $documento );
echo "<br>";
// Contamos los documentos de una coleccion.
echo $coleccion->count();
echo "<br>";
// Cursor.
$cursor = $coleccion->find();
foreach($cursor as $id => $valor) {
	echo "$id: ";
	var_dump( $valor );
	echo "<br>";
}
// Consulta.
$consulta = array( 'nombre' => 'Entrada nueva uno');
$cursor = $coleccion->find( $consulta );
while ( $cursor->hasNext()) {
	var_dump( $cursor->getNext() );
	echo "<br>";
}
?>
