<?php

//Conectamos con Redis server
$redis = new Redis();
$redis->pconnect('127.0.0.1', 6379);

//Seleccionamos la db 5
$redis->select('7');

//Probamos la conexion
echo "Server is running?";
echo "<br />";
echo $redis->ping()."<br />";

//Agregamos una entrada en la base de datos
$redis->set("entrada-nueva", "prueba de entrada desde PHP de nuevo");
echo "Vamos a recuperar la entrada agregada 'entrada-nueva': ".$redis->get("entrada-nueva")."<br />";

//Mostramos informacion de la conexion actual.
echo "<br />";
//$array = $redis->client('info');
//foreach ($array as $key => $value) {
//  echo "Key: $key - Value: $value <br />";
//}

$array = $redis->client('list');
$i = 0;
while($array[$i]) {
  foreach ($array[$i] as $key => $value) {
    if ( $key == 'db')
      echo "La base de datos es ".$value;
  }
  $i++;
}
var_dump($array);
 ?>
