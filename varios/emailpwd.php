<!doctype html>
<html>
  <head>
    <title>WikiB-PWD</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
      html, body {
        margin: 0px;
        padding: 0px;
        background-color: grey;
        width: 100%;
        color: navajowhite;
        font-weight: bold;
      }
      #contenido {
        max-width: 350px;
        margin: 15px auto;
      }
      #cabecera {
        /*background-color: #66a3ff;*/
        background-color: blue;
        margin: 10px auto 5px;
        border: none;
        text-align: center;
        padding: 5px 10px;
        text-decoration: underline;
      }
      #cuerpo {
        /*background-color: #ff6666;*/
        background-color: blue;
        margin: auto;
        border: none;
        text-align: left;
        padding: 10px;
      }
      #cuerpo a {
        color: #ff6666;
      }
      #cuerpo a:hover {
        color: black;
      }
      #pie {
        /*background-color: #66a3ff;*/
        background-color: blue;
        margin: 5px auto 10px;
        text-align: right;
        padding: 2px 10px;
        font-size: 10px;
      }
    </style>
  </head>
  <body>
    <div id="contenido">
      <div id="cabecera">
        <h2>WikiB - reinicio de contrase&ntilde;a</h2>
      </div>
      <div id="cuerpo">
        <p>Acceidendo al siguiente enlace se generará una contraseña aleatoria para tu cuenta.</p>
        <p>El enlace caduca en 5 minutos, <b>¡date prisa!</b></p>
        <a href="#">ENLACE</a>
      </div>
      <div id="pie">
        <h3>WikiB</h3>
      </div>
    </div>
  </body>
</html>
